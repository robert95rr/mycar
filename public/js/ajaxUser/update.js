//Funkcja aktualizacji danych użytkownika
var url = "http://127.0.0.1:8000/ajaxUpdateU";

$(document).on('click', '.open_modal', function(){
  var userId = $(this).val();
  $.get(url + '/' + userId, function (data){
    //success
    console.log(data);
    $('#userId').val(data.id);
    $('#first_name_ajax').val(data.first_name);
    $('#last_name_ajax').val(data.last_name);
    $('#email_ajax').val(data.email);
    $('#phone_number_ajax').val(data.phone_number);
    $('#btn-update').val("update");
    $('#modal-default-editUser').modal('show');
  })
});

//update existing User
$("#btn-update").click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData = $('#updateUser').serialize();
  var userId =  $('#userId').val();
  var type = "PUT";
  var myUrl = url + '/' + userId;
  $.ajax({
    type: type,
    url: myUrl,
    data: formData,
    dataType: 'json',
    success: function (data){
      var user = '<div class="box-body box-profile" id="user' + data.id + '">' +
                  '<img class="profile-user-img img-responsive img-circle" src="/photos/Robert_Rezler_2019.png" alt="User profile picture">' +
                  '<h3 class="profile-username text-center">' + data.first_name + ' ' + data.last_name + '</h3>' +
                  '<p class="text-muted text-center">Użytkownik</p><br>' + '<div class="col-md-10 col-md-offset-1">' +
                  '<ul class="list-group list-group-unbordered">' + '<li class="list-group-item">' +
                  '<b>Adres E-mail</b> <a class="pull-right">' + data.email + '</a>' + '</li><li class="list-group-item">' +
                  '<b>Numer telefonu</b> <a class="pull-right">' + data.phone_number + '</a>' + '</li></ul></div>' + '<div class="col-md-12"><br>' +
                  '<button type="button" class="btn btn-sm btn-warning pull-right open_modal" value="' + data.id + '"><i class="fa fa-edit"></i> Edytuj</button>' +
                  '</div></div>';

      $("#user" + userId).replaceWith(user);
      $('#updateUser').trigger("reset");
      $('#modal-default-editUser').modal('hide')
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
