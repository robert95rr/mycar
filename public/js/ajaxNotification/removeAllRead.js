//Funkcja, króra usuwa wszystkie przeczytane powiadomienia przez użytkownika
var urlN_dAR = "http://127.0.0.1:8000/ajaxRemoveAllReadNotifications";
$('#btn-deleteAllRead').click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  $.ajax({
    type: "DELETE",
    url: urlN_dAR,
    success: function (data) {
      console.log(data);
      $("#showAllReadNotifications").remove();
      $('#modal-default-showAllReadNotifications').modal('hide');

    },
    error: function (data) {
      console.log('Error:', data);
    }
  });
});
