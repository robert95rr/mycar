//Funkcja pokazująca wybrane powiadomienie przez użytkownika
var urlShowNotification = "http://127.0.0.1:8000/ajaxShowNotification";

$(document).ready(function(){
  $(document).on('click', '#showNotification', function (e){
    var notificationId = $(this).attr('value');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
      }
    })
    e.preventDefault();
    var type = "GET";
    var myUrl = urlShowNotification + '/' + notificationId;
    $.ajax({
      type: type,
      url: myUrl,
      dataType: 'json',
      success: function (data){
        console.log(data[3]);
        var test = $('.label-warning').text();
        console.log(test);
        var contentHeader = '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                            + '<h4 class="modal-title">' + data[0].data[0]['category'] +'<span class="label label-success">' + data[1] + '&nbsp;&nbsp;' + data[2] + '</span></h4>';
        var contentBody = '<h4><i class="fa fa-calendar margin-r-5"></i>' + data[0].data[0]['date_event'] + '<span class="label label-primary pull-right">' + data[3] + '</span></h4>'
                          + '<p>' + data[0].data[0]['description'] + '</p>';
        var contentFooter = '<button type="button" class="btn btn-outline pull-right" id="markAsRead" value="' + data[0].id + '">Oznacz jako przeczytane</button>';
        $('#headerNotification').html(contentHeader);
        $('#contentNotification').html(contentBody);
        $('#footerNotification').html(contentFooter);
        $('#modal-default-showNotification').modal('show')
      },
      error: function (data){
        console.log('Error:',data);
      }
    });
  });
});
