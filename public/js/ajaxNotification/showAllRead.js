//Funkcja pokazuje wszytkie przeczytane powiadomienia dla użytkownika
var urlN_showAR = "http://127.0.0.1:8000/ajaxShowNotificationsAllRead";

$("#showAllReadNotifications").click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csfr-token"]').attr('content')
    }
  })
  e.preventDefault();
  var type = "GET";
  var myUrl = urlN_showAR;
  $.ajax({
    type: type,
    url: myUrl,
    dataType: 'json',
    success: function (data){
      var notifications = "";
      console.log(data[0][0].data[0]['category']);
      console.log(data[1]);
      if(data[0].length === 0)
      {
        notifications = '<h3>Brak</h3>';
      }
      else
      {
        $.each(data[0], function(index){
          if(data[0][index].data[0]['category'] == 'Ubezpieczenie')
          {
            notifications += '<div class="jumbotron"><h4><span class="label pull-right" id="labelColor">' + data[1][index].make + ' ' + data[1][index].model +
            '</span><i class="icon fa fa-shield"></i><b> ' + data[0][index].data[0]['date_event'] + '</b></h4><p>' + data[0][index].data[0]['description'] + '</p></div>';
          }
          else if(data[0][index].data[0]['category'] == 'Serwis')
          {
            notifications += '<div class="jumbotron"><h4><span class="label pull-right" id="labelColor">' + data[1][index].make + ' ' + data[1][index].model +
            '</span><i class="icon fa fa-wrench"></i><b> ' + data[0][index].data[0]['date_event'] + '</b></h4><p>' + data[0][index].data[0]['description'] + '</p></div>';
          }
          else if(data[0][index].data[0]['category'] == 'Przeglad techniczny')
          {
            notifications += '<div class="jumbotron"><h4><span class="label pull-right" id="labelColor">' + data[1][index].make + ' ' + data[1][index].model +
            '</span><i class="icon fa fa-stethoscope"></i><b> ' + data[0][index].data[0]['date_event'] + '</b></h4><p>' + data[0][index].data[0]['description'] + '</p></div>';
          }
        });
      }
      $("#contentNotifications").html(notifications);
      $('#modal-default-showAllReadNotifications').modal('show')
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
