//Funkcja, która usuwa wybrane powiadomienie przez uzytkownika
var urlN_d = "http://127.0.0.1:8000/ajaxRemoveN";

$(document).on('click', '.open_modalDeleteNotification', function(){
  var notificationId = $(this).val();

  $.get(urlN_d + '/' + notificationId, function (data){
    //success data
    console.log(data.id);
    $('#notification_delete_id').val(data.id);
    $('#btn-deleteN').val("deleteN");
    $('#modal-danger-remove-n').modal('show');
  })
});
//usuwanie zapisanego powiadomienia
$('#btn-deleteN').click(function (e){
  var notificationId = $('#notification_delete_id').val();
  console.log(notificationId);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  $.ajax({
    type: "DELETE",
    url: urlN_d + '/' + notificationId,
    success: function (data) {
      console.log(data);
      $("#notification" + notificationId).remove();
      $('#modal-danger-remove-n').modal('hide');
    },
    error: function (data) {
      console.log('Error:', data);
    }
  });
});
