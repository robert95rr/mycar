//Funkcja, która oznacza wyswietlone powiadomienie przez użytkownika jako przeczytane
var urlNotificationMarkAsRead = "http://127.0.0.1:8000/ajaxNotificationMarkAsRead";

$(document).on('click', '#markAsRead', function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var notificationIdAsRead = $(this).val();
  var type = 'GET';
  var myUrl2 = urlNotificationMarkAsRead + '/' + notificationIdAsRead;
  $.ajax({
    type: type,
    url: myUrl2,
    dataType: 'json',
    success: function(data){
      console.log(data);
      location.reload();
      $('#modal-default-showNotification').modal('hide')
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
