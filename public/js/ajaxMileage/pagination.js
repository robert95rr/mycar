//Paginacja listy przebiegów
$(document).ready(function(){
  $(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    fetch_data(page);
  });

  function fetch_data(page)
  {
    var carId = $('#car_id').val();
    console.log(carId);
    $.ajax({
      url:"/fetch_dataMileage/"+carId+"?page="+page,
      success:function(data)
      {
        $('#timeline_mileage').html(data);
      }
    });
  }
});
