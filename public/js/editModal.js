//Funkcje pobierania i wstawiania wartości do pól formularza Edycyjnego

//Edycja Przebiegu (Mileage)
$('#modal-default-edit-m').on('show.bs.modal', function (event) {

  var button = $(event.relatedTarget)
  var value = button.data('myvalue')
  var date = button.data('mydate')
  var description = button.data('mydescription')
  var mileage_id = button.data('mileageid')
  var modal = $(this)

  modal.find('.modal-body #value').val(value);
  modal.find('.modal-body #datepicker').val(date);
  modal.find('.modal-body #description').val(description);
  modal.find('.modal-body #mileage_id').val(mileage_id);
})
//Edycja Paliwa (Fuel)
$('#modal-default-edit-f').on('show.bs.modal', function (event) {

  var button = $(event.relatedTarget)
  var amount_before = button.data('amountbefore')
  var amount_refueled = button.data('amountrefueled')
  var refueling_price = button.data('refuelingprice')
  var mileage = button.data('mileage')
  var date = button.data('date')
  var place = button.data('place')
  var fuel_id = button.data('fuelid')
  var modal = $(this)

  modal.find('.modal-body #amount_before').val(amount_before);
  modal.find('.modal-body #amount_refueled').val(amount_refueled);
  modal.find('.modal-body #refueling_price').val(refueling_price);
  modal.find('.modal-body #mileage').val(mileage);
  modal.find('.modal-body #datepicker').val(date);
  modal.find('.modal-body #place').val(place);
  modal.find('.modal-body #fuel_id').val(fuel_id);
})
//Edycja Stanu bieżnika (Tread condition)
$('#modal-default-edit-tc').on('show.bs.modal', function (event) {

  var button = $(event.relatedTarget)
  var name = button.data('name')
  var dot = button.data('dot')
  var start_tread = button.data('starttread')
  var end_tread = button.data('endtread')
  var start_date = button.data('startdate')
  var end_date = button.data('enddate')
  var start_mileage = button.data('startmileage')
  var end_mileage = button.data('endmileage')
  var tire_position = button.data('tireposition')
  var season = button.data('season')
  var tread_condition_id = button.data('treadconditionid')
  var modal = $(this)

  modal.find('.modal-body #name').val(name);
  modal.find('.modal-body #dot').val(dot);
  modal.find('.modal-body #start_tread').val(start_tread);
  modal.find('.modal-body #end_tread').val(end_tread);
  modal.find('.modal-body #datepicker7').val(start_date);
  modal.find('.modal-body #datepicker8').val(end_date);
  modal.find('.modal-body #start_mileage').val(start_mileage);
  modal.find('.modal-body #end_mileage').val(end_mileage);
  modal.find('.modal-body #tire_position').val(tire_position);
  modal.find('.modal-body #season').val(season);
  modal.find('.modal-body #tread_condition_idEdit').val(tread_condition_id);
})
//Edycja Planu serwisowego (Service plan)
$('#modal-default-edit-sp').on('show.bs.modal', function (event) {

  var button = $(event.relatedTarget)
  var car_model = button.data('carmodel')
  var service_task = button.data('servicetask')
  var interval = button.data('interval')
  var description = button.data('description')
  var service_plan_id = button.data('serviceplanid')
  var modal = $(this)

  modal.find('.modal-body #car_model').val(car_model);
  modal.find('.modal-body #service_task').val(service_task);
  modal.find('.modal-body #interval').val(interval);
  modal.find('.modal-body #description').val(description);
  modal.find('.modal-body #service_plan_idEdit').val(service_plan_id);
})
