//Funkcja edycji ubezpieczenia
var urlUpdate = "http://127.0.0.1:8000/ajaxUpdateInsurance";

$(document).on('click', '.open_modalEdit', function(){
  var insuranceId = $(this).val();
  $.get(urlUpdate + '/' + insuranceId, function(data){
    //success data
    console.log(data[0]);
    console.log(data[1]);
    $('#insurance_id').val(data[0].id);
    $('#car_id').val(data[0].car_id);
    $('#company_name').val(data[0].company_name);
    $('#product_name').val(data[0].product_name);
    $('#insurance_type').val(data[1]).trigger("change");
    $('#policy_number').val(data[0].policy_number);
    $('#datepicker6').val(data[0].period_from);
    $('#datepicker7').val(data[0].period_to);
    $('#price').val(data[0].price);
    $('#mileage').val(data[0].mileage);
    $('#btn-update').val("update");
    $('#modal-default-editInsurance').modal('show');
  })
});

$("#btn-update").click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData = $('#updateInsurance').serialize();
  var insuranceId = $('#insurance_id').val();
  var type = "PUT";
  var urlAjaxUpdate = urlUpdate + '/' + insuranceId;
  $.ajax({
    type: type,
    url: urlAjaxUpdate,
    data: formData,
    dataType: 'json',
    success: function(data){
      var insurance = '<div id="insurance' + data.id + '"><h3 class="text-center">' + data.company_name + '</h3>'
      + '<p class="text-muted text-center">' + "Firma ubezpieczeniowa" + '</p>' + '<hr>' +
      '<strong><i class="fa fa-shield margin-r-5"></i>' + "Nazwa ubezpieczenia" + '</strong>' +
      '<p class="text-muted">' + data.product_name + '</p>' + '<br>' +
      '<strong>' + "Rodzaj ubezpieczenia" + '</strong>' +
      '<p class="text-muted">' + data.insurance_type + '</p>' + '<br>' +
      '<strong>' + "Numer polisy" + '</strong>' +
      '<p class="text-muted">' + data.policy_number + '</p>' + '<br>' +
      '<strong><i class="fa fa-calendar margin-r-5"></i>' + "Okres ubezpieczenia" + '</strong>' +
      '<p class="text-muted">' + data.period_from + " - " + data.period_to + '</p>' + '<br>' +
      '<strong>' + "Cena" + '</strong>' +
      '<p class="text-muted"><span class="label label-danger">' + data.price + " zł" + '</span></p>' + '<br>' +
      '<strong><i class="fa fa-road margin-r-5"></i>' + "Przebieg" + '</strong>' +
      '<p class="text-muted">' + data.mileage + " km" + '</p><hr>' +
      '<button class="btn btn-warning margin btn-sm open_modalEdit" value="' + data.id + '"><i class="fa fa-edit"></i>' + " Edytuj" + '</button>' +
      '</div>';

      $("#insurance" + insuranceId).replaceWith(insurance);
      $('#updateInsurance').trigger("reset");
      $('#modal-default-editInsurance').modal('hide')
    },
    error: function(data){
      console.log('Error:', data);
    }
  });
});
