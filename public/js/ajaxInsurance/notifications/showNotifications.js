//Funkcja pokazuje wszytkie zapisane powiadomienia w bazie danych dotyczace ubezpieczenia
var urlIN_show = "http://127.0.0.1:8000/ajaxShowNotificationsInInsurance";

$("#showNI").click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csfr-token"]').attr('content')
    }
  })
  e.preventDefault();
  var carId = $('#car_id').val();
  var type = "GET";
  var myUrl = urlIN_show + '/' + carId;
  $.ajax({
    type: type,
    url: myUrl,
    dataType: 'json',
    success: function (data){
      var notification = "";
      var date = new Date();
      var day = ('0' + date.getDate()).slice(-2);
      var month = ('0' + (date.getMonth() + 1)).slice(-2);
      var year = date.getFullYear();
      var fullDate = year + '-' + month + '-' + day;
      console.log(fullDate);
      $.each(data, function(index){
        if(data[index].data[0]['reminder'] == "1")
        {
          if(data[index].data[0]['date_event'] < fullDate)
          {
            notification += '<div class="info-box bg-blue" id="notification' + data[index].id + '"><span class="info-box-icon"><i class="fa fa-shield"></i></span>' +
            '<div class="info-box-content"><span class="info-box-text">' + data[index].data[0]['description'] + '<span class="label label-danger pull-right">Nieaktualne</span></span>' +
            '<span class=info-box-number">' + data[index].data[0]['date_event'] + '</span>' +
            '<div class="progress"><div class="progress-bar" style="width: 100%"></div></div>' +
            ' Przypomnienie: <strong id="reminderInsurance"> 1 dzień wcześniej </strong><button class="btn btn-xs btn-primary pull-right open_modalDeleteNotification" value="' + data[index].id + '">Usuń</button><button class="btn btn-xs btn-primary pull-right open_modalEditNotification" value="' + data[index].id + '">' + "Edytuj" + '</button>' +
            '</div></div>';
          }else
            notification += '<div class="info-box bg-blue" id="notification' + data[index].id + '"><span class="info-box-icon"><i class="fa fa-shield"></i></span>' +
            '<div class="info-box-content"><span class="info-box-text">' + data[index].data[0]['description'] + '</span>' +
            '<span class=info-box-number">' + data[index].data[0]['date_event'] + '</span>' +
            '<div class="progress"><div class="progress-bar" style="width: 100%"></div></div>' +
            ' Przypomnienie: <strong id="reminderInsurance"> 1 dzień wcześniej </strong><button class="btn btn-xs btn-primary pull-right open_modalDeleteNotification" value="' + data[index].id + '">Usuń</button><button class="btn btn-xs btn-primary pull-right open_modalEditNotification" value="' + data[index].id + '">' + "Edytuj" + '</button>' +
            '</div></div>';
        }else
          if(data[index].data[0]['date_event'] < fullDate)
          {
            notification += '<div class="info-box bg-blue" id="notification' + data[index].id + '"><span class="info-box-icon"><i class="fa fa-shield"></i></span>' +
            '<div class="info-box-content"><span class="info-box-text">' + data[index].data[0]['description'] + '<span class="label label-danger pull-right">Nieaktualne</span></span>' +
            '<span class=info-box-number">' + data[index].data[0]['date_event'] + '</span>' +
            '<div class="progress"><div class="progress-bar" style="width: 100%"></div></div>' +
            ' Przypomnienie: <strong id="reminderInsurance">' + data[index].data[0]['reminder'] + ' dni wcześniej </strong><button class="btn btn-xs btn-primary pull-right open_modalDeleteNotification" value="' + data[index].id + '">Usuń</button><button class="btn btn-xs btn-primary pull-right open_modalEditNotification" value="' + data[index].id + '">' + "Edytuj" + '</button>' +
            '</div></div>';
          }else
            notification += '<div class="info-box bg-blue" id="notification' + data[index].id + '"><span class="info-box-icon"><i class="fa fa-shield"></i></span>' +
            '<div class="info-box-content"><span class="info-box-text">' + data[index].data[0]['description'] + '</span>' +
            '<span class=info-box-number">' + data[index].data[0]['date_event'] + '</span>' +
            '<div class="progress"><div class="progress-bar" style="width: 100%"></div></div>' +
            ' Przypomnienie: <strong id="reminderInsurance">' + data[index].data[0]['reminder'] + ' dni wcześniej </strong><button class="btn btn-xs btn-primary pull-right open_modalDeleteNotification" value="' + data[index].id + '">Usuń</button><button class="btn btn-xs btn-primary pull-right open_modalEditNotification" value="' + data[index].id + '">' + "Edytuj" + '</button>' +
            '</div></div>';

      });
      $("#block_notificationsIns").html(notification);
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
