//Funkcja edycji powiadomienia dotyczacego ubezpieczenia
var urlUpdateNotification = "http://127.0.0.1:8000/ajaxUpdateNotification";

$(document).on('click', '.open_modalEditNotification', function(){
  var notificationId = $(this).val();
  $.get(urlUpdateNotification + '/' + notificationId, function(data){
    $('#notificationInsuranceId').val(data[0].id);
    $('#datepicker2').val(data[0].data[0]['date_event']);
    $('#reminder option').filter('[value="' + data[0].data[0]['reminder'] + '"]').prop('selected', true);
    $('#descriptionEdit').val(data[0].data[0]['description']);
    $('#btn-updateNotification').val("update");
    $('#modal-default-editNotificationInsurance').modal('show');
  })
});

$("#btn-updateNotification").click(function (e){
  $.ajaxSetup({
    headers: {
      'X-SCRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData  = $('#updateNotificationsInsurance').serialize();
  var notificationId = $('#notificationInsuranceId').val();
  var type = 'PUT';
  var urlAjaxUpdate = urlUpdateNotification + '/' + notificationId;
  $.ajax({
    type: type,
    url: urlAjaxUpdate,
    data: formData,
    dataType: 'json',
    success: function(data){
      console.log(data);
      var date = new Date();
      var day = ('0' + date.getDate()).slice(-2);
      var month = ('0' + (date.getMonth() + 1)).slice(-2);
      var year = date.getFullYear();
      var fullDate = year + '-' + month + '-' + day;
      if(data[0].data[0]['reminder'] == "1")
      {
        if(data[0].data[0]['date_event'] < fullDate)
        {
          var notificationUpdate = '<div class="info-box bg-blue" id="notification' + data[0].id + '"><span class="info-box-icon"><i class="fa fa-shield"></i></span>' +
          '<div class="info-box-content"><span class="info-box-text">' + data[0].data[0]['description'] + '<span class="label label-danger pull-right">Nieaktualne</span></span>' +
          '<span class=info-box-number">' + data[0].data[0]['date_event'] + '</span>' +
          '<div class="progress"><div class="progress-bar" style="width: 100%"></div></div>' +
          ' Przypomnienie: <strong id="reminderInsurance"> 1 dzień wcześniej </strong><button class="btn btn-xs btn-primary pull-right open_modalDeleteNotification" value="' + data[0].id + '">Usuń</button><button class="btn btn-xs btn-primary pull-right open_modalEditNotification" value="' + data[0].id + '">' + "Edytuj" + '</button>' +
          '</div></div>';
        }else
          var notificationUpdate = '<div class="info-box bg-blue" id="notification' + data[0].id + '"><span class="info-box-icon"><i class="fa fa-shield"></i></span>' +
          '<div class="info-box-content"><span class="info-box-text">' + data[0].data[0]['description'] + '</span>' +
          '<span class=info-box-number">' + data[0].data[0]['date_event'] + '</span>' +
          '<div class="progress"><div class="progress-bar" style="width: 100%"></div></div>' +
          ' Przypomnienie: <strong id="reminderInsurance"> 1 dzień wcześniej </strong><button class="btn btn-xs btn-primary pull-right open_modalDeleteNotification" value="' + data[0].id + '">Usuń</button><button class="btn btn-xs btn-primary pull-right open_modalEditNotification" value="' + data[0].id + '">' + "Edytuj" + '</button>' +
          '</div></div>';
      }else
        if(data[0].data[0]['date_event'] < fullDate)
        {
          var notificationUpdate = '<div class="info-box bg-blue" id="notification' + data[0].id + '"><span class="info-box-icon"><i class="fa fa-shield"></i></span>' +
          '<div class="info-box-content"><span class="info-box-text">' + data[0].data[0]['description'] + '<span class="label label-danger pull-right">Nieaktualne</span></span>' +
          '<span class=info-box-number">' + data[0].data[0]['date_event'] + '</span>' +
          '<div class="progress"><div class="progress-bar" style="width: 100%"></div></div>' +
          ' Przypomnienie: <strong id="reminderInsurance">' + data[0].data[0]['reminder'] + ' dni wcześniej </strong><button class="btn btn-xs btn-primary pull-right open_modalDeleteNotification" value="' + data[0].id + '">Usuń</button><button class="btn btn-xs btn-primary pull-right open_modalEditNotification" value="' + data[0].id + '">' + "Edytuj" + '</button>' +
          '</div></div>';
        }else
        var notificationUpdate = '<div class="info-box bg-blue" id="notification' + data[0].id + '"><span class="info-box-icon"><i class="fa fa-shield"></i></span>' +
        '<div class="info-box-content"><span class="info-box-text">' + data[0].data[0]['description'] + '</span>' +
        '<span class=info-box-number">' + data[0].data[0]['date_event'] + '</span>' +
        '<div class="progress"><div class="progress-bar" style="width: 100%"></div></div>' +
        ' Przypomnienie: <strong id="reminderInsurance">' + data[0].data[0]['reminder'] + ' dni wcześniej </strong><button class="btn btn-xs btn-primary pull-right open_modalDeleteNotification" value="' + data[0].id + '">Usuń</button><button class="btn btn-xs btn-primary pull-right open_modalEditNotification" value="' + data[0].id + '">' + "Edytuj" + '</button>' +
        '</div></div>';

        $("#notification" + notificationId).replaceWith(notificationUpdate);
        $('#updateNotificationsInsurance').trigger("reset");
        $('#modal-default-editNotificationInsurance').modal('hide');
    },
    error: function(data){
      console.log('Error:', data);
    }
  });
});
