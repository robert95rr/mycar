//Paginacja kosztów
$(document).ready(function(){
  $(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    fetch_data(page);
  });

  function fetch_data(page)
  {
    var carId = $('#carIdCT').val();
    console.log(carId);
    $.ajax({
      url:"/fetch_data/"+carId+"?page="+page,
      success:function(data)
      {
        $('#timeline_costs').html(data);
      },
      error:function(data)
      {
        console.log('Error:', data);
      }
    });
  }
});
