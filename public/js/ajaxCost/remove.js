//Funkcja usuwa wybrany koszt
var urlC_d = "http://127.0.0.1:8000/ajaxRemoveC";

$(document).on('click', '.open_modal_delete', function(){
  var costId = $(this).val();

  $.get(urlC_d + '/' + costId, function (data){
    //success data
    console.log(data);
    $('#cost_delete_id').val(data.id);
    $('#btn-delete').val("delete");
    $('#modal-danger-remove-c').modal('show');
  })
});
//usuwanie zapisanego kosztu
$('#btn-delete').click(function (e){
  var costId = $('#cost_delete_id').val();
  console.log(costId);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  $.ajax({
    type: "DELETE",
    url: urlC_d + '/' + costId,
    success: function (data) {
      console.log(data);
      $("#cost" + costId).remove();
      $('#modal-danger-remove-c').modal('hide');
    },
    error: function (data) {
      console.log('Error:', data);
    }
  });
});
