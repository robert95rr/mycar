//Funkcja oblicza wszystkie koszty w podanym przedziale czasowyn
var urlC = "http://127.0.0.1:8000/ajaxCalculate";

$("#btn-calculate").click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData = $('#frmCost').serialize();
  var carId = $('#carId').val();
  var type = "POST";
  var myUrl = urlC + '/' + carId;
  $.ajax({
    type: type,
    url: myUrl,
    data: formData,
    dataType: 'json',
    success: function (data){
      $('#title_Calc').text('od ' + data[1] + ' do ' + data[2]);
      $('#cost_Calc').text(data[0] + ' zł');
      $('#modal-primary').modal('show')
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
