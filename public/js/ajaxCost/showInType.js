//Funkcja pokazująca koszty w wybranym rodzaju
var urlCT_show = "http://127.0.0.1:8000/ajaxShowCostsType";
//show costs in type
$('#btn-showCT').click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData = $('#frmCostType').serialize();
  var carId = $('#carIdCT').val();
  var type = "POST";
  var myUrl = urlCT_show + '/' + carId;
  $.ajax({
    type: type,
    url: myUrl,
    data: formData,
    dataType: 'json',
    success: function (data) {
      var costTypeModal = "";
      if(data[0] == ""){
      var modal_c = '<center><h4>Brak kosztów!!!</h4></center>';
      $('#answer_ct').replaceWith(modal_c);
      }
      $.each(data[0], function(index){
          costTypeModal += '<li id="costsShowType"><span class="bg-navy">' + data[0][index].date + '</span><div class="timeline-item">' +
          '<span class="time"><span class="badge bg-blue"><font color="white">' + data[0][index].price + "zł" + '</font></span></span>' +
          '<h3 class="timeline-header"><font color="#006666"><b>' + data[0][index].cost_type + '</b></font></h3>' +
          '<div class="timeline-body">' +
          '<strong><i class="fa fa-map-marker margin-r-5"></i>Miejsce</strong><br>' +
          data[0][index].place + '<br><br>' +
          '<strong><i class="fa fa-pencil-square-o margin-r-5"></i>Opis</strong><br>' +
          data[0][index].description + '</div></div></li>';
      });
      $("#answer_ct").html(costTypeModal);
      var header_type = 'Koszty | ' + '<b>' + data[1] + '</b>';
      $('#h_type_cost').html(header_type);
      $('#modal-costs-type').modal('show')
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
