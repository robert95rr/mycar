//Funkcja edycji wybranego kosztu
var url = "http://127.0.0.1:8000/ajaxUpdateC";

$(document).on('click', '.open_modal', function(){
  var costId = $(this).val();
  $.get(url + '/' + costId, function (data){
    //success data
    console.log(data);
    $('#costId').val(data.id);
    $('#car_id').val(data.car_id_cost);
    $('#service_id_ajax').val(data.service_id);
    $('#cost_type_ajax').val(data.cost_type);
    $('#datepicker7').val(data.date);
    $('#price_ajax').val(data.price);
    $('#place_ajax').val(data.place);
    $('#description_ajax').val(data.description);
    $('#btn-update').val("update");
    $('#modal-default-editCost').modal('show');
  })
});

//Proces aktualizacji danego kosztu metodą AJAX
$("#btn-update").click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData = $('#updateCost').serialize();
  var costId = $('#costId').val();
  var type = "PUT";
  var myUrl = url + '/' + costId;
  $.ajax({
    type: type,
    url: myUrl,
    data: formData,
    dataType: 'json',
    success: function (data){
      var cost = '<li id="cost' + data.id + '"><span class="bg-navy">' + data.date + '</span><div class="timeline-item"><span class="time"><span class="badge bg-blue"><font color="white">' + data.price + "zł" + '</font></span></span>' +
      '<h3 class="timeline-header"><font color="#006666"><b>' + data.cost_type + '</b></font></h3>' +
      '<div class="timeline-body">' +
      '<strong><i class="fa fa-map-marker margin-r-5"></i>Miejsce</strong><br>' +
      data.place + '<br><br>' +
      '<strong><i class="fa fa-pencil-square-o margin-r-5"></i>Opis</strong><br>' +
      data.description +
      '</div><div class="timeline-footer">' +
      '<button class="btn btn-warning btn-xs open_modal" value="' + data.id +'">Edytuj</button>' +
      ' <button class="btn btn-danger btn-xs open_modal_delete" value="' + data.id + '">Usuń</button>' +
      '</div></div></li>';

      $("#cost" + costId).replaceWith(cost);
      $('#updateCost').trigger("reset");
      $('#modal-default-editCost').modal('hide')
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
