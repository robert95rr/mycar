//Funkcja pokazuje koszty poniesione w wybranym przedziale czasowym
var urlC_show = "http://127.0.0.1:8000/ajaxShowCosts";

$("#btn-showC").click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData = $('#frmCostShow').serialize();
  var carId = $('#carIdCS').val();
  var type = "POST";
  var myUrl = urlC_show + '/' + carId;
  $.ajax({
    type: type,
    url: myUrl,
    data: formData,
    dataType: 'json',
    success: function (data){
      var costModal = "";
      if(data[0] == ""){
      var modal_c = '<center><h4>Brak kosztów!!!</h4></center>';
      $('#answer_c').replaceWith(modal_c);
      }
      $.each(data[0], function(index){
          costModal += '<li id="costsShow"><span class="bg-navy">' + data[0][index].date + '</span><div class="timeline-item">' +
          '<span class="time"><span class="badge bg-blue"><font color="white">' + data[0][index].price + "zł" + '</font></span></span>' +
          '<h3 class="timeline-header"><font color="#006666"><b>' + data[0][index].cost_type + '</b></font></h3>' +
          '<div class="timeline-body">' +
          '<strong><i class="fa fa-map-marker margin-r-5"></i>Miejsce</strong><br>' +
          data[0][index].place + '<br><br>' +
          '<strong><i class="fa fa-pencil-square-o margin-r-5"></i>Opis</strong><br>' +
          data[0][index].description + '</div></div></li>';
      });
      $("#answer_c").html(costModal);
      var header_date = '<h4 class="modal-title" id="h_dates_cost">Koszty | ' + '<b>' + data[1] + '</b>' + ' do ' + '<b>' + data[2] + '</b>' + '</h4>';
      $('#h_dates_cost').replaceWith(header_date);
      $('#modal-costs-dates').modal('show')
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
