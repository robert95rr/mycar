//Funkcje pobierania i wstawiania id do formularza Usuwania

//Usuwanie Przebiegu (Mileage)
$('#modal-danger-remove-m').on('show.bs.modal', function (event) {

  var button = $(event.relatedTarget)
  var mileage_id = button.data('mileageid')
  var modal = $(this)

  modal.find('.modal-body #mileage_id').val(mileage_id);
})
//Usuwanie Paliwa (Fuel)
$('#modal-danger-remove-f').on('show.bs.modal', function (event) {

  var button = $(event.relatedTarget)
  var fuel_id = button.data('fuelid')
  var modal = $(this)

  modal.find('.modal-body #fuel_idDelete').val(fuel_id);
})
//Usuwanie Stanu bieżnika (Tread condition)
$('#modal-danger-remove-tc').on('show.bs.modal', function (event) {

  var button = $(event.relatedTarget)
  var tread_condition_id = button.data('treadconditionid')
  var modal = $(this)

  modal.find('.modal-body #tread_condition_id').val(tread_condition_id);
})
//Usuwanie Serwis planu (Service plan)
$('#modal-danger-remove-sp').on('show.bs.modal', function (event) {

  var button = $(event.relatedTarget)
  var service_plan_id = button.data('serviceplanid')
  var modal = $(this)

  modal.find('.modal-body #service_plan_id').val(service_plan_id);
})
