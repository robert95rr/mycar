//Funkcja aktualizacji wybranego serwisu
var url = "http://127.0.0.1:8000/ajaxUpdate";

$(document).on('click', '.open_modal', function(){
  var serviceId = $(this).val();
  $.get(url + '/' + serviceId, function (data) {
    //success data
    console.log(data[0]);
    console.log(data[1]);
    $('#serviceId').val(data[0].id);
    $('#car_id').val(data[0].car_id);
    $('#service_planId').val(data[0].service_plan_id);
    $('#service_plan_id_ajax').val(data[0].service_plan_id);
    $('#datepicker6').val(data[0].date_service);
    $('#mileage_ajax').val(data[0].mileage);
    $('#next_service_mileage_ajax').val(data[0].next_service_mileage);
    $('#description_ajax').val(data[0].description);
    $('#service_task').val(data[1]);
    $('#btn-update').val("update");
    $('#modal-default-editService').modal('show');
  })
});

$("#btn-update").click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData = $('#updateService').serialize();
  var serviceId = $('#serviceId').val();
  var type = "PUT";
  var myUrl = url + '/' + serviceId;
  $.ajax({
    type: type,
    url: myUrl,
    data: formData,
    dataType: 'json',
    success: function (data){
      var service_task = $('#service_task').val();
      var service_plan_id = $('#service_planId').val();
      if( service_plan_id == ""){
      var service = '<li id="serviceHistories' + data.id + '"><span class="bg-navy">' + data.date_service + '</span><div class="timeline-item">' +
          '<span class="time"><i class="fa fa-road margin-r-5"></i><font color="black">' + data.mileage + " km" + ' </font><i class="fa fa-long-arrow-right"></i> <font color="black">' + data.next_service_mileage + " km" + '</font></span>'
           + '<h3 class="timeline-header"><font color="#006666"><b>Brak planu serwisowego dla tej czynności</b></font></h3>'
            + '<div class="timeline-body">' + data.description + '</div><div class="timeline-footer">' +
           '<button class="btn btn-warning btn-xs open_modal" value="' + data.id + '">Edytuj</button>' + ' <a class="btn btn-danger btn-xs" data-serviceid="' + data.id + '" data-toggle="modal" data-target="#modal-danger-remove-s">Usuń</a></div></div></li>';}
      else{
      var service = '<li id="serviceHistories' + data.id + '"><span class="bg-navy">' + data.date_service + '</span><div class="timeline-item">' +
          '<span class="time"><i class="fa fa-road margin-r-5"></i><font color="black">' + data.mileage + " km" + ' </font><i class="fa fa-long-arrow-right"></i> <font color="black">' + data.next_service_mileage + " km" + '</font></span>' +
          '<h3 class="timeline-header"><font color="#006666"><b>' + service_task + '</b></font></h3>'
           + '<div class="timeline-body">' + data.description + '</div><div class="timeline-footer">' +
          '<button class="btn btn-warning btn-xs open_modal" value="' + data.id + '">Edytuj</button>' + ' <a class="btn btn-danger btn-xs" data-serviceid="' + data.id + '" data-toggle="modal" data-target="#modal-danger-remove-s">Usuń</a></div></div></li>';}

      $("#serviceHistories" + serviceId).replaceWith(service);
      $('#updateService').trigger("reset");
      $('#modal-default-editService').modal('hide')
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
