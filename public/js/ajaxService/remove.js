//Funkcja usuwania zapisanego serwisu
var urlR = "http://127.0.0.1:8000/ajaxRemove";

$(document).on('click', '.open_modal_delete', function(){
  var serviceId = $(this).val();

  $.get(urlR + '/' + serviceId, function (data) {
    //success data
    console.log(data);
    $('#service_delete_id').val(data.id);
    $('#btn-delete').val("delete");
    $('#modal-danger-remove-s').modal('show');
  })
});

$("#btn-delete").click(function (e){
  var serviceId = $('#service_delete_id').val();
  var car_id = $('#service_car_id').val();
  console.log(serviceId);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  $.ajax({
    type: "DELETE",
    url: urlR + '/' + serviceId + '/' + car_id,
    success: function (data) {
      console.log(data);
      $("#serviceHistories" + serviceId).remove();
      $('#modal-danger-remove-s').modal('hide');
    },
    error: function (data) {
      $('#modal-danger-remove-s').modal('hide');
      setTimeout(function(){
        $('#modal-warning-error').modal('show');
      }, 1000);

    }
  });
});
