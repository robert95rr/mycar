//Funkcja pokazania wykresu z kosztami w wybranym przedziale czasowym
var urlChartC = "http://127.0.0.1:8000/fuel/ajaxChartC";

$(document).on('click', '.open_modalChartC', function(){
  var carId = $(this).val();
  $.get(urlChartC + '/' + carId, function (data){
    //success data
    console.log(data);
    $('#datepicker6').val(data[0].date);
    $('#datepicker7').val(data[1].date);
    $('#btn-showChart').val("showChart");
    $('#modal-default-selectDates-chartCost').modal('show');
  })
});

//funkcja pokazania wykresu z kosztami paliwa
$('#btn-showChart').click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData = $('#selectDates').serialize();
  var carId = $('#car_idCost').val();
  var type = "GET";
  var urlChartC1 = "http://127.0.0.1:8000/fuel/ajaxChartC1";
  var myUrl = urlChartC1 + '/' + carId;
  var Dates = new Array();
  var Costs = new Array();
  $('#modal-default-selectDates-chartCost').modal('hide');
  $.ajax({
    type: type,
    url: myUrl,
    data: formData,
    dataType: 'json',
    success: function (data){
      for(var i = 0; i < data.length; i++)
      {
        Dates.push(data[i].date);
        Costs.push(data[i].total_cost);
      }
      var ctx = document.getElementById("barChart").getContext('2d');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: Dates,
              datasets: [{
                  label: 'Koszt paliwa',
                  data: Costs,
                  backgroundColor: 'rgba(255, 99, 132, 0.5)',
                  borderColor: 'rgba(255,99,132,1)',
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
