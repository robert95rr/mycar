//Funkcja pokazania wykresu z użyciami paliwa w wybranym zakresie czasowym
var urlChartU = "http://127.0.0.1:8000/fuel/ajaxChartU";

$(document).on('click', '.open_modalChartU', function(){
  var carId = $(this).val();
  $.get(urlChartU + '/' + carId, function (data){
    //success data
    $('#datepicker9').val(data[0].date);
    $('#datepicker8').val(data[1].date);
    $('#btn-showChartU').val("showChartU");
    $('#modal-default-selectDates-chartUsage').modal('show');
  })
});

$('#btn-showChartU').click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData = $('#selectDates1').serialize();
  var carId = $('#car_idUsage').val();
  var type = "GET";
  var urlChartU1 = "http://127.0.0.1:8000/fuel/ajaxChartU1";
  var myUrl = urlChartU1 + '/' + carId;
  var Dates = new Array();
  var Usages = new Array();
  $('#modal-default-selectDates-chartUsage').modal('hide');
  $.ajax({
    type: type,
    url: myUrl,
    data: formData,
    dataType: 'json',
    success: function (data){
      console.log(data);
      for(var i = 0; i < data.length; i++)
      {
        Dates.push(data[i].date);
        Usages.push(data[i].fuel_usage);
      }
      var ctx = document.getElementById("barChart1").getContext('2d');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: Dates,
              datasets: [{
                  label: 'Zużycie paliwa',
                  data: Usages,
                  backgroundColor: 'rgba(54, 162, 235, 0.5)',
                  borderColor: 'rgba(54, 162, 235, 1)',
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
