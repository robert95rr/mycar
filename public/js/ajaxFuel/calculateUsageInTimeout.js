//Funkcja oblicza średnie zużycie paliwa w przedziale czasowym
var url2 = "http://127.0.0.1:8000/ajaxCalculateMU";

$('#btn-calculateMU').click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData = $('#frmCalculateMediumUsed').serialize();
  var carId = $('#carIdMU').val();
  var type = "POST";
  var myUrl = url2 + '/' + carId;
  $.ajax({
    type: type,
    url: myUrl,
    data: formData,
    dataType: 'json',
    success: function (data){
      $('#used_CalcMU').text(data[0] + ' l/100 km');
      $('#date_CalcMU').text('od ' + data[1] + ' do ' + data[2]);
      $('#modal-calculateMU').modal('show')
    },
    error: function (data) {
      console.log('Error:', data);
    }
  });
});
