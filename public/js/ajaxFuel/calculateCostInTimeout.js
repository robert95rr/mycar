//Funkcja oblicza koszt paliwa w przedziale czasowym
var url = "http://127.0.0.1:8000/ajaxCalculateFS";

$('#btn-calculateFS').click(function (e){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  e.preventDefault();
  var formData = $('#frmCalculateCost').serialize();
  var carId = $('#carIdFS').val();
  var type = "POST";
  var myUrl = url + '/' + carId;
  $.ajax({
    type: type,
    url: myUrl,
    data: formData,
    dataType: 'json',
    success: function (data){
      $('#title_CalcFS').text('od ' + data[1] + ' do ' + data[2]);
      $('#cost_CalcFS').text(data[0] + ' zł');
      $('#modal-calculateFS').modal('show')
    },
    error: function (data){
      console.log('Error:', data);
    }
  });
});
