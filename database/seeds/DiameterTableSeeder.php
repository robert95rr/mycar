<?php

use Illuminate\Database\Seeder;

class DiameterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('diameter')->insert([
           'value' => '10'
       ]);

       DB::table('diameter')->insert([
            'value' => '12'
        ]);

        DB::table('diameter')->insert([
             'value' => '13'
         ]);

         DB::table('diameter')->insert([
              'value' => '14'
          ]);

          DB::table('diameter')->insert([
               'value' => '15'
           ]);

           DB::table('diameter')->insert([
                'value' => '16'
            ]);

            DB::table('diameter')->insert([
                 'value' => '17'
             ]);

             DB::table('diameter')->insert([
                  'value' => '18'
              ]);

              DB::table('diameter')->insert([
                   'value' => '19'
               ]);

               DB::table('diameter')->insert([
                    'value' => '20'
                ]);

                DB::table('diameter')->insert([
                     'value' => '21'
                 ]);

                 DB::table('diameter')->insert([
                      'value' => '22'
                  ]);

                  DB::table('diameter')->insert([
                       'value' => '23'
                   ]);

                   DB::table('diameter')->insert([
                        'value' => '24'
                    ]);

                    DB::table('diameter')->insert([
                         'value' => '26'
                     ]);

                     DB::table('diameter')->insert([
                          'value' => '28'
                      ]);

                      DB::table('diameter')->insert([
                           'value' => '30'
                       ]);
    }
}
