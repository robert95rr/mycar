<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('type')->insert([
           'name' => 'Letnie'
       ]);

       DB::table('type')->insert([
            'name' => 'Zimowe'
        ]);

        DB::table('type')->insert([
             'name' => 'Całoroczne'
         ]);
    }
}
