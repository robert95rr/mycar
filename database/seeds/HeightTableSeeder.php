<?php

use Illuminate\Database\Seeder;

class HeightTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('height')->insert([
           'value' => '20'
       ]);

       DB::table('height')->insert([
            'value' => '25'
        ]);

        DB::table('height')->insert([
             'value' => '30'
         ]);

         DB::table('height')->insert([
              'value' => '35'
          ]);

          DB::table('height')->insert([
               'value' => '40'
           ]);

           DB::table('height')->insert([
                'value' => '45'
            ]);

            DB::table('height')->insert([
                 'value' => '50'
             ]);

             DB::table('height')->insert([
                  'value' => '55'
              ]);

              DB::table('height')->insert([
                   'value' => '60'
               ]);

               DB::table('height')->insert([
                    'value' => '65'
                ]);

                DB::table('height')->insert([
                     'value' => '70'
                 ]);

                 DB::table('height')->insert([
                      'value' => '75'
                  ]);

                  DB::table('height')->insert([
                       'value' => '80'
                   ]);

                   DB::table('height')->insert([
                        'value' => '85'
                    ]);

                    DB::table('height')->insert([
                         'value' => '610'
                     ]);

                     DB::table('height')->insert([
                          'value' => '625'
                      ]);

                      DB::table('height')->insert([
                           'value' => '665'
                       ]);
    }
}
