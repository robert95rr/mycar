<?php

use Illuminate\Database\Seeder;

class WidthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('width')->insert([
           'value' => '115'
       ]);

       DB::table('width')->insert([
            'value' => '125'
        ]);

        DB::table('width')->insert([
             'value' => '135'
         ]);

         DB::table('width')->insert([
              'value' => '145'
          ]);

          DB::table('width')->insert([
               'value' => '155'
           ]);

           DB::table('width')->insert([
                'value' => '165'
            ]);

            DB::table('width')->insert([
                 'value' => '175'
             ]);

             DB::table('width')->insert([
                  'value' => '185'
              ]);

              DB::table('width')->insert([
                   'value' => '195'
               ]);

               DB::table('width')->insert([
                    'value' => '205'
                ]);

                DB::table('width')->insert([
                     'value' => '210'
                 ]);

                 DB::table('width')->insert([
                      'value' => '215'
                  ]);

                  DB::table('width')->insert([
                       'value' => '225'
                   ]);

                   DB::table('width')->insert([
                        'value' => '230'
                    ]);

                    DB::table('width')->insert([
                         'value' => '235'
                     ]);

                     DB::table('width')->insert([
                          'value' => '245'
                      ]);

                      DB::table('width')->insert([
                           'value' => '255'
                       ]);

                       DB::table('width')->insert([
                            'value' => '265'
                        ]);

                        DB::table('width')->insert([
                             'value' => '275'
                         ]);

                         DB::table('width')->insert([
                              'value' => '285'
                          ]);

    }
}
