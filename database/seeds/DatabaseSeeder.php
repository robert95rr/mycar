<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(Vehicle_typeTableSeeder::class);
        $this->call(TypeTableSeeder::class);
        $this->call(WidthTableSeeder::class);
        $this->call(HeightTableSeeder::class);
        $this->call(DiameterTableSeeder::class);
    }
}
