<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Vehicle_typeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('vehicle_types')->insert([
           'name' => 'Kabriolet'
       ]);

       DB::table('vehicle_types')->insert([
            'name' => 'Sedan'
        ]);

        DB::table('vehicle_types')->insert([
             'name' => 'Coupe'
         ]);

         DB::table('vehicle_types')->insert([
              'name' => 'Pickup'
          ]);

          DB::table('vehicle_types')->insert([
               'name' => 'Hatchback'
           ]);

           DB::table('vehicle_types')->insert([
                'name' => 'Kombi'
            ]);

            DB::table('vehicle_types')->insert([
                 'name' => 'Terenowy'
             ]);

             DB::table('vehicle_types')->insert([
                  'name' => 'Minibus'
              ]);

              DB::table('vehicle_types')->insert([
                   'name' => 'Minivan'
               ]);

               DB::table('vehicle_types')->insert([
                    'name' => 'SUV'
                ]);



    }
}
