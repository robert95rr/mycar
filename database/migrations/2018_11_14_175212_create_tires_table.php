<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tires', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->integer('width_id')->unsigned();
            $table->integer('height_id')->unsigned();
            $table->integer('diameter_id')->unsigned();

            $table->string('manufacturer');//producent
            $table->string('model');//model
            $table->string('dot');//rok i tydzien produkcji
            $table->date('date_put');//data załozenia

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('tires', function(Blueprint $table) {
            $table->foreign('car_id')
                ->references('id')
                ->on('cars');
        });

        Schema::table('tires', function(Blueprint $table) {
            $table->foreign('type_id')
                ->references('id')
                ->on('type');
        });

        Schema::table('tires', function(Blueprint $table) {
            $table->foreign('width_id')
                ->references('id')
                ->on('width');
        });

        Schema::table('tires', function(Blueprint $table) {
            $table->foreign('height_id')
                ->references('id')
                ->on('height');
        });

        Schema::table('tires', function(Blueprint $table) {
            $table->foreign('diameter_id')
                ->references('id')
                ->on('diameter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tires');
    }
}
