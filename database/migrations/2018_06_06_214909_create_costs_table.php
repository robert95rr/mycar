<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id_cost')->unsigned()->nullable();
            $table->integer('service_id')->unsigned()->nullable();

            $table->string('cost_type');
            $table->decimal('price', 7, 2);
            $table->string('place');
            $table->date('date');
            $table->text('description');


            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('costs', function(Blueprint $table) {
            $table->foreign('service_id')
                ->references('id')
                ->on('service');
        });

        Schema::table('costs', function(Blueprint $table) {
            $table->foreign('car_id_cost')
                ->references('id')
                ->on('cars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costs');
    }
}
