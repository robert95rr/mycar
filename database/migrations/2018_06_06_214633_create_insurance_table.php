<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id')->unsigned();

            $table->string('company_name');
            $table->string('product_name');
            $table->string('insurance_type');
            $table->string('policy_number');
            $table->date('period_from');
            $table->date('period_to');
            $table->decimal('price', 6, 2);
            $table->integer('mileage');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('insurance', function(Blueprint $table) {
           $table->foreign('car_id')
               ->references('id')
               ->on('cars');
       });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance');
    }
}
