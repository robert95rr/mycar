<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('vehicle_type_id')->unsigned();

            $table->string('make');
            $table->string('model');
            $table->year('year_production');
            $table->string('engine_size');
            $table->string('engine_power');
            $table->string('fuel_type');
            $table->string('registration_number');
            $table->string('vin_number');
            $table->string('interior_colour');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('cars', function(Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });

        Schema::table('cars', function(Blueprint $table) {
            $table->foreign('vehicle_type_id')
                ->references('id')
                ->on('vehicle_types');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
