<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id')->unsigned();

            $table->integer('amount_before');
            $table->integer('amount_refueled');
            $table->decimal('refueling_price', 4, 2);
            $table->integer('mileage');
            $table->date('date');
            $table->decimal('total_cost', 5, 2);
            $table->decimal('fuel_usage', 4, 2);
            $table->integer('mil_driven');
            $table->string('place');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('fuel', function(Blueprint $table) {
            $table->foreign('car_id')
                ->references('id')
                ->on('cars');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel');
    }
}
