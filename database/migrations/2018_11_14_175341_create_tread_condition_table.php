<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreadConditionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tread_condition', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tires_id')->unsigned();

            $table->string('name');
            $table->string('dot');
            $table->decimal('start_tread', 3, 1);
            $table->decimal('end_tread', 3, 1);
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->integer('start_mileage');
            $table->integer('end_mileage');
            $table->integer('diff_mileage');
            $table->string('tire_position');
            $table->string('season');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('tread_condition', function(Blueprint $table) {
            $table->foreign('tires_id')
                ->references('id')
                ->on('tires');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tread_condition');
    }
}
