<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id')->unsigned();
            $table->integer('service_plan_id')->unsigned()->nullable();

            $table->date('date_service');
            $table->integer('mileage');
            $table->integer('next_service_mileage');
            $table->text('description');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('service', function(Blueprint $table) {
            $table->foreign('car_id')
                ->references('id')
                ->on('cars');
        });

        Schema::table('service', function(Blueprint $table) {
            $table->foreign('service_plan_id')
                ->references('id')
                ->on('service_plan');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service');
    }
}
