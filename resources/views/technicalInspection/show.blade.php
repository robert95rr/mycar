@extends('layouts.app')

@section('sidebar')

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form>
    <!-- /.search form -->
    @guest
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">HEADER</li>
      <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
          <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#">Link in level 2</a></li>
              <li><a href="#">Link in level 2</a></li>
            </ul>
          </li>
        </ul>
        @else
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MENU</li>
              <li class="active treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Auta</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li class="treeview">
                    <a href="#"> Moje Auta
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                      <ul class="treeview-menu">
                        @foreach ($car as $cars)
                        <li><a href="{{ route('showCar', $cars->id) }}"><i class="fa fa-dot-circle-o"></i>{{$cars->make}}&nbsp;{{$cars->model}}</a></li>
                        @endforeach
                      </ul>
                  </li>
                    <li><a href="{{ route('addFormCar') }}">Dodaj</a></li>
                </ul>
              </li>
              <li class="header"></li>
              <li>
                <a href="{{ route('showMileage', $car_one->id) }}">
                    <i class="fa fa-road"></i> <span>Przebieg</span><!--Przebieg auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showFuel', $car_one->id) }}">
                  <i class="fa fa-tint"></i> <span>Paliwo</span><!--Paliwo auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showInsurance', $car_one->id) }}">
                  <i class="fa fa-shield"></i> <span>Ubezpieczenie</span><!--Ubezpieczenie auta-->
                </a>
              </li>
              <li class="treeview">
              <a href="#">
                <i class="fa fa-dot-circle-o"></i>
                <span>Ogumienie</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @foreach ($tire as $tires)
                <li ><a href="{{ route('showTire', [$tires->id, $car_one->id]) }}"><i class="fa fa-circle-o"></i>{{$tires->manufacturer}}&nbsp;{{$tires->model}}
                  @if($tires->type_id == 1)
                  <small class="label pull-right bg-yellow">Letnie</small>
                  @elseif($tires->type_id == 2)
                  <small class="label pull-right bg-blue">Zimowe</small>
                  @else
                  <small class="label pull-right bg-green">Całoroczne</small>
                  @endif
                  </a></li>
                @endforeach
                <li><a href="{{ route('addFormTire', $car_one->id) }}">Dodaj</a></li><!--Ogumienie auta-->
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-wrench"></i>
                <span>Serwis</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('showService', [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Serwis Auta</a></li>
                <li><a href="{{ route('showServicePlan', [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Plan Serwisowy</a></li>
              </ul>
            </li><!--Serwis auta-->
              <li>
                <a href="{{ route('showCosts', $car_one->id) }}">
                  <i class="fa fa-usd"></i> <span>Koszty</span><!--Koszty auta-->
                </a>
              </li>
              <li class="header">POWIADOMIENIA</li>
              <li>
                <a href="{{ route('showTechnicalInspection', $car_one->id) }}">
                  <i class="fa fa-stethoscope"></i><span class="label bg-olive"> Przegląd techniczny </span>
                </a>
              </li>
        </ul>
          @endguest

@endsection

@section('content_header')
      <h1>
       {{$car_one->make}}&nbsp;{{$car_one->model}} <small>| Przegląd techniczny</small>
     </h1>
     <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('showCar', $car_one->id) }}">{{$car_one->make}}&nbsp;{{$car_one->model}}</a></li>
        <li class="active">Przegląd techniczny</li>
      </ol><br/>
@endsection


@section('content')
<div class="col-md-6 col-md-offset-3">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">
        <i class="fa fa-stethoscope"></i>
        Przegląd techniczny
      </h3>
      <div class="btn-group pull-right">
        <input type="hidden" id="car_id" name="car_id" value="{{$car_one->id}}">
        <button type="button" class="btn btn-primary btn-sm open_modalAdd"><i class="fa fa-bullhorn"></i> Powiadomienia</button>
        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <li class="dropdown-header"><b>Aplikacja</b></li>
          <li><a href="#" data-toggle="modal" data-target="#modal-default-addNotification">Dodaj</a></li>
          <li><a href="showN" id="showNTI">Pokaż</a></li>
          <li class="dropdown-header"><b>Email</b></li>
          <li><a href="#" data-toggle="modal" data-target="#modal-default-sendNotification">Wyślij</a></li>
        </ul>
      </div>
    </div>
    <div class="box-body"><br>
      <div class="col-md-10 col-md-offset-1" id="block_notificationsTechnicalInspection">
      </div>
    </div>
  </div>
</div>
<!--FORMS-->
@include('notifications.technicalInspection.forms.addform')
@include('notifications.technicalInspection.forms.editform')
@include('notifications.technicalInspection.forms.sendform')
@include('notifications.noDataModal')
@include('notifications.remove')
<!--JavaScript-->
<script src="{{ asset("adminlte/jquery/dist/jquery.min.js") }}"></script>
@if(session('status'))
@include('notifications.successModal')
<script type="text/javascript">
$(document).ready(function(){
  setTimeout(function(){
    $('#modal-positiveFeedback').modal('show');
  }, 1000);
  setTimeout(function(){
    $('#modal-positiveFeedback').modal('hide');
  }, 3000);
});
</script>
@endif
<script src="{{ asset("js/ajaxTechnicalInspection/notifications/shownotifications.js") }}"></script>
<script src="{{ asset("js/ajaxTechnicalInspection/notifications/update.js") }}"></script>
<script src="{{ asset("js/ajaxNotification/remove.js") }}"></script>
@endsection
