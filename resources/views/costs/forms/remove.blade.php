<!--Usuwanie kosztu w oknie modalnyms-->
    <div class="modal modal-danger fade" id="modal-danger-remove-c">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title center">Usuwanie kosztu</h4>
                  </div>
                  <form id="delete_cost" name="delete_cost">
                  {{ csrf_field() }}
                  <div class="modal-body">
                    <p class="center">Czy na pewno chcesz usunąć ten koszt ?</p>
                      <input type="hidden" name="cost_delete_id" id="cost_delete_id" value="">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Nie</button>
                    <button class="btn btn-outline" id="btn-delete" value="delete">Tak</button>
                  </div>
                </form>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
