<!--Formularz dodawania w oknie modalnym-->
<div class="modal fade" id="modal-default-add-c">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Dodawanie Kosztu</h4>
      </div>
      <form method="post" action="{{ route('addCost', $car_one->id) }}" enctype="multipart/form-data">
      @csrf
      <div class="modal-body">
        <div class="row">
<!--Pole - Czynność serwisowa (opcjonalnie *)-->
            <div class="col-xs-11" data-toggle="tooltip" data-placement="top" title="Czynność serwisowa">
              <select id="service_idAdd" class="form-control" name="service_id">
                <option value="NULL" label="Nie wybrano"></option>
                    @foreach ($serviceNotToCosts as $service)
                        <option value="{{$service->id}}">{{$service->description}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$service->date_service}}</option>
                    @endforeach
              </select>
              @if ($errors->has('service_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('service_id') }}</strong>
                  </span>
              @endif
            </div><div class="col-xs-1"><b>*</b></div>
          </div><br>
<!--Pole - Rodzaj kosztu-->
<div class="row">
          <div class="col-xs-6"  data-toggle="tooltip" data-placement="top" title="Rodzaj kosztu">
            <select id="cost_typeAdd" class="form-control" name="cost_type" required>
              <option label="Wybierz..."></option>
              <option>Badanie techniczne</option>
              <option>Kosmetyka</option>
              <option>Kupno auta</option>
              <option>Mandat</option>
              <option>Naprawa eksploatacyjna</option>
              <option>Naprawa powypadkowa</option>
              <option>Ogumienie</option>
              <option>Parking</option>
              <option>Rejestracja</option>
              <option>Inny</option>
            </select>
            @if ($errors->has('cost_type'))
                <span class="help-block">
                    <strong>{{ $errors->first('cost_type') }}</strong>
                </span>
            @endif
        </div>
<!--Pole - Data kosztu-->
          <div class="col-xs-6">
            <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Data kosztu">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            <input type="text" id="datepicker1" class="form-control pull-right" name="date" data-date-format = "yyyy-mm-dd" placeholder = "____-__-__" required>
              </div>
            @if ($errors->has('date'))
                <span class="help-block">
                    <strong>{{ $errors->first('date') }}</strong>
                </span>
            @endif
        </div>
      </div><br>
<!--Pole - Miejsce kosztu (Adres serwisu) -->
      <div class="row">
        <div class="col-xs-4">
          <div class="input-group">
          <input type="text" id="priceAdd" class="form-control" name="price" placeholder="Cena" required>
          <span class="input-group-addon">zł</span>
        </div>
          @if ($errors->has('price'))
                    <span class="help-block">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
          @endif
        </div>
<!--Pole - Miejsce kosztu (Adres serwisu) -->
      <div class="col-xs-8">
        <div class="input-group">
          <input type="text" id="placeAdd" class="form-control" name="place" placeholder = "Miejsce" required>
          <div class="input-group-addon">
            <i class="fa fa-map-marker"></i>
          </div>
        </div>
        @if ($errors->has('place'))
            <span class="help-block">
                <strong>{{ $errors->first('place') }}</strong>
            </span>
        @endif
      </div>
    </div><br>
<!--Pole - Opis kosztu-->
      <div class="row">
        <div class="col-xs-12">
          <textarea class="form-control" id="descriptionAdd" name="description" rows="3" placeholder="Opis..." required></textarea>
          @if ($errors->has('description'))
              <span class="help-block">
                  <strong>{{ $errors->first('description') }}</strong>
              </span>
          @endif
        </div>
      </div><br>
      <p class="help-block"><b>*</b>&nbsp; - pole nieobowiązkowe.</p>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Dodaj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
