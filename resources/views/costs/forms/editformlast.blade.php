<!--Formularz edycji ostatniego kosztu-->
<div class="modal fade" id="modal-default-edit-cl">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edycja Ostatniego Kosztu</h4>
      </div>
      <form method="post" action="{{ route('updateCostL', [$cost_last->id, $car_one->id]) }}" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="modal-body">
        <div class="row">
<!--Pole - Czynność serwisowa (opcjonalnie *)-->
            <div class="col-xs-11" data-toggle="tooltip" data-placement="top" title="Czynność serwisowa">
              <select id="service_id" class="form-control" name="service_id" required>
                <option value="NULL" label="Brak" @if(old('service_id', $cost_last->service_id) == NULL) selected @endif></option>
                    @foreach ($services as $service)
                        <option value="{{$service->id}}" @if(old('service_id', $cost_last->service_id) == $service->id ) selected @endif> {{$service->description}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$service->date_service}}</option>
                    @endforeach
              </select>
              @if ($errors->has('service_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('service_id') }}</strong>
                  </span>
              @endif
            </div><div class="col-xs-1"><b>*</b></div>
          </div><br>
<!--Pole - Rodzaj kosztu-->
<div class="row">
          <div class="col-xs-6"  data-toggle="tooltip" data-placement="top" title="Rodzaj kosztu">
            <select id="cost_type" class="form-control" name="cost_type" required>
              <option label="Wybierz..."></option>
              <option @if(old('cost_type',$cost_last->cost_type) == 'Badanie techniczne') selected @endif>Badanie techniczne</option>
              <option @if(old('cost_type',$cost_last->cost_type) == 'Kosmetyka') selected @endif>Kosmetyka</option>
              <option @if(old('cost_type',$cost_last->cost_type) == 'Kupno auta') selected @endif>Kupno auta</option>
              <option @if(old('cost_type',$cost_last->cost_type) == 'Mandat') selected @endif>Mandat</option>
              <option @if(old('cost_type',$cost_last->cost_type) == 'Naprawa eksploatacyjna') selected @endif>Naprawa eksploatacyjna</option>
              <option @if(old('cost_type',$cost_last->cost_type) == 'Naprawa powypadkowa') selected @endif>Naprawa powypadkowa</option>
              <option @if(old('cost_type',$cost_last->cost_type) == 'Ogumienie') selected @endif>Ogumienie</option>
              <option @if(old('cost_type',$cost_last->cost_type) == 'Parking') selected @endif>Parking</option>
              <option @if(old('cost_type',$cost_last->cost_type) == 'Rejestracja') selected @endif>Rejestracja</option>
              <option @if(old('cost_type',$cost_last->cost_type) == 'Inny') selected @endif>Inny</option>
            </select>
            @if ($errors->has('cost_type'))
                <span class="help-block">
                    <strong>{{ $errors->first('cost_type') }}</strong>
                </span>
            @endif
        </div>
<!--Pole - Data kosztu-->
          <div class="col-xs-6">
            <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Data kosztu">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            <input type="text" id="datepicker6" class="form-control pull-right" name="date" data-date-format = "yyyy-mm-dd" required value="{{ old('date') ?: $cost_last->date }}">
              </div>
            @if ($errors->has('date'))
                <span class="help-block">
                    <strong>{{ $errors->first('date') }}</strong>
                </span>
            @endif
        </div>
      </div><br>
<!--Pole - Cena-->
      <div class="row">
        <div class="col-xs-4">
          <div class="input-group">
          <input type="text" id="price" class="form-control" name="price" placeholder="Cena" required value="{{ old('price') ?: $cost_last->price }}">
          <span class="input-group-addon">zł</span>
        </div>
          @if ($errors->has('price'))
                    <span class="help-block">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
          @endif
        </div>
<!--Pole - Miejsce kosztu (Adres serwisu) -->
      <div class="col-xs-8">
        <div class="input-group">
          <input type="text" id="place" class="form-control" name="place" placeholder = "Miejsce" required value="{{ old('place') ?: $cost_last->place }}">
          <div class="input-group-addon">
            <i class="fa fa-map-marker"></i>
          </div>
        </div>
        @if ($errors->has('place'))
            <span class="help-block">
                <strong>{{ $errors->first('place') }}</strong>
            </span>
        @endif
      </div>
    </div><br>
<!--Pole - Opis kosztu-->
      <div class="row">
        <div class="col-xs-12">
          <textarea class="form-control" id="description" name="description" rows="3" required>{{ old('description') ?: $cost_last->description }}</textarea>
          @if ($errors->has('description'))
              <span class="help-block">
                  <strong>{{ $errors->first('description') }}</strong>
              </span>
          @endif
        </div>
      </div><br>
      <p class="help-block"><b>*</b>&nbsp; - pole nieobowiązkowe.</p>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Aktualizuj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
