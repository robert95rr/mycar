<!--Usuwanie ostaniego kosztu-->
<div class="modal modal-danger fade" id="modal-danger-remove-cl">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title center">Usuwanie ostatniego kosztu</h4>
              </div>
              <div class="modal-body">
                <p class="center">Czy na pewno chcesz usunąć ten koszt ?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Nie</button>
                <a class="btn btn-outline" href="{{ route('removeCostL', $cost_last->id) }}">Tak</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
