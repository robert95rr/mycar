<div class="box-header with-border">
  <i class="fa fa-usd"></i>
  <h3 class="box-title">Koszty
  </h3>
</div>
<!-- /.box-header -->
<div class="box-body">
  <ul class="timeline timeline-inverse">
    @foreach($costs as $cost)
    <!-- /.timeline-label -->
    <!-- timeline item -->
    <li id="cost{{$cost->id}}">
      <span class="bg-navy">
        {{$cost->date}}
      </span>

      <div class="timeline-item">
        <span class="time"><span class="badge bg-blue">{{$cost->price}} zł</span></span>
        <h3 class="timeline-header"><b class="changeColor" @foreach ($service_to_cost as $service)
          @if($cost->service_id == $service->id)
          data-toggle="tooltip" data-placement="right" title="Serwis: {{$service->description}}"
          @endif
          @endforeach>{{$cost->cost_type}}</b></h3>
        <div class="timeline-body">
          <strong><i class="fa fa-map-marker margin-r-5"></i>Miejsce</strong><br>
          {{$cost->place}}<br><br>
          <strong><i class="fa fa-pencil-square-o margin-r-5"></i>Opis</strong><br>
          {{$cost->description}}
        </div>
        <div class="timeline-footer">
          <button class="btn btn-warning btn-xs open_modal" value="{{$cost->id}}">Edytuj</button>
          <button class="btn btn-danger btn-xs open_modal_delete" value="{{$cost->id}}">Usuń</button>
        </div>
      </div>
    </li>
    <!-- END timeline item -->
    @endforeach
  </ul>
  @if(count($costs) > 0)
  {{$costs->links()}}
  @endif
</div>
<!-- /.box-body -->
