@extends('layouts.app')

@section('sidebar')

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form>
    <!-- /.search form -->
    @guest
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">HEADER</li>
      <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
          <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#">Link in level 2</a></li>
              <li><a href="#">Link in level 2</a></li>
            </ul>
          </li>
        </ul>
        @else
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">HEADER</li>
              <li class="active treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Auta</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li class="treeview">
                    <a href="#"> Moje Auta
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                      <ul class="treeview-menu">
                        @foreach ($car as $cars)
                        <li><a href="{{ route('showCar', $cars->id) }}"><i class="fa fa-dot-circle-o"></i>{{$cars->make}}&nbsp;{{$cars->model}}</a></li>
                        @endforeach
                      </ul>
                  </li>
                    <li><a href="{{ route('addFormCar') }}">Dodaj</a></li>
                </ul>
              </li>
              <li class="header"></li>
              <li>
                <a href="{{ route('showMileage', $car_one->id) }}">
                    <i class="fa fa-road"></i> <span>Przebieg</span><!--Przebieg auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showFuel', $car_one->id) }}">
                  <i class="fa fa-tint"></i> <span>Paliwo</span><!--Paliwo auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showInsurance', $car_one->id) }}">
                  <i class="fa fa-shield"></i> <span>Ubezpieczenie</span><!--Ubezpieczenie auta-->
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-dot-circle-o"></i>
                  <span>Ogumienie</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @foreach ($tire as $tires)
                  <li><a href="{{ route('showTire', [$tires->id, $car_one->id]) }}"><i class="fa fa-circle-o"></i>{{$tires->manufacturer}}&nbsp;{{$tires->model}}
                    @if($tires->type_id == 1)
                    <small class="label pull-right bg-yellow">Letnie</small>
                    @elseif($tires->type_id == 2)
                    <small class="label pull-right bg-blue">Zimowe</small>
                    @else
                    <small class="label pull-right bg-green">Całoroczne</small>
                    @endif
                    </a></li>
                  @endforeach
                  <li><a href="{{ route('addFormTire', $car_one->id) }}">Dodaj</a></li>
                </ul>
              </li><!--Ogumienie auta-->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-wrench"></i>
                  <span>Serwis</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{ route('showService',  [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Serwis Auta</a></li>
                  <li><a href="{{ route('showServicePlan',  [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Plan Serwisowy</a></li>
                </ul>
              </li><!--Serwis auta-->
              <li>
                <a href="{{ route('showCosts', $car_one->id) }}">
                  <i class="fa fa-usd"></i><span>Koszty</span><!--Koszty auta-->
                </a>
              </li>
              <li class="header">POWIADOMIENIA</li>
              <li>
                <a href="{{ route('showTechnicalInspection', $car_one->id) }}">
                  <i class="fa fa-stethoscope"></i><span class="label bg-olive"> Przegląd techniczny </span>
                </a>
              </li>
        </ul>
          @endguest

@endsection

@section('content_header')
      <h1>
        {{$car_one->make}}&nbsp;{{$car_one->model}} <small>| Koszty</small></h1><!--Nazwa auta (selected)-->
     <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('showCar', $car_one->id) }}">{{$car_one->make}}&nbsp;{{$car_one->model}}</a></li>
        <li class="active">Koszty</li>
      </ol><br/>
@endsection

@section('content')

    <div class="row">
      <!--Filtr - Suma wydatków (zakres czasowy)-->
      <div class="col-md-5 col-md-offset-1">
        <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Koszt wydatków w przedziale czasowym</h3>
                </div>
                <form class="form-horizontal" id="frmCost" name="frmCost">
                @csrf
                <div class="box-body">
                  <input type="hidden" id="carId" name="carId" value="{{$car_one->id}}">
                  <div class="row">
                    <div class="col-xs-4">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                      <input type="text" id="datepicker2" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date2" placeholder="____-__-__" >
                      </div>
                        @if ($errors->has('date2'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date2') }}</strong>
                                </span>
                        @endif
                      </div>
                    <div class="col-xs-4">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                      <input type="text" id="datepicker3" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date3" value=" " >
                      </div>
                        @if ($errors->has('date3'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date3') }}</strong>
                                </span>
                        @endif
                  </div>
                  <div class="col-xs-2">
                      <button type="button" class="btn btn-primary btn-sm" id="btn-calculate" value="calculate">Oblicz</button>
                  </div>
                    </div>
                  </div>
                </form>
                </div>
                <!-- /.box-body -->
    </div>

    <div class="col-md-5">
        <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Widok z kosztami w przedziale czasowym</h3>
                </div>
                <form class="form-horizontal" id="frmCostShow" name="frmCostShow">
                @csrf
                <div class="box-body">
                  <input type="hidden" id="carIdCS" name="carIdCS" value="{{$car_one->id}}">
                  <div class="row">
                    <div class="col-xs-4">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                      <input type="text" id="datepicker4" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date_2" placeholder="____-__-__" >
                      </div>
                        @if ($errors->has('date_2'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date_2') }}</strong>
                                </span>
                        @endif
                      </div>
                    <div class="col-xs-4">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                      <input type="text" id="datepicker5" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date_3" value=" " >
                      </div>
                        @if ($errors->has('date_3'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date_3') }}</strong>
                                </span>
                        @endif
                  </div>
                  <div class="col-xs-2">
                      <button type="submit" class="btn btn-primary btn-sm" id="btn-showC" value="showC">Pokaż</button>
                  </div>
                    </div>
                  </div>
                </form>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
  </div><br>

      <div class="row">
        <div class="col-md-4">
    @if($findCostType != '[]')
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Widok z kosztami w wybranym rodzaju</h3>
          </div>
          <form class="form-horizontal" id="frmCostType" name="frmCostType">
            @csrf
            <div class="body">
              <input type="hidden" id="carIdCT" name="carIdCT" value="{{$car_one->id}}">
              <br>
              <div class="row">
                <div class="col-xs-7 col-md-offset-1">
                  <select class="form-control" id="selectType" name="selectType">
                    @foreach ($findCostType as $findCosts)
                      <option>{{$findCosts}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-xs-2">
                  <button type="submit" class="btn btn-primary btn-sm" id="btn-showCT" value="showCT">Pokaż</button>
                </div>
              </div><br>
            </div>
          </form>
        </div><br><br>
    @endif

        <!--Pudełko_1 - Ostatni zapisany koszt-->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-usd"></i>
              <h3 class="box-title">Ostatni koszt</h3>
              <button class="btn bg-olive btn-sm pull-right" data-toggle="modal" data-target="#modal-default-add-c"><i class="fa fa-plus"></i> Dodaj</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @isset($cost_last)
              <div class="col-md-12">
                <div class="box box-danger box-solid">
                  <div class="box-header with-border">
                    <h3 class="box-title">{{$cost_last->cost_type}}</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <strong><i class="fa fa-calendar margin-r-5"></i> Data</strong>
                    <p class="text-muted col">
                      {{$cost_last->date}}
                    </p>
                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Miejsce</strong>
                    <p class="text-muted col">
                      {{$cost_last->place}}
                    </p>
                    <hr>
                    <strong><i class="fa fa-money margin-r-5"></i> Cena</strong>
                    <p class="text-muted">
                      <span class="label label-warning bl">{{$cost_last->price}} zł</span>
                    </p>
                    <hr>
                    <strong><i class="fa fa-pencil-square-o margin-r-5"></i> Opis</strong>
                    <p class="text-muted col">
                      {{$cost_last->description}}
                    </p>
                    <hr>
                    <div class="pull-right">
                      <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-default-edit-cl">Edytuj</button>
                      <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-danger-remove-cl">Usuń</button>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
              <!-- /.col -->
              @endisset
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

      </div><!-- /.col -->

        <!--Pudełko_2 - Historia zapisanych kosztów-->
    @if($findCostType != '[]')
      <div class="col-md-8">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-pie-chart"></i> Koszty według rodzaju</h3>
          </div>
          <div class="box-body">
            <canvas id="pieChart" style="height: 100px; width: 300px;"></canvas>
          </div>
        </div>
          <div class="box box-primary" id="timeline_costs">
            @include('costs.pagination_list')
          </div>
          <!-- /.box -->
      </div>
      <!-- /.col -->
    @endif

    </div>

<!--Window modal with all cost in time-->
  <div class="modal modal-danger fade" id="modal-primary">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title center">Koszt wydatków</h4>
          </div>
          <div class="modal-body">
            <h3 id="cost_Calc">Koszt zł</h3>
            <hr>
            <p id="title_Calc">data</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Ok</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

  <!--Window modal with costs in time-->
    <div class="modal fade" id="modal-costs-dates">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="h_dates_cost">Koszty</h4>
            </div>
            <div class="modal-body"><br>
              <ul class="timeline timeline-inverse" id="answer_c">
                <!-- timeline time label -->
                <li id="costsShow">
                  <span class="bg-navy">
                    cost->date
                  </span>
                  <div class="timeline-item">
                    <span class="time"><span class="badge bg-blue">costTimes->price zł</span></span>
                    <h3 class="timeline-header"><b>costTimes->cost_type</b></h3>
                    <div class="timeline-body">
                      <strong><i class="fa fa-map-marker margin-r-5"></i>Miejsce</strong><br>
                      costTimes->place<br><br>
                      <strong><i class="fa fa-pencil-square-o margin-r-5"></i>Opis</strong><br>
                      costTimes->description
                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">OK</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

  <!--Window modal with costs in cost type-->
    <div class="modal fade" id="modal-costs-type">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="h_type_cost">Koszty</h4>
            </div>
            <div class="modal-body"><br>
              <ul class="timeline timeline-inverse" id="answer_ct">
                <!-- timeline time label -->
                <li id="costsShowType">
                  <span class="bg-navy">
                    cost->date
                  </span>
                  <div class="timeline-item">
                    <span class="time"><span class="badge bg-blue">costTimes->price zł</span></span>
                    <h3 class="timeline-header"><b>costTimes->cost_type</b></h3>
                    <div class="timeline-body">
                      <strong><i class="fa fa-map-marker margin-r-5"></i>Miejsce</strong><br>
                      costTimes->place<br><br>
                      <strong><i class="fa fa-pencil-square-o margin-r-5"></i>Opis</strong><br>
                      costTimes->description
                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">OK</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
<!--FORMS-->
@include('costs.forms.addform')
@isset($cost_last)
@include('costs.forms.editformlast')
@include('costs.forms.removelast')
@endisset
@include('costs.forms.editform')
@include('costs.forms.remove')

<!-- jQuery 3 -->
<script src="{{ asset("adminlte/jquery/dist/jquery.min.js") }}"></script>
<script src="{{ asset("js/ajaxCost/update.js") }}"></script>
<script src="{{ asset("js/ajaxCost/calculateInTimeout.js") }}"></script>
<script src="{{ asset("js/ajaxCost/remove.js") }}"></script>
<script src="{{ asset("js/ajaxCost/showInTimeout.js") }}"></script>
<script src="{{ asset("js/ajaxCost/showInType.js") }}"></script>
<script src="{{ asset("js/ajaxCost/pagination.js") }}"></script>
<script>

var tt = @json($freshCt);
var date1 = @json($tab1);
var ctx = document.getElementById("pieChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: tt,
        datasets: [{
            label: '# of Votes',
            data: date1,
            backgroundColor: [
                'rgba(255, 0, 0, 0.6)',
                'rgba(0, 0, 128, 0.7)',
                'rgba(0, 128, 0, 0.6)',
                'rgba(0, 255, 255, 0.6)',
                'rgba(128, 0, 128, 0.7)',
                'rgba(128, 128, 0, 0.6)',
                'rgba(255, 255, 0, 0.8)',
                'rgba(128, 128, 128, 0.5)'
            ]
        }]
    }
});

</script>

@endsection
