<!--Formularz wyboru sezonu w oknie modalnym-->
<div class="modal fade" id="modal-default-view-s">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Wybierz sezon</h4>
      </div>
      <form method="get" action="{{ route('showSeasonTires', [$tire_one->id, $car_one->id]) }}" enctype="multipart/form-data">
      @csrf
      <div class="modal-body">
  <!--Pole - Sezon-->
          <div class="row">
            <div class="col-xs-8">
              <select class="form-control" id="seasonSelect" name="seasonSelect" >
                @foreach ($seasons as $season)
                    <option value="{{$season}}">{{$season}}</option>
                @endforeach
              </select>
              @if ($errors->has('seasonSelect'))
                  <span class="help-block">
                      <strong>{{ $errors->first('seasonSelect') }}</strong>
                  </span>
              @endif
            </div>
          </div>
          <br>
        </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
              <button type="submit" class="btn btn-primary">Pokaż</button>
            </div>
          </form>
        </div>
          <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
