<!--Formularz usuwania kompletu opon w oknie modalnym-->
    <div class="modal modal-danger fade" id="modal-danger-remove-t">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title center">Usuwanie kompletu opon</h4>
                  </div>
                  <form method="post" action="{{ route('removeTire') }}">
                  {{ method_field('delete') }}
                  {{ csrf_field() }}
                  <div class="modal-body">
                    <p class="center">Czy na pewno chcesz usunąć ten komplet opon ?</p>
                      <input type="hidden" name="tire_id" id="tire_id" value="">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Nie</button>
                    <button class="btn btn-outline">Tak</button>
                  </div>
                </form>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
