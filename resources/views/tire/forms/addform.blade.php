<!--Formularz dodawania stanu bieżnika w oknie modalnym-->
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Dodawanie Stanu Bieżnika</h4>
      </div>
      <form method="post" action="{{ route('addTreadCondition', $tire_one->id) }}" enctype="multipart/form-data">
      @csrf
      <div class="modal-body">
        <div class="row">
<!--Pole - Nazwa opony (unikalna)-->
            <div class="col-xs-6">
                <input type="text" id="nameAdd" class="form-control" name="name" placeholder = "Nazwa opony">
              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>
<!--Pole - Numer produkcji opony-->
          <div class="col-xs-5">
              <input type="text" id="dotAdd" class="form-control" name="dot" placeholder = "DOT">
            @if ($errors->has('dot'))
                <span class="help-block">
                    <strong>{{ $errors->first('dot') }}</strong>
                </span>
            @endif
          </div><b>*</b>
          </div>
          <br>
        <div class="row">
<!--Pole - Głebokość bieżnika opony przed założeniem-->
            <div class="col-xs-5">
              <div class="input-group">
                <input type="text" id="start_treadAdd" class="form-control" name="start_tread" placeholder = "Początkowa głebokość">
                <span class="input-group-addon">mm</span>
              </div>
              @if ($errors->has('start_tread'))
                  <span class="help-block">
                      <strong>{{ $errors->first('start_tread') }}</strong>
                  </span>
              @endif
            </div><div class="col-xs-1"><b>*</b></div>
<!--Pole - Głebokość bieżnika opony po ściągnięciu-->
          <div class="col-xs-5">
            <div class="input-group">
              <input type="text" id="end_treadAdd" class="form-control" name="end_tread" placeholder = "Końcowa głębokość">
              <span class="input-group-addon">mm</span>
            </div>
            @if ($errors->has('end_tread'))
                <span class="help-block">
                    <strong>{{ $errors->first('end_tread') }}</strong>
                </span>
            @endif
          </div><div class="col-xs-1"><b>*</b></div>
        </div>
          <hr>
<!--Pole - Data załozenia opony-->
    <div class="row">
      <div class="col-xs-6">
        <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Data założenia">
        <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        <input type="text" id="datepicker3" class="form-control pull-right" name="start_date"  data-date-format = "yyyy-mm-dd" placeholder = "____-__-__" required>
      </div>
          @if ($errors->has('start_date'))
              <span class="help-block">
                  <strong>{{ $errors->first('start_date') }}</strong>
              </span>
          @endif
        </div>
<!--Pole - Data ściągnięcia opony-->
          <div class="col-xs-5">
            <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Data ściągnięcia">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" id="datepicker4" class="form-control pull-right" name="end_date"  data-date-format = "yyyy-mm-dd" placeholder = "____-__-__">
            </div>
            @if ($errors->has('end_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('end_date') }}</strong>
                </span>
            @endif
            </div><div class="col-xs-1"><b>*</b></div>
          </div>
          <br>
<!--Pole - Przebieg przed założeniem opony-->
          <div class="row">
            <div class="col-xs-6" data-toggle="tooltip" data-placement="top" title="Przebieg przed założeniem">
              <div class="input-group">
                <input type="text" id="start_mileageAdd" class="form-control" name="start_mileage" placeholder = "Początkowy przebieg">
                <span class="input-group-addon">Km</span>
              </div>
              @if ($errors->has('start_mileage'))
                  <span class="help-block">
                      <strong>{{ $errors->first('start_mileage') }}</strong>
                  </span>
              @endif
          </div>
<!--Pole - Przebieg po ściągnięciu opony-->
          <div class="col-xs-5" data-toggle="tooltip" data-placement="top" title="Przebieg po ściągnięciu">
            <div class="input-group">
              <input type="text" id="end_mileageAdd" class="form-control" name="end_mileage" placeholder = "Końcowy przebieg">
              <span class="input-group-addon">Km</span>
            </div>
            @if ($errors->has('end_mileage'))
                <span class="help-block">
                    <strong>{{ $errors->first('end_mileage') }}</strong>
                </span>
            @endif
          </div><div class="col-xs-1"><b>*</b></div>
        </div>
        <hr>
<!--Pole - Pozycja opony-->
        <div class="row">
          <div class="col-xs-4" data-toggle="tooltip" data-placement="bottom" title="Pozycja opony">
            <select class="form-control" id="tire_positionAdd" name="tire_position"  required>
                           <option label="Wybierz..."></option>
                           <option>LP</option>
                           <option>LT</option>
                           <option>PP</option>
                           <option>PT</option>
                         </select>
            @if ($errors->has('tire_position'))
                <span class="help-block">
                    <strong>{{ $errors->first('tire_position') }}</strong>
                </span>
            @endif
          </div>
<!--Pole - Sezon-->
        <div class="col-xs-4" data-toggle="tooltip" data-placement="bottom" title="Sezon">
          <input class="form-control" id="seasonAdd" name="season" placeholder="2018 lub 2018/2019" required>
            @if ($errors->has('season'))
                <span class="help-block">
                    <strong>{{ $errors->first('season') }}</strong>
                </span>
            @endif
        </div>
      </div><br>
      <p class="help-block"><b>*</b>&nbsp; Pole nieobowiązkowe.</p>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Dodaj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
