<!--Formularz edycji ogumienia w oknie modalnym-->
<div class="modal fade" id="modal-default-edit-t">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edycja Ogumienia</h4>
      </div>
      <form method="post" action="{{ route('updateTire', [$tire_one->id, $car_one->id]) }}" >
      {{ csrf_field() }}
      <div class="modal-body">
        <div class="row">
  <!--Pole - Nazwa producenta ogumienia-->
              <div class="col-xs-6">
                <input type="text" id="manufacturer" class="form-control" name="manufacturer" required value="{{ old('manufacturer') ?: $tire_one->manufacturer }}">
                @if ($errors->has('manufacturer'))
                    <span class="help-block">
                        <strong>{{ $errors->first('manufacturer') }}</strong>
                    </span>
                @endif
              </div>
  <!--Pole - Nazwa modelu-->
            <div class="col-xs-6">
              <input type="text" id="model_t" class="form-control" name="model_t" required value="{{ old('model_t') ?: $tire_one->model }}">
              @if ($errors->has('model_t'))
                  <span class="help-block">
                      <strong>{{ $errors->first('model_t') }}</strong>
                  </span>
              @endif
              </div>
            </div>
            <br>
  <!--Pole - Rodzaj ogumienia-->
      <div class="row">
        <div class="col-xs-6">
              <select class="form-control" id="type_id" name="type_id" required>
                <option label="Wybierz..."></option>
                @foreach ($types as $type)
                  @if ($type->id == $tire_one->type_id)
                    <option value="{{$type->id}}" selected="selected">{{$type->name}}</option>
                  @else
                    <option value="{{$type->id}}">{{$type->name}}</option>
                  @endif
                @endforeach
            </select>
              @if ($errors->has('type_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('type_id') }}</strong>
                  </span>
              @endif
          </div>
  <!--Pole - DOT (numer produkcji kompletu opon)-->
            <div class="col-xs-6">
              <input type="text" id="dot_id" class="form-control" name="dot" required value="{{ old('dot') ?: $tire_one->dot }}">
                @if ($errors->has('dot'))
                    <span class="help-block">
                        <strong>{{ $errors->first('dot') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <br>
  <!--Pole - Szerokość opon-->
            <div class="row">
              <div class="col-xs-4" data-toggle="tooltip" data-placement="top" title="Szerokość">
                <select class="form-control" id="width_id" name="width_id" required>
                  <option label="Wybierz..."></option>
                  @foreach ($widths as $width)
                    @if ($width->id == $tire_one->width_id)
                      <option value="{{$width->id}}" selected="selected">{{$width->value}}</option>
                    @else
                      <option value="{{$width->id}}">{{$width->value}}</option>
                    @endif
                  @endforeach
                  </select>
                @if ($errors->has('width_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('width_id') }}</strong>
                    </span>
                @endif
            </div>
  <!--Pole - Wysokość opon-->
            <div class="col-xs-4" data-toggle="tooltip" data-placement="top" title="Profil">
              <select class="form-control" id="height_id" name="height_id" required>
                <option label="Wybierz..."></option>
                @foreach ($heights as $height)
                  @if ($height->id == $tire_one->height_id)
                    <option value="{{$height->id}}" selected="selected">{{$height->value}}</option>
                  @else
                    <option value="{{$height->id}}">{{$height->value}}</option>
                  @endif
                @endforeach
                </select>
              @if ($errors->has('height_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('height_id') }}</strong>
                  </span>
              @endif
            </div>
  <!--Pole - Średnica opon-->
            <div class="col-xs-4" data-toggle="tooltip" data-placement="top" title="Średnica">
              <select class="form-control" id="diameter_id" name="diameter_id" required>
                <option label="Wybierz..."></option>
                @foreach ($diameters as $diameter)
                  @if ($diameter->id == $tire_one->diameter_id)
                    <option value="{{$diameter->id}}" selected="selected">{{$diameter->value}}</option>
                  @else
                    <option value="{{$diameter->id}}">{{$diameter->value}}</option>
                  @endif
                @endforeach
                </select>
              @if ($errors->has('diameter_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('diameter_id') }}</strong>
                  </span>
              @endif
            </div>
          </div>
          <br>
  <!--Pole - Data założenia-->
        <div class="row">
          <div class="col-xs-6">
            <div class="input-group date" data-toggle="tooltip" data-placement="right" title="Data założenia">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" id="datepicker1" class="form-control pull-right" name="date_put"  data-date-format = "yyyy-mm-dd" required value="{{ old('date_put') ?: $tire_one->date_put }}">
          </div>
            @if ($errors->has('date_put'))
                <span class="help-block">
                    <strong>{{ $errors->first('date_put') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <br>
        <p class="help-block">DOT - numer produkcji opon.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Aktualizuj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
