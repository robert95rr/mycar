<!--Formularz edycji stanu bieżnikka w oknie modalnym-->
<div class="modal fade" id="modal-default-edit-tc">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edycja Stanu Bieżnika</h4>
      </div>
      <form method="post" action="{{ route('updateTreadCondition', $tire_one->id) }}" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}
      <div class="modal-body">
        <input type="hidden" name="tread_condition_id" id="tread_condition_idEdit" value="">
        <div class="row">
<!--Pole - Nazwa opony (unikalna)-->
            <div class="col-xs-6">
                <input type="text" id="name" class="form-control" name="name" >
              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>
<!--Pole - Numer produkcji opony-->
          <div class="col-xs-6" data-toggle="tooltip" data-placement="top" title="DOT">
              <input type="text" id="dot" class="form-control" name="dot" >
            @if ($errors->has('dot'))
                <span class="help-block">
                    <strong>{{ $errors->first('dot') }}</strong>
                </span>
            @endif
            </div>
          </div>
          <br>
        <div class="row">
<!--Pole - Głebokość bieżnika opony przed założeniem-->
            <div class="col-xs-6" data-toggle="tooltip" data-placement="top" title="Początkowa głębokość">
              <div class="input-group">
                <input type="text" id="start_tread" class="form-control" name="start_tread">
                <span class="input-group-addon">mm</span>
              </div>
              @if ($errors->has('start_tread'))
                  <span class="help-block">
                      <strong>{{ $errors->first('start_tread') }}</strong>
                  </span>
              @endif
            </div>
<!--Pole - Głebokość bieżnika opony po ściągnięciu-->
          <div class="col-xs-6" data-toggle="tooltip" data-placement="top" title="Końcowa głębokość">
            <div class="input-group">
              <input type="text" id="end_tread" class="form-control" name="end_tread">
              <span class="input-group-addon">mm</span>
            </div>
            @if ($errors->has('end_tread'))
                <span class="help-block">
                    <strong>{{ $errors->first('end_tread') }}</strong>
                </span>
            @endif
            </div>
          </div>
          <hr>
<!--Pole - Data załozenia opony-->
    <div class="row">
      <div class="col-xs-6">
        <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Data założenia">
        <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        <input type="text" id="datepicker7" class="form-control pull-right" name="start_date"  data-date-format = "yyyy-mm-dd">
      </div>
          @if ($errors->has('start_date'))
              <span class="help-block">
                  <strong>{{ $errors->first('start_date') }}</strong>
              </span>
          @endif
        </div>
<!--Pole - Data ściągnięcia opony-->
          <div class="col-xs-6">
            <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Data ściągnięcia">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" id="datepicker8" class="form-control pull-right" name="end_date"  data-date-format = "yyyy-mm-dd">
            </div>
            @if ($errors->has('end_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('end_date') }}</strong>
                </span>
            @endif
            </div>
          </div>
          <br>
<!--Pole - Przebieg przed założeniem opony-->
          <div class="row">
            <div class="col-xs-6" data-toggle="tooltip" data-placement="top" title="Przebieg przed założeniem">
              <div class="input-group">
                <input type="text" id="start_mileage" class="form-control" name="start_mileage">
                <span class="input-group-addon">Km</span>
              </div>
              @if ($errors->has('start_mileage'))
                  <span class="help-block">
                      <strong>{{ $errors->first('start_mileage') }}</strong>
                  </span>
              @endif
          </div>
<!--Pole - Przebieg po ściągnięciu opony-->
          <div class="col-xs-6" data-toggle="tooltip" data-placement="top" title="Przebieg po ściągnięciu">
            <div class="input-group">
              <input type="text" id="end_mileage" class="form-control" name="end_mileage">
              <span class="input-group-addon">Km</span>
            </div>
            @if ($errors->has('end_mileage'))
                <span class="help-block">
                    <strong>{{ $errors->first('end_mileage') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <hr>
<!--Pole - Pozycja opony-->
        <div class="row">
          <div class="col-xs-4" data-toggle="tooltip" data-placement="top" title="Pozycja opony">
            <select class="form-control" id="tire_position" name="tire_position">
                           <option>LP</option>
                           <option>LT</option>
                           <option>PP</option>
                           <option>PT</option>
                         </select>
            @if ($errors->has('tire_position'))
                <span class="help-block">
                    <strong>{{ $errors->first('tire_position') }}</strong>
                </span>
            @endif
          </div>
<!--Pole - Sezon-->
        <div class="col-xs-4" data-toggle="tooltip" data-placement="top" title="Sezon">
          <input class="form-control" id="season" name="season" >
            @if ($errors->has('season'))
                <span class="help-block">
                    <strong>{{ $errors->first('season') }}</strong>
                </span>
            @endif
        </div>
      </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Aktualizuj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
