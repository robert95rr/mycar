<!--Formularz wyboru nazwy opony w oknie modalnym-->
<div class="modal fade" id="modal-default-view-n">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Wybierz nazwę</h4>
      </div>
      <form method="get" action="{{ route('showNameTires', [$tire_one->id, $car_one->id]) }}" enctype="multipart/form-data">
      @csrf
      <div class="modal-body">
  <!--Pole - Nazwa -->
          <div class="row">
            <div class="col-xs-8">
              <select class="form-control" id="nameSelect" name="nameSelect">
                  @foreach ($names as $name)
                      <option value="{{$name}}">{{$name}}</option>
                  @endforeach
              </select>
              @if ($errors->has('nameSelect'))
                  <span class="help-block">
                      <strong>{{ $errors->first('nameSelect') }}</strong>
                  </span>
              @endif
            </div>
          </div>
          <br>
        </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
              <button type="submit" class="btn btn-primary">Pokaż</button>
            </div>
          </form>
        </div>
          <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
