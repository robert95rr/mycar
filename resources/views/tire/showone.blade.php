@extends('layouts.app')

@section('sidebar')

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form>
    <!-- /.search form -->
    @guest
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">HEADER</li>
      <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
          <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#">Link in level 2</a></li>
              <li><a href="#">Link in level 2</a></li>
            </ul>
          </li>
        </ul>
        @else
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">HEADER</li>
              <li class="active treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Auta</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li class="treeview">
                    <a href="#"> Moje Auta
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                      <ul class="treeview-menu">
                        @foreach ($car as $cars)
                        <li><a href="{{ route('showCar', $cars->id) }}"><i class="fa fa-dot-circle-o"></i>{{$cars->make}}&nbsp;{{$cars->model}}</a></li>
                        @endforeach
                      </ul>
                  </li>
                    <li><a href="{{ route('addFormCar') }}">Dodaj</a></li>
                </ul>
              </li>
              <li class="header"></li>
              <li>
                <a href="{{ route('showMileage', $car_one->id) }}">
                    <i class="fa fa-road"></i> <span>Przebieg</span><!--Przebieg auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showFuel', $car_one->id) }}">
                  <i class="fa fa-tint"></i> <span>Paliwo</span><!--Paliwo auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showInsurance', $car_one->id) }}">
                  <i class="fa fa-shield"></i> <span>Ubezpieczenie</span><!--Ubezpieczenie auta-->
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-dot-circle-o"></i>
                  <span>Ogumienie</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @foreach ($tire as $tires)
                  <li><a href="{{ route('showTire', [$tires->id, $car_one->id]) }}"><i class="fa fa-circle-o"></i>{{$tires->manufacturer}}&nbsp;{{$tires->model}}
                    @if($tires->type_id == 1)
                    <small class="label pull-right bg-yellow">Letnie</small>
                    @elseif($tires->type_id == 2)
                    <small class="label pull-right bg-blue">Zimowe</small>
                    @else
                    <small class="label pull-right bg-green">Całoroczne</small>
                    @endif
                    </a></li>
                  @endforeach
                  <li><a href="{{ route('addFormTire', $car_one->id) }}">Dodaj</a></li>
                </ul>
              </li><!--Ogumienie auta-->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-wrench"></i>
                  <span>Serwis</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{ route('showService',  [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Serwis Auta</a></li>
                  <li><a href="{{ route('showServicePlan',  [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Plan Serwisowy</a></li>
                </ul>
              </li><!--Serwis auta-->
              <li>
                <a href="{{ route('showCosts', $car_one->id) }}">
                  <i class="fa fa-usd"></i><span>Koszty</span><!--Koszty auta-->
                </a>
              </li>
              <li class="header">POWIADOMIENIA</li>
              <li>
                <a href="{{ route('showTechnicalInspection', $car_one->id) }}">
                  <i class="fa fa-stethoscope"></i><span class="label bg-olive"> Przegląd techniczny </span>
                </a>
              </li>
        </ul>
          @endguest

@endsection

@section('content_header')
      <h1>
        {{$car_one->make}}&nbsp;{{$car_one->model}} <small>| Ogumienie</small></h1><!--Nazwa auta (selected)-->
     <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('showCar', $car_one->id) }}">{{$car_one->make}}&nbsp;{{$car_one->model}}</a></li>
        <li class="active">Ogumienie</li>
      </ol><br/>
@endsection

@section('content')

<div class="row">
  <div class="col-md-5">
<!--Pierwszy box-->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Komplet Opon</h3>
      </div>
      <div class="box-body box-profile">

        <h3 class="profile-username text-center">{{$type_one->name}}</h3>

        <p class="text-muted text-center">Typ</p><br>
        <div class="col-md-10 col-md-offset-1">
          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Producent</b> <a class="pull-right">{{ $tire_one->manufacturer }}</a>
            </li>
            <li class="list-group-item">
              <b>Model</b> <a class="pull-right">{{ $tire_one->model }}</a>
            </li>
            <li class="list-group-item">
              <b>DOT</b> <a class="pull-right">{{ $tire_one->dot }}</a>
            </li>
            <li class="list-group-item">
              <b>Data założenia</b> <a class="pull-right">{{ $tire_one->date_put }}</a>
            </li>
            <li class="list-group-item">
              <b>Rozmiar</b> <a class="pull-right">{{$width_one->value}}/{{$height_one->value}} R{{$diameter_one->value}}</a>
            </li>
          </ul>
        </div>
          <button type="button" class="btn btn-warning margin btn-sm pull-right" data-toggle="modal" data-target="#modal-default-edit-t"><i class="fa fa-edit"></i> Edytuj</button>
          <button type="button" class="btn btn-danger margin btn-sm pull-right" data-tireid="{{$tire_one->id}}" data-toggle="modal" data-target="#modal-danger-remove-t"><i class="fa fa-trash"></i> Usuń</button>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->

<!--Drugi box-->
<div class="col-md-7">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Opony</h3>
      <div class="btn-group pull-right">
        <button type="button" class="btn btn-primary btn-sm">Pokaż stan bieżnika wg</button>
        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#" data-toggle="modal" data-target="#modal-default-view-s">Sezon</a></li>
          <li><a href="#" data-toggle="modal" data-target="#modal-default-view-n">Nazwa</a></li>
        </ul>
      </div>
    </div>
      <div class="box-body">

        <h4 class="text-center">Rozmieszczenie opon</h4>
        <div class="col-md-10 col-md-offset-1"><hr></div>
        <div class="col-md-4">
          <a href="{{ route('showTreadConditionLP', [$tire_one->id, $car_one->id]) }}" class="btn bg-olive margin btn-sm pull-right">Lewy Przód</a><br><br><br><br><br><br><br><br><br>
          <a href="{{ route('showTreadConditionLT', [$tire_one->id, $car_one->id]) }}" class="btn bg-purple margin btn-sm pull-right">Lewy Tył</a>
        </div>
        <div class="col-md-4">
          <img class="img-responsive center" src="/photos/chassisCar1.png" alt="Car chassis">
        </div>
        <div class="col-md-4">
          <a href="{{ route('showTreadConditionPP', [$tire_one->id, $car_one->id]) }}" class="btn bg-orange margin btn-sm pull-left">Prawy Przód</a><br><br><br><br><br><br><br><br><br>
          <a href="{{ route('showTreadConditionPT', [$tire_one->id, $car_one->id]) }}" class="btn bg-maroon margin btn-sm pull-left">Prawy Tył</a>
        </div>
        <div class="col-md-8 col-md-offset-2"><hr></div>

      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!--/col-->


</div><br>

@isset($tire_pos_lp)
    <div class="row">
            <div class="center-block col-xs-11" style="float: none;">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Pozycja - <span class="badge bg-olive">LP</span></h3>
                    <button type="button" class="btn bg-olive margin btn-sm pull-right" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i>
                      Dodaj
                    </button>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Nazwa</th>
                      <th>DOT</th>
                      <th>Początkowa głebokość</th>
                      <th>Data założenia</th>
                      <th>Początkowy przebieg</th>
                      <th>Końcowa głębokość</th>
                      <th>Data ściągnięcia</th>
                      <th>Końcowy przebieg</th>
                      <th>Przejechane</th>
                      <th>Sezon</th>
                      <th>#Akcja</th>
                    </tr>
                @foreach ($tread_condition as $tread_conditions)
                    <tr>
                      <td>{{ $tread_conditions->name }}</td>
                      <td>{{ $tread_conditions->dot }}</td>
                      <td>{{ $tread_conditions->start_tread }} mm</td>
                      <td>{{ $tread_conditions->start_date }}</td>
                      <td>{{ $tread_conditions->start_mileage }} km</td>
                      <td>{{ $tread_conditions->end_tread }} mm</td>
                      <td>@if($tread_conditions->end_date === NULL)
                            <font color="red">Brak</font>
                          @else
                            {{ $tread_conditions->end_date }}
                          @endif</td>
                      <td>@if($tread_conditions->end_mileage == '0')
                        <font color="red">{{ $tread_conditions->end_mileage }} km</font>
                          @else
                            {{ $tread_conditions->end_mileage }} km
                          @endif
                      </td>
                      <td>{{ $tread_conditions->diff_mileage }} km</td>
                      <td><span class="label label-primary">{{ $tread_conditions->season }}</span></td>
                      <td>
                        <button class="btn btn-warning btn-xs" data-toggle="modal" data-name="{{$tread_conditions->name}}" data-dot="{{$tread_conditions->dot}}" data-starttread="{{$tread_conditions->start_tread}}" data-endtread="{{$tread_conditions->end_tread}}" data-startdate="{{$tread_conditions->start_date}}" data-enddate="{{$tread_conditions->end_date}}" data-startmileage="{{$tread_conditions->start_mileage}}" data-endmileage="{{$tread_conditions->end_mileage}}"  data-tireposition="{{$tread_conditions->tire_position}}" data-season="{{$tread_conditions->season}}" data-treadconditionid="{{$tread_conditions->id}}" data-target="#modal-default-edit-tc">Edytuj</button>
                        <button class="btn btn-danger btn-xs"  data-toggle="modal" data-treadconditionid="{{$tread_conditions->id}}" data-target="#modal-danger-remove-tc">Usuń</button>
                      </td>
                    </tr>
                @endforeach
                  </table>
                </div>
                <div class="box box-footer">
                  Tabela - Stan bieżnika
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
    </div>
@endisset

@isset($tire_pos_lt)
    <div class="row">
            <div class="center-block col-xs-11" style="float: none;">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Pozycja - <span class="badge bg-purple">LT</span></h3>
                    <button type="button" class="btn bg-olive margin btn-sm pull-right" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i>
                      Dodaj
                    </button>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Nazwa</th>
                      <th>DOT</th>
                      <th>Początkowa głebokość</th>
                      <th>Data założenia</th>
                      <th>Początkowy przebieg</th>
                      <th>Końcowa głębokość</th>
                      <th>Data ściągnięcia</th>
                      <th>Końcowy przebieg</th>
                      <th>Przejechane</th>
                      <th>Sezon</th>
                      <th>#Akcja</th>
                    </tr>
                @foreach ($tread_condition as $tread_conditions)
                    <tr>
                      <td>{{ $tread_conditions->name }}</td>
                      <td>{{ $tread_conditions->dot }}</td>
                      <td>{{ $tread_conditions->start_tread }} mm</td>
                      <td>{{ $tread_conditions->start_date }}</td>
                      <td>{{ $tread_conditions->start_mileage }} km</td>
                      <td>{{ $tread_conditions->end_tread }} mm</td>
                      <td>@if($tread_conditions->end_date === NULL)
                            <font color="red">Brak</font>
                          @else
                            {{ $tread_conditions->end_date }}
                          @endif</td>
                      <td>@if($tread_conditions->end_mileage == '0')
                        <font color="red">{{ $tread_conditions->end_mileage }} km</font>
                          @else
                            {{ $tread_conditions->end_mileage }} km
                          @endif</td>
                      <td>{{ $tread_conditions->diff_mileage }} km</td>
                      <td><span class="label label-primary">{{ $tread_conditions->season }}</span></td>
                      <td>
                        <button class="btn btn-warning btn-xs" data-toggle="modal" data-name="{{$tread_conditions->name}}" data-dot="{{$tread_conditions->dot}}" data-starttread="{{$tread_conditions->start_tread}}" data-endtread="{{$tread_conditions->end_tread}}" data-startdate="{{$tread_conditions->start_date}}" data-enddate="{{$tread_conditions->end_date}}" data-startmileage="{{$tread_conditions->start_mileage}}" data-endmileage="{{$tread_conditions->end_mileage}}"  data-tireposition="{{$tread_conditions->tire_position}}" data-season="{{$tread_conditions->season}}" data-treadconditionid="{{$tread_conditions->id}}" data-target="#modal-default-edit-tc">Edytuj</button>
                        <button class="btn btn-danger btn-xs"  data-toggle="modal" data-treadconditionid="{{$tread_conditions->id}}" data-target="#modal-danger-remove-tc">Usuń</button>
                      </td>
                    </tr>
                @endforeach
                  </table>
                </div>
                <div class="box box-footer">
                  Tabela - Stan bieżnika
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
    </div>
@endisset

@isset($tire_pos_pp)
    <div class="row">
            <div class="center-block col-xs-11" style="float: none;">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Pozycja - <span class="badge bg-orange">PP</span></h3>
                    <button type="button" class="btn bg-olive margin btn-sm pull-right" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i>
                      Dodaj
                    </button>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Nazwa</th>
                      <th>DOT</th>
                      <th>Początkowa głebokość</th>
                      <th>Data założenia</th>
                      <th>Początkowy przebieg</th>
                      <th>Końcowa głębokość</th>
                      <th>Data ściągnięcia</th>
                      <th>Końcowy przebieg</th>
                      <th>Przejechane</th>
                      <th>Sezon</th>
                      <th>#Akcja</th>
                    </tr>
                @foreach ($tread_condition as $tread_conditions)
                    <tr>
                      <td>{{ $tread_conditions->name }}</td>
                      <td>{{ $tread_conditions->dot }}</td>
                      <td>{{ $tread_conditions->start_tread }} mm</td>
                      <td>{{ $tread_conditions->start_date }}</td>
                      <td>{{ $tread_conditions->start_mileage }} km</td>
                      <td>{{ $tread_conditions->end_tread }} mm</td>
                      <td>@if($tread_conditions->end_date === NULL)
                            <font color="red">Brak</font>
                          @else
                            {{ $tread_conditions->end_date }}
                          @endif</td>
                      <td>@if($tread_conditions->end_mileage == '0')
                        <font color="red">{{ $tread_conditions->end_mileage }} km</font>
                          @else
                            {{ $tread_conditions->end_mileage }} km
                          @endif</td>
                      <td>{{ $tread_conditions->diff_mileage }} km</td>
                      <td><span class="label label-primary">{{ $tread_conditions->season }}</span></td>
                      <td>
                        <button class="btn btn-warning btn-xs" data-toggle="modal" data-name="{{$tread_conditions->name}}" data-dot="{{$tread_conditions->dot}}" data-starttread="{{$tread_conditions->start_tread}}" data-endtread="{{$tread_conditions->end_tread}}" data-startdate="{{$tread_conditions->start_date}}" data-enddate="{{$tread_conditions->end_date}}" data-startmileage="{{$tread_conditions->start_mileage}}" data-endmileage="{{$tread_conditions->end_mileage}}"  data-tireposition="{{$tread_conditions->tire_position}}" data-season="{{$tread_conditions->season}}" data-treadconditionid="{{$tread_conditions->id}}" data-target="#modal-default-edit-tc">Edytuj</button>
                        <button class="btn btn-danger btn-xs"  data-toggle="modal" data-treadconditionid="{{$tread_conditions->id}}" data-target="#modal-danger-remove-tc">Usuń</button>
                      </td>
                    </tr>
                @endforeach
                  </table>
                </div>
                <div class="box box-footer">
                  Tabela - Stan bieżnika
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
    </div>
@endisset

@isset($tire_pos_pt)
    <div class="row">
            <div class="center-block col-xs-11" style="float: none;">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Pozycja - <span class="badge bg-maroon">PT</span></h3>
                    <button type="button" class="btn bg-olive margin btn-sm pull-right" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i>
                      Dodaj
                    </button>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Nazwa</th>
                      <th>DOT</th>
                      <th>Początkowa głebokość</th>
                      <th>Data założenia</th>
                      <th>Początkowy przebieg</th>
                      <th>Końcowa głębokość</th>
                      <th>Data ściągnięcia</th>
                      <th>Końcowy przebieg</th>
                      <th>Przejechane</th>
                      <th>Sezon</th>
                      <th>#Akcja</th>
                    </tr>
                @foreach ($tread_condition as $tread_conditions)
                    <tr>
                      <td>{{ $tread_conditions->name }}</td>
                      <td>{{ $tread_conditions->dot }}</td>
                      <td>{{ $tread_conditions->start_tread }} mm</td>
                      <td>{{ $tread_conditions->start_date }}</td>
                      <td>{{ $tread_conditions->start_mileage }} km</td>
                      <td>{{ $tread_conditions->end_tread }} mm</td>
                      <td>@if($tread_conditions->end_date === NULL)
                            <font color="red">Brak<font>
                          @else
                            {{ $tread_conditions->end_date }}
                          @endif</td>
                      <td>@if($tread_conditions->end_mileage == '0')
                        <font color="red">{{ $tread_conditions->end_mileage }} km</font>
                          @else
                            {{ $tread_conditions->end_mileage }} km
                          @endif</td>
                      <td>{{ $tread_conditions->diff_mileage }} km</td>
                      <td><span class="label label-primary">{{ $tread_conditions->season }}</span></td>
                      <td>
                        <button class="btn btn-warning btn-xs" data-toggle="modal" data-name="{{$tread_conditions->name}}" data-dot="{{$tread_conditions->dot}}" data-starttread="{{$tread_conditions->start_tread}}" data-endtread="{{$tread_conditions->end_tread}}" data-startdate="{{$tread_conditions->start_date}}" data-enddate="{{$tread_conditions->end_date}}" data-startmileage="{{$tread_conditions->start_mileage}}" data-endmileage="{{$tread_conditions->end_mileage}}"  data-tireposition="{{$tread_conditions->tire_position}}" data-season="{{$tread_conditions->season}}" data-treadconditionid="{{$tread_conditions->id}}" data-target="#modal-default-edit-tc">Edytuj</button>
                        <button class="btn btn-danger btn-xs"  data-toggle="modal" data-treadconditionid="{{$tread_conditions->id}}" data-target="#modal-danger-remove-tc">Usuń</button>
                      </td>
                    </tr>
                @endforeach
                  </table>
                </div>
                <div class="box box-footer">
                  Tabela - Stan bieżnika
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
    </div>
@endisset

@isset($seasonSelected)
<div class="row">
  <div class="center-block col-xs-10" style="float: none;">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Sezon <span class="label label-danger">{{ $seasonCheck }}</span></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-condensed">
            <tr>
              <th>Nazwa</th>
              <th>DOT</th>
              <th>Początkowa głębokość</th>
              <th>Data założenia</th>
              <th>Początkowy przebieg</th>
              <th>Końcowa głębokość</th>
              <th>Data ściągnięcia</th>
              <th>Końcowy przebieg</th>
              <th>Przejechane</th>
              <th>Pozycja</th>
            </tr>
        @foreach ($seasonSelected as $seasonSelecteds)
            <tr>
              <td>{{ $seasonSelecteds->name }}</td>
              <td>{{ $seasonSelecteds->dot }}</td>
              <td>{{ $seasonSelecteds->start_tread }} mm</td>
              <td>{{ $seasonSelecteds->start_date }}</td>
              <td>{{ $seasonSelecteds->start_mileage }} km</td>
              <td>{{ $seasonSelecteds->end_tread }} mm</td>
              <td>@if($seasonSelecteds->end_date === NULL)
                    <font color="red">Brak</font>
                  @else
                    {{ $seasonSelecteds->end_date }}
                  @endif</td>
              <td>{{ $seasonSelecteds->end_mileage }} km</td>
              <td>{{ $seasonSelecteds->diff_mileage }} km</td>
              @if($seasonSelecteds->tire_position == 'LP')
              <td><span class="badge bg-olive">{{ $seasonSelecteds->tire_position }}</span></td>
              @elseif($seasonSelecteds->tire_position == 'LT')
              <td><span class="badge bg-purple">{{ $seasonSelecteds->tire_position }}</span></td>
              @elseif($seasonSelecteds->tire_position == 'PP')
              <td><span class="badge bg-orange">{{ $seasonSelecteds->tire_position }}</span></td>
              @else
              <td><span class="badge bg-maroon">{{ $seasonSelecteds->tire_position }}</span></td>
              @endif
            </tr>
        @endforeach
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box box-footer">
          Stan bieżnika
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>

@endisset


@isset($nameSelected)
<div class="row">
  <div class="center-block col-xs-10" style="float: none;">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Nazwa <span class="label label-danger">{{ $nameCheck }}</span></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-condensed">
            <tr>
              <th>DOT</th>
              <th>Początkowa głębokość</th>
              <th>Data założenia</th>
              <th>Początkowy przebieg</th>
              <th>Końcowa głębokość</th>
              <th>Data ściągnięcia</th>
              <th>Końcowy przebieg</th>
              <th>Przejechane</th>
              <th>Pozycja</th>
              <th>Sezon</th>
            </tr>
        @foreach ($nameSelected as $nameSelecteds)
            <tr>
              <td>{{ $nameSelecteds->dot }}</td>
              <td>{{ $nameSelecteds->start_tread }} mm</td>
              <td>{{ $nameSelecteds->start_date}}</td>
              <td>{{ $nameSelecteds->start_mileage }} km</td>
              <td>{{ $nameSelecteds->end_tread }} mm</td>
              <td>@if($nameSelecteds->end_date === NULL)
                    <font color="red">Brak</font>
                  @else
                    {{ $nameSelecteds->end_date }}
                  @endif</td>
              <td>{{ $nameSelecteds->end_mileage }} km</td>
              <td>{{ $nameSelecteds->diff_mileage }} km</td>
              @if($nameSelecteds->tire_position == 'LP')
              <td><span class="badge bg-olive">{{ $nameSelecteds->tire_position }}</span></td>
              @elseif($nameSelecteds->tire_position == 'LT')
              <td><span class="badge bg-purple">{{ $nameSelecteds->tire_position }}</span></td>
              @elseif($nameSelecteds->tire_position == 'PP')
              <td><span class="badge bg-orange">{{ $nameSelecteds->tire_position }}</span></td>
              @else
              <td><span class="badge bg-maroon">{{ $nameSelecteds->tire_position }}</span></td>
              @endif
              <td><span class="label label-primary">{{ $nameSelecteds->season }}</span></td>
            </tr>
        @endforeach
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box box-footer">
          Stan bieżnika
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>

@endisset
<!--FORMS-->
@include('tire.forms.selectseasonform')
@include('tire.forms.selectnameform')
@include('tire.forms.addform')
@include('tire.forms.editform')
@include('tire.forms.editform_treadcondition')
@include('tire.forms.remove')
@include('tire.forms.removetreadcondition')

@endsection
