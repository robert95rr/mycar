@extends('layouts.app')

@section('sidebar')

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form>
    <!-- /.search form -->
    @guest
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">HEADER</li>
      <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
          <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#">Link in level 2</a></li>
              <li><a href="#">Link in level 2</a></li>
            </ul>
          </li>
        </ul>
        @else
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">HEADER</li>
              <li class="active treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Auta</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li class="treeview">
                    <a href="#"> Moje Auta
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                      <ul class="treeview-menu">
                        @foreach ($car as $cars)
                        <li><a href="{{ route('showCar', $cars->id) }}"><i class="fa fa-dot-circle-o"></i>{{$cars->make}}&nbsp;{{$cars->model}}</a></li>
                        @endforeach
                      </ul>
                  </li>
                    <li><a href="{{ route('addFormCar') }}">Dodaj</a></li>
                </ul>
              </li>
              <li class="header"></li>
              <li>
                <a href="{{ route('showMileage', $car_one->id) }}">
                    <i class="fa fa-road"></i> <span>Przebieg</span><!--Przebieg auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showFuel', $car_one->id) }}">
                  <i class="fa fa-tint"></i> <span>Paliwo</span><!--Paliwo auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showInsurance', $car_one->id) }}">
                  <i class="fa fa-shield"></i> <span>Ubezpieczenie</span><!--Ubezpieczenie auta-->
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-dot-circle-o"></i>
                  <span>Ogumienie</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @foreach ($tire as $tires)
                  <li ><a href="{{ route('showTire', [$tires->id, $car_one->id]) }}"><i class="fa fa-circle-o"></i>{{$tires->manufacturer}}&nbsp;{{$tires->model}}
                    @if($tires->type_id == 1)
                    <small class="label pull-right bg-yellow">Letnie</small>
                    @elseif($tires->type_id == 2)
                    <small class="label pull-right bg-blue">Zimowe</small>
                    @else
                    <small class="label pull-right bg-green">Całoroczne</small>
                    @endif
                    </a></li>
                  @endforeach
                  <li><a href="{{ route('addFormTire', $car_one->id) }}">Dodaj</a></li><!--Ogumienie auta-->
                </ul>
              </li><!--Ogumienie auta-->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-wrench"></i>
                  <span>Serwis</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{ route('showService', [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Serwis Auta</a></li>
                  <li><a href="{{ route('showServicePlan', [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Plan Serwisowy</a></li>
                </ul>
              </li><!--Serwis auta-->
              <li>
                <a href="{{ route('showCosts', $car_one->id) }}">
                  <i class="fa fa-usd"></i> <span>Koszty</span><!--Koszty auta-->
                </a>
              </li>
        </ul>
    @endguest

@endsection

@section('content_header')
      <h1>
        Ogumienie - Formularz
      </h1>
     <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('showCar', $car_one->id) }}">{{$car_one->make}}&nbsp;{{$car_one->model}}</a></li>
        <li>Ogumienie</li>
        <li class="active">Dodaj</li>
      </ol><br/>
@endsection

@section('content')


        <button type="button" class="btn bg-olive btn-sm"  data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i>
          Dodaj
        </button>


      <!--Modal Form-->
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Dodawanie Ogumienia</h4>
            </div>
            <form method="post" action="{{ route('addTire', $car_one->id) }}" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
              <div class="row">
      <!--Pole - Nazwa producenta ogumienia-->
                  <div class="col-xs-6">
                    <input type="text" id="manufacturer" class="form-control" name="manufacturer" placeholder = "Producent">
                    @if ($errors->has('manufacturer'))
                        <span class="help-block">
                            <strong>{{ $errors->first('manufacturer') }}</strong>
                        </span>
                    @endif
                  </div>
      <!--Pole - Nazwa modelu-->
                <div class="col-xs-6">
                  <input type="text" id="model_t" class="form-control" name="model_t" placeholder = "Model">
                  @if ($errors->has('model_t'))
                      <span class="help-block">
                          <strong>{{ $errors->first('model_t') }}</strong>
                      </span>
                  @endif
                  </div>
                </div>
                <br>
      <!--Pole - Rodzaj ogumienia-->
          <div class="row">
            <div class="col-xs-6">
                  <select class="form-control" id="type_id" name="type_id" required>
                    <option label="Wybierz..."></option>
                    @foreach ($types as $type)
                        <option value="{{$type->id}}">{{$type->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('type_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('type_id') }}</strong>
                    </span>
                @endif
              </div>
      <!--Pole - DOT (numer produkcji kompletu opon)-->
                <div class="col-xs-5">
                  <input type="text" id="dot" class="form-control" name="dot" placeholder="DOT">
                  @if ($errors->has('dot'))
                      <span class="help-block">
                          <strong>{{ $errors->first('dot') }}</strong>
                      </span>
                  @endif
                </div><b>*</b>
                </div>
                <br>
      <!--Pole - Szerokość opon-->
                <div class="row">
                  <div class="col-xs-4" data-toggle="tooltip" data-placement="top" title="Szerokość">
                    <select class="form-control" id="width_id" name="width_id" required>
                      <option label="Wybierz..."></option>
                      @foreach ($widths as $width)
                          <option value="{{$width->id}}">{{$width->value}}</option>
                      @endforeach
                      </select>
                    @if ($errors->has('width_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('width_id') }}</strong>
                        </span>
                    @endif
                </div>
      <!--Pole - Wysokość opon-->
                <div class="col-xs-4" data-toggle="tooltip" data-placement="top" title="Profil">
                  <select class="form-control" id="height_id" name="height_id" required>
                    <option label="Wybierz..."></option>
                    @foreach ($heights as $height)
                        <option value="{{$height->id}}">{{$height->value}}</option>
                    @endforeach
                    </select>
                  @if ($errors->has('height_id'))
                      <span class="help-block">
                          <strong>{{ $errors->first('height_id') }}</strong>
                      </span>
                  @endif
                </div>
      <!--Pole - Średnica opon-->
                <div class="col-xs-4" data-toggle="tooltip" data-placement="top" title="Średnica">
                  <select class="form-control" id="diameter_id" name="diameter_id" required>
                    <option label="Wybierz..."></option>
                    @foreach ($diameters as $diameter)
                        <option value="{{$diameter->id}}">{{$diameter->value}}</option>
                    @endforeach
                    </select>
                  @if ($errors->has('diameter_id'))
                      <span class="help-block">
                          <strong>{{ $errors->first('diameter_id') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <br>
      <!--Pole - Data założenia-->
            <div class="row">
              <div class="col-xs-6">
                <div class="input-group date" data-toggle="tooltip" data-placement="right" title="Data założenia">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" id="datepicker8" class="form-control pull-right" name="date_put"  data-date-format = "yyyy-mm-dd" placeholder = "____-__-__" required>
              </div>
                @if ($errors->has('date_put'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date_put') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <br>
            <p class="help-block">DOT - numer produkcji opon.</p>
            <p class="help-block"><b>*</b>&nbsp; Pole nieobowiązkowe.</p>
          </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
              <button type="submit" class="btn btn-primary">Dodaj</button>
            </div>
          </form>
        </div>
          <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@endsection
