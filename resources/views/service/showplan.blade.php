@extends('layouts.app')

@section('sidebar')

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form>
    <!-- /.search form -->
    @guest
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">HEADER</li>
      <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
          <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#">Link in level 2</a></li>
              <li><a href="#">Link in level 2</a></li>
            </ul>
          </li>
        </ul>
        @else
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">HEADER</li>
              <li class="active treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Auta</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li class="treeview">
                    <a href="#"> Moje Auta
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                      <ul class="treeview-menu">
                        @foreach ($car as $cars)
                        <li><a href="{{ route('showCar', $cars->id) }}"><i class="fa fa-dot-circle-o"></i>{{$cars->make}}&nbsp;{{$cars->model}}</a></li>
                        @endforeach
                      </ul>
                  </li>
                    <li><a href="{{ route('addFormCar') }}">Dodaj</a></li>
                </ul>
              </li>
              <li class="header"></li>
              <li>
                <a href="{{ route('showMileage', $car_one->id) }}">
                    <i class="fa fa-road"></i> <span>Przebieg</span><!--Przebieg auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showFuel', $car_one->id) }}">
                  <i class="fa fa-tint"></i> <span>Paliwo</span><!--Paliwo auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showInsurance', $car_one->id) }}">
                  <i class="fa fa-shield"></i> <span>Ubezpieczenie</span><!--Ubezpieczenie auta-->
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-dot-circle-o"></i>
                  <span>Ogumienie</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @foreach ($tire as $tires)
                  <li><a href="{{ route('showTire', [$tires->id, $car_one->id]) }}"><i class="fa fa-circle-o"></i>{{$tires->manufacturer}}&nbsp;{{$tires->model}}
                    @if($tires->type_id == 1)
                    <small class="label pull-right bg-yellow">Letnie</small>
                    @elseif($tires->type_id == 2)
                    <small class="label pull-right bg-blue">Zimowe</small>
                    @else
                    <small class="label pull-right bg-green">Całoroczne</small>
                    @endif
                    </a></li>
                  @endforeach
                  <li><a href="{{ route('addFormTire', $car_one->id) }}">Dodaj</a></li>
                </ul>
              </li><!--Ogumienie auta-->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-wrench"></i>
                  <span>Serwis</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{ route('showService',  [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Serwis Auta</a></li>
                  <li><a href="{{ route('showServicePlan', [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Plan Serwisowy</a></li>
                </ul>
              </li><!--Serwis auta-->
              <li>
                <a href="{{ route('showCosts', $car_one->id) }}">
                  <i class="fa fa-usd"></i><span>Koszty</span><!--Koszty auta-->
                </a>
              </li>
              <li class="header">POWIADOMIENIA</li>
              <li>
                <a href="{{ route('showTechnicalInspection', $car_one->id) }}">
                  <i class="fa fa-stethoscope"></i><span class="label bg-olive"> Przegląd techniczny </span>
                </a>
              </li>
        </ul>
          @endguest

@endsection

@section('content_header')
      <h1>
        {{$car_one->make}}&nbsp;{{$car_one->model}} <small>| Plan Serwisowy</small></h1><!--Nazwa auta (selected)-->
     <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('showCar', $car_one->id) }}">{{$car_one->make}}&nbsp;{{$car_one->model}}</a></li>
        <li class="active">Plan Serwisowy</li>
      </ol><br/>
@endsection

@section('content')

  <div class="row">
    <div class="center-block col-md-7" style="float: none;">
      <div class="box box-primary">
        <div class="box-header with-border"><i class="fa fa-wrench"></i><h2 class="box-title">Plan Serwisowy</h2>
          <button type="button" class="btn bg-olive btn-sm pull-right" data-toggle="modal" data-target="#modal-default-add-sp"><i class="fa fa-plus"></i> Dodaj</button>
        </div>
        <div class="box-body">
          <br>
            <div class="col-md-10 col-md-offset-1">
                <div class="box-group" id="accordion">
                  <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                @foreach ($servicePlan as $servicePlans)
                  <div class="panel box box-primary">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a class="serviceplanCol" data-toggle="collapse" data-parent="#accordion" href="#{{ $servicePlans->id }}">
                          {{ $servicePlans->service_task }}
                        </a>
                      </h4>
                    </div>
                    <div id="{{ $servicePlans->id }}" class="panel-collapse collapse">
                      <div class="box-body">
                        <b>Interwał serwisowy:</b>
                        <p><span class="label label-success">{{ $servicePlans->interval }} km</span></p>
                        <hr>
                        <b>Opis:</b>
                        <p>{{ $servicePlans->description }}</p>
                        <hr>
                        <div class="pull-right">
                          <button type="button" class="btn btn-warning btn-xs" data-carmodel="{{$servicePlans->car_model}}" data-servicetask="{{$servicePlans->service_task}}" data-interval="{{$servicePlans->interval}}" data-description="{{$servicePlans->description}}" data-serviceplanid={{$servicePlans->id}} data-toggle="modal" data-target="#modal-default-edit-sp">Edytuj</button>
                          <button type="button" class="btn btn-danger btn-xs" data-serviceplanid="{{$servicePlans->id}}" data-toggle="modal" data-target="#modal-danger-remove-sp">Usuń</button>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div><br>
              @if($servicePlan != '[]')
              <div class="btn-group pull-right">
                <input type="hidden" id="car_id" name="car_id" value="{{$car_one->id}}">
                <button type="button" class="btn btn-primary btn-sm open_modalAdd"><i class="fa fa-bullhorn"></i> Powiadomienia</button>
                <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li class="dropdown-header"><b>Aplikacja</b></li>
                  <li><a href="#" data-toggle="modal" data-target="#modal-default-addNotification">Dodaj</a></li>
                  <li><a href="showN" id="showNS">Pokaż</a></li>
                  <li class="dropdown-header"><b>Email</b></li>
                  <li><a href="#" data-toggle="modal" data-target="#modal-default-sendNotification">Wyślij</a></li>
                </ul>
              </div><br><hr>
              <div class="box-footer" id="block_notificationsSer">
              </div>
              @endif
            </div>
        </div>
      </div>
    </div><!-- /.col-md-6 -->

  </div>
<!--FORMS-->
@include('service.forms.serviceplan.addform')
@include('service.forms.serviceplan.editform')
@include('service.forms.serviceplan.remove')
@include('notifications.service.forms.addform')
@include('notifications.service.forms.editform')
@include('notifications.remove')
@include('notifications.service.forms.sendform')
@include('notifications.noDataModal')
<script src="{{ asset("adminlte/jquery/dist/jquery.min.js") }}"></script>
@if(session('status'))
@include('notifications.successModal')
<script type="text/javascript">
$(document).ready(function(){
  setTimeout(function(){
    $('#modal-positiveFeedback').modal('show');
  }, 1000);
  setTimeout(function(){
    $('#modal-positiveFeedback').modal('hide');
  }, 3000);
});
</script>
@endif
<script src="{{ asset("js/ajaxService/notifications/showNotifications.js")}}"></script>
<script src="{{ asset("js/ajaxService/notifications/update.js")}}"></script>
<script src="{{ asset("js/ajaxNotification/remove.js")}}"></script>
@endsection
