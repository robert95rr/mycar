@extends('layouts.app')

@section('sidebar')

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form>
    <!-- /.search form -->
    @guest
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">HEADER</li>
      <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
          <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#">Link in level 2</a></li>
              <li><a href="#">Link in level 2</a></li>
            </ul>
          </li>
        </ul>
        @else
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">HEADER</li>
              <li class="active treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Auta</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li class="treeview">
                    <a href="#"> Moje Auta
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                      <ul class="treeview-menu">
                        @foreach ($car as $cars)
                        <li><a href="{{ route('showCar', $cars->id) }}"><i class="fa fa-dot-circle-o"></i>{{$cars->make}}&nbsp;{{$cars->model}}</a></li>
                        @endforeach
                      </ul>
                  </li>
                    <li><a href="{{ route('addFormCar') }}">Dodaj</a></li>
                </ul>
              </li>
              <li class="header"></li>
              <li>
                <a href="{{ route('showMileage', $car_one->id) }}">
                    <i class="fa fa-road"></i> <span>Przebieg</span><!--Przebieg auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showFuel', $car_one->id) }}">
                  <i class="fa fa-tint"></i> <span>Paliwo</span><!--Paliwo auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showInsurance', $car_one->id) }}">
                  <i class="fa fa-shield"></i> <span>Ubezpieczenie</span><!--Ubezpieczenie auta-->
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-dot-circle-o"></i>
                  <span>Ogumienie</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @foreach ($tire as $tires)
                  <li><a href="{{ route('showTire', [$tires->id, $car_one->id]) }}"><i class="fa fa-circle-o"></i>{{$tires->manufacturer}}&nbsp;{{$tires->model}}
                    @if($tires->type_id == 1)
                    <small class="label pull-right bg-yellow">Letnie</small>
                    @elseif($tires->type_id == 2)
                    <small class="label pull-right bg-blue">Zimowe</small>
                    @else
                    <small class="label pull-right bg-green">Całoroczne</small>
                    @endif
                    </a></li>
                  @endforeach
                  <li><a href="{{ route('addFormTire', $car_one->id) }}">Dodaj</a></li>
                </ul>
              </li><!--Ogumienie auta-->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-wrench"></i>
                  <span>Serwis</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{ route('showService',  [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Serwis Auta</a></li>
                  <li><a href="{{ route('showServicePlan',  [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Plan Serwisowy</a></li>
                </ul>
              </li><!--Serwis auta-->
              <li>
                <a href="{{ route('showCosts', $car_one->id) }}">
                  <i class="fa fa-usd"></i><span>Koszty</span><!--Koszty auta-->
                </a>
              </li>
              <li class="header">POWIADOMIENIA</li>
              <li>
                <a href="{{ route('showTechnicalInspection', $car_one->id) }}">
                  <i class="fa fa-stethoscope"></i><span class="label bg-olive"> Przegląd techniczny </span>
                </a>
              </li>
        </ul>
      @endguest

@endsection

@section('content_header')
      <h1>
        {{$car_one->make}}&nbsp;{{$car_one->model}} <small>| Serwis</small></h1><!--Nazwa auta (selected)-->
     <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('showCar', $car_one->id) }}">{{$car_one->make}}&nbsp;{{$car_one->model}}</a></li>
        <li class="active">Serwis</li>
      </ol><br/>
@endsection

@section('content')

  <div class="row">
    <div class="center-block col-md-8" style="float: none;">
      <div class="box box-primary">
        <div class="box-header with-border"><i class="fa fa-wrench"></i><h2 class="box-title">Serwis</h2>
          <button class="btn bg-olive btn-sm pull-right" data-toggle="modal" data-target="#modal-default-add-s"><i class="fa fa-plus"></i> Dodaj</button>
        </div>
        <div class="box-body">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Ostatnia czynność serwisowa</a></li>
              <li><a href="#tab_2" data-toggle="tab">Historia serwisowa</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <!--1 Punkt-->
                @isset($service)
                <br>
                  <div class="box box-success box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">{{ $service_task }}</h3>
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                    </div>
                    <div class="box-body">
                      <!--Data serwisu-->
                      <strong><i class="fa fa-calendar margin-r-5"></i> Data serwisu</strong>
                      <p class="text-muted col">
                        {{ $service->date_service }}
                      </p>
                      <hr>
                      <strong><i class="fa fa-road margin-r-5"></i> Przebieg</strong>
                      <p class="text-muted col">
                        {{ $service->mileage }} km
                      </p>
                      <hr>
                      <strong><i class="fa fa-road margin-r-5"></i> Przebieg następnego serwisu</strong>
                      <p class="text-muted">
                        <span class="label label-primary">{{ $service->next_service_mileage }} km</span>
                      </p>
                      <hr>
                      <strong><i class="fa fa-pencil-square-o margin-r-5"></i> Opis</strong>
                      <p class="text-muted col">
                      {{ $service->description }}
                      </p>
                      <hr>
                      <div class="pull-right">
                        <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-default-edit-sl">Edytuj</button>
                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-danger-remove-sl">Usuń</button>
                    </div>
                    </div>
                  </div>
                  @endisset
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <!-- Czynność serwisowa -->
                <!-- The timeline --><br>
                @isset($serviceHistory)
                <ul class="timeline timeline-inverse">
                  @foreach ($serviceHistory as $serviceHistories)

                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li id="serviceHistories{{$serviceHistories->id}}">
                    <span class="bg-navy">
                      {{ $serviceHistories->date_service }}
                    </span>

                    <div class="timeline-item">
                      <span class="time colService"><i class="fa fa-road margin-r-5"></i> {{ $serviceHistories->mileage }} km <i class="fa fa-long-arrow-right"></i> {{ $serviceHistories->next_service_mileage }} km</span>
                      @if($serviceHistories->service_task === NULL)
                      <h3 class="timeline-header service"><b>Brak planu serwisowego dla tej czynności</b></h3>
                      @else
                      <h3 class="timeline-header service"><b>{{ $serviceHistories->service_task }}</b></h3>
                      @endif
                      <div class="timeline-body">
                        {{ $serviceHistories->description }}
                      </div>
                      <div class="timeline-footer">
                        <button class="btn btn-warning btn-xs open_modal" value="{{$serviceHistories->id}}">Edytuj</button>
                        <button class="btn btn-danger btn-xs open_modal_delete" value="{{$serviceHistories->id}}">Usuń</button>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  @endforeach
                </ul>
                @endisset

              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
      </div>
    </div>
  </div>
<!--FORMS-->
@include('service.forms.service.addform')

@isset($service)
@include('service.forms.service.editformlast')
@include('service.forms.service.removelast')
@endisset

@include('service.forms.service.editform')
@include('service.forms.service.remove')
@include('service.forms.service.removewarning')

<!-- jQuery 3 -->
<script src="{{ asset("adminlte/jquery/dist/jquery.min.js") }}"></script>
<script src="{{ asset("js/ajaxService/update.js") }}"></script>
<script src="{{ asset("js/ajaxService/remove.js") }}"></script>
@endsection
