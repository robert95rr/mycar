<!--Formularz usuwania czynności serwisowej w oknie modalnym-->
    <div class="modal modal-danger fade" id="modal-danger-remove-sp">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title center">Usuwanie Czynności Serwisowej</h4>
                  </div>
                  <form method="post" action="{{ route('removeServicePlan') }}" >
                  {{ method_field('delete') }}
                  {{ csrf_field() }}
                  <div class="modal-body">
                    <p class="center">Czy na pewno chcesz usunąć tą czynność serwisową ?</p>
                      <input type="hidden" name="service_plan_id" id="service_plan_id" value="">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Nie</button>
                    <button class="btn btn-outline">Tak</button>
                  </div>
                </form>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
