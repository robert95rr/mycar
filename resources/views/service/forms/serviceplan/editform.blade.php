<!--Formularz edycji czynności serwisowej w oknie modalnym-->
<div class="modal fade" id="modal-default-edit-sp">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edycja Czynności Serwisowej</h4>
      </div>
      <form method="post" action="{{ route('updateServicePlan') }}" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}
      <div class="modal-body">
        <input type="hidden" name="service_plan_id" id="service_plan_idEdit" value="">
        <div class="row">
<!--Pole - Model Auta-->
            <div class="col-xs-12">
                <select id="car_model" class="form-control" name="car_model">
                  @foreach ($car as $cars)
                    <option>{{$cars->make}}&nbsp;{{$cars->model}}</option>
                  @endforeach
                </select>
              @if ($errors->has('car_model'))
                  <span class="help-block">
                      <strong>{{ $errors->first('car_model') }}</strong>
                  </span>
              @endif
            </div>
          </div><br>
<!--Pole - Czynność serwisowa-->
        <div class="row">
          <div class="col-xs-6" data-toggle="tooltip" data-placement="top" title="Czynność serwisowa">
              <select id="service_task" class="form-control" name="service_task">
                      <option>Wymiana oleju i filtra</option>
                      <option>Wymiana rozrządu</option>
                      <option>Wymiana filtra powietrza</option>
                      <option>Wymiana płynu hamulcowego</option>
                      <option>Wymiana filtra paliwa</option>
                      <option>Wymiana sprzęgła</option>
                      <option>Serwis klimatyzacji</option>
                      <option>Wymiana oleju w skrzyni biegów</option>
                      <option>Wymiana świec żarowych</option>
                      <option>Wymiana płynu chłodzenia</option>
                      <option>Wymiana filtra kabinowego</option>
              </select>
            @if ($errors->has('service_task'))
                <span class="help-block">
                    <strong>{{ $errors->first('service_task') }}</strong>
                </span>
            @endif
          </div>
<!--Pole - Interwał-->
            <div class="col-xs-6">
              <div class="input-group">
                <input type="text" id="interval" class="form-control" name="interval">
                <span class="input-group-addon">Km</span>
              </div>
              @if ($errors->has('interval'))
                  <span class="help-block">
                      <strong>{{ $errors->first('interval') }}</strong>
                  </span>
              @endif
            </div>
          </div>
            <br>
<!--Pole - Opis-->
      <div class="row">
          <div class="col-xs-12">
            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Aktualizuj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
