<!--Formularz usuwania serwisu w oknie modalnym-->
    <div class="modal modal-danger fade" id="modal-danger-remove-s">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title center">Usuwanie serwisu</h4>
                  </div>
                  <form id="delete_service" name="delete_service">
                  {{ csrf_field() }}
                  <div class="modal-body">
                    <p class="center">Czy na pewno chcesz usunąć ten serwis ?</p>
                      <input type="hidden" name="service_delete_id" id="service_delete_id" value="">
                      <input type="hidden" name="service_car_id" id="service_car_id" value="{{$car_one->id}}">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Nie</button>
                    <button class="btn btn-outline" id="btn-delete" value="delete">Tak</button>
                  </div>
                </form>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
