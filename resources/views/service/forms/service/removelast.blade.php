<!--Formularz usuwania ostaniego serwisu w oknie modalnym-->
<div class="modal modal-danger fade" id="modal-danger-remove-sl">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title center">Usuwanie Serwisu</h4>
              </div>
              <div class="modal-body">
                <p class="center">Czy na pewno chcesz usunąć ten serwis ?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Nie</button>
                <a class="btn btn-outline" href="{{ route('removeServiceL', $service->id) }}">Tak</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
