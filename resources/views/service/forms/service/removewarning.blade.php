<!--Formularz z ostrzeżeniem w przypadku usuwania serwisu, który jest już zapisany jako koszt-->
    <div class="modal modal-warning fade" id="modal-warning-error">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title center">Ostrzeżenie</h4>
                  </div>
                  <div class="modal-body">
                    <p class="center">Jeśli chcesz usunąć ten serwis, to najpierw usuń koszt związany z tym serwisem.</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Zamknij</button>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
