<!--Formularz edycji serwisu w oknie modalym-->
<div class="modal fade" id="modal-default-editService">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edycja Serwisu</h4>
      </div>
      <form id="updateService" name="updateService" class="form-horizontal" novalidate="">
      {{ csrf_field() }}
      <div class="modal-body">
        <input type="hidden" id="serviceId" name="serviceId" value="">
        <input type="hidden" id="car_id" name="car_id" value="{{$car_one->id}}">
        <input type="hidden" id="service_task" name="service_task" value="">
        <input type="hidden" id="service_planId" name="service_planId" value="">
        <div class="row"><br>
<!--Pole - Czynność serwisowa (opcjonalnie *)-->
            <div class="col-xs-6" data-toggle="tooltip" data-placement="top" title="Czynność serwisowa">
              <select id="service_plan_id_ajax" class="form-control" name="service_plan_id" disabled>
                <option value="NULL" label="Brak"></option>
                    @foreach ($servicePlans as $servicePlan)
                        <option value="{{$servicePlan->id}}">{{$servicePlan->service_task}} - {{$servicePlan->interval}} km</option>
                    @endforeach
              </select>
              @if ($errors->has('service_plan_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('service_plan_id') }}</strong>
                  </span>
              @endif
            </div>
<!--Pole - Data serwisu-->
          <div class="col-xs-6">
            <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Data serwisu">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            <input type="text" id="datepicker6" class="form-control pull-right" name="date_service" data-date-format = "yyyy-mm-dd" value="">
              </div>
            @if ($errors->has('date_service'))
                <span class="help-block">
                    <strong>{{ $errors->first('date_service') }}</strong>
                </span>
            @endif
        </div>
      </div><br>
<!--Pole - Przebieg auta -->
      <div class="row">
            <div class="col-xs-6">
              <div class="input-group">
                <input type="text" id="mileage_ajax" class="form-control" name="mileage" value="">
                <span class="input-group-addon">Km</span>
              </div>
              @if ($errors->has('mileage'))
                  <span class="help-block">
                      <strong>{{ $errors->first('mileage') }}</strong>
                  </span>
              @endif
            </div>
<!--Pole - Przebieg następnego serwisu-->
          <div class="col-xs-5">
            <div class="input-group">
            <input type="text" id="next_service_mileage_ajax" class="form-control" name="next_service_mileage" value="">
            <span class="input-group-addon">Km</span>
          </div>
            @if ($errors->has('next_service_mileage'))
                <span class="help-block">
                    <strong>{{ $errors->first('next_service_mileage') }}</strong>
                </span>
            @endif
          </div><b>*</b>
        </div><br>
<!--Pole - Opis-->
      <div class="row">
        <div class="col-xs-12">
          <textarea class="form-control" id="description_ajax" name="description" rows="3"></textarea>
          @if ($errors->has('description'))
              <span class="help-block">
                  <strong>{{ $errors->first('description') }}</strong>
              </span>
          @endif
        </div>
      </div><br>
      <p class="help-block"><b>*</b>&nbsp; - pole nieobowiązkowe.</p>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="button" class=" btn btn-primary" id="btn-update" value="update">Aktualizuj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
