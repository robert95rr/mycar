@extends('layouts.app')

@section('sidebar')

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form>
    <!-- /.search form -->
    @guest
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">HEADER</li>
      <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
          <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#">Link in level 2</a></li>
              <li><a href="#">Link in level 2</a></li>
            </ul>
          </li>
        </ul>
        @else
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">HEADER</li>
              <li class="active treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Auta</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li class="treeview">
                    <a href="#"> Moje Auta
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                      <ul class="treeview-menu">
                        @foreach ($car as $cars)
                        <li><a href="{{ route('showCar', $cars->id) }}"><i class="fa fa-dot-circle-o"></i>{{$cars->make}}&nbsp;{{$cars->model}}</a></li>
                        @endforeach
                      </ul>
                  </li>
                  <li><a href="{{ route('addFormCar') }}">Dodaj</a></li>
                </ul>
              </li>
              <li class="header"></li>
              @isset($car_one)
              <li>
                <a href="{{ route('showMileage', $car_one->id) }}">
                    <i class="fa fa-road"></i> <span>Przebieg</span><!--Przebieg auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showFuel', $car_one->id) }}">
                  <i class="fa fa-tint"></i> <span>Paliwo</span><!--Paliwo auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showInsurance', $car_one->id) }}">
                  <i class="fa fa-shield"></i> <span>Ubezpieczenie</span><!--Ubezpieczenie auta-->
                </a>
              </li>
              <li class="treeview">
              <a href="#">
                <i class="fa fa-dot-circle-o"></i>
                <span>Ogumienie</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @foreach ($tire as $tires)
                <li ><a href="{{ route('showTire', [$tires->id, $car_one->id]) }}"><i class="fa fa-circle-o"></i>{{$tires->manufacturer}}&nbsp;{{$tires->model}}
                  @if($tires->type_id == 1)
                  <small class="label pull-right bg-yellow">Letnie</small>
                  @elseif($tires->type_id == 2)
                  <small class="label pull-right bg-blue">Zimowe</small>
                  @else
                  <small class="label pull-right bg-green">Całoroczne</small>
                  @endif
                  </a></li>
                @endforeach
                <li><a href="{{ route('addFormTire', $car_one->id) }}">Dodaj</a></li><!--Ogumienie auta-->
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-wrench"></i>
                <span>Serwis</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('showService', [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Serwis Auta</a></li>
                <li><a href="{{ route('showServicePlan', [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Plan Serwisowy</a></li>
              </ul>
            </li><!--Serwis auta-->
              <li>
                <a href="{{ route('showCosts', $car_one->id) }}">
                  <i class="fa fa-usd"></i> <span>Koszty</span><!--Koszty auta-->
                </a>
              </li>
              <li class="header">POWIADOMIENIA</li>
              <li>
                <a href="{{ route('showTechnicalInspection', $car_one->id) }}">
                  <i class="fa fa-stethoscope"></i><span class="label bg-olive"> Przegląd techniczny </span>
                </a>
              </li>
              @endisset
        </ul>
          @endguest

@endsection

@section('content_header')
      <h1>
       Użytkownik
       <small>| Profil</small>
     </h1>
     <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">{{ Auth::user()->first_name }}</a></li>
        <li class="active">Profil</li>
      </ol><br/>
@endsection


@section('content')

<div class="row">
  <div class="center-block col-md-5" style="float: none;">

    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile" id="user{{$user->id}}">
        <img class="profile-user-img img-responsive img-circle" src="/photos/Robert_Rezler_2019.png" alt="User profile picture">

        <h3 class="profile-username text-center">{{$user->first_name}} {{$user->last_name}}</h3>

        <p class="text-muted text-center">Użytkownik</p><br>

        <div class="col-md-10 col-md-offset-1">
          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Adres E-mail</b> <a class="pull-right">{{$user->email}}</a>
            </li>
            <li class="list-group-item">
              <b>Numer telefonu</b> <a class="pull-right">{{$user->phone_number}}</a>
            </li>
          </ul>
        </div>
        <div class="col-md-12"><br>
            <button type="button" class="btn btn-sm btn-warning pull-right open_modal" value="{{$user->id}}"><i class="fa fa-edit"></i> Edytuj</button>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
<!--FORMS-->
@include('user.forms.editform')

<!-- jQuery 3 -->
<script src="{{ asset("adminlte/jquery/dist/jquery.min.js") }}"></script>
<script src="{{ asset("js/ajaxUser/update.js") }}"></script>
@endsection
