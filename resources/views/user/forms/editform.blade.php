<!--Formularz edycji danych użytkownika w oknie modalnym-->
<div class="modal fade" id="modal-default-editUser">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edycja Użytkownika</h4>
      </div>
      <form id="updateUser" name="updateUser" class="form-horizontal" novalidate="">
      {{ csrf_field() }}
      <div class="modal-body">
        <input type="hidden" id="userId" name="userId" value="">
        <div class="row">
<!--Pole - Imię-->
          <div class="col-xs-6">
            <input type="text" id="first_name_ajax" class="form-control" name="first_name" value="">
            @if ($errors->has('first_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
            @endif
          </div>
<!--Pole - Nazwisko-->
          <div class="col-xs-6">
            <input type="text" id="last_name_ajax" class="form-control" name="last_name" value="">
            @if ($errors->has('last_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
            @endif
          </div>
        </div><br>
<!--Pole - Adres E-mail -->
    <div class="row">
      <div class="col-xs-7">
        <div class="input-group">
          <input type="text" id="email_ajax" class="form-control" name="email" value="">
          <div class="input-group-addon">
            <i class="fa fa-envelope"></i>
          </div>
        </div>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
<!--Pole - Numer telefonu-->
        <div class="col-xs-5">
          <div class="input-group">
            <input type="text" id="phone_number_ajax" class="form-control" name="phone_number" value="">
            <div class="input-group-addon">
              <i class="fa fa-phone"></i>
            </div>
          </div>
          @if ($errors->has('phone_number'))
              <span class="help-block">
                  <strong>{{ $errors->first('phone_number') }}</strong>
              </span>
          @endif
        </div>
      </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="button" class=" btn btn-primary" id="btn-update" value="update">Aktualizuj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
