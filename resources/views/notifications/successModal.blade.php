<!--Window modal with positive feedback for user-->
  <div class="modal modal-success fade" id="modal-positiveFeedback">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header" id="headerModalSuccess">
            <h4 class="modal-title" id="headerIconSuccess"><i class="fa fa-check-circle-o"></i></h4>
          </div>
          <div class="modal-body">
            <h4 id="successHeaderModal">{{ session('status') }}</h4>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
