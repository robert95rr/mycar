<!--Formularz wysłania powiadomienia (przegląd techniczny) na email(uzytkownika) w oknie modalnym-->
<div class="modal fade" id="modal-default-sendNotification">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Wysłanie Powiadomienia (Email)</h4>
      </div>
      <form method="post" action="{{ route('sendNotification', $car_one->id) }}" enctype="multipart/form-data">
      @csrf
      <div class="modal-body">
        <input type="hidden" id="category" name="category" value="przegląd techniczny">
        <div class="row">
<!--Pole - Opis wydarzenia-->
            <div class="col-xs-12">
              <textarea class="form-control" id="descriptionSend" name="descriptionSend" rows="3" placeholder="Treść wiadomości..." required></textarea>
              @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
              @endif
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Wyślij</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
