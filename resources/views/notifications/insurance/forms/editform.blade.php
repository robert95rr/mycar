<!--Formularz edycji powiadomienia (ubezpieczenia) w oknie modalnym-->
<div class="modal fade" id="modal-default-editNotificationInsurance">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edycja Powiadomienia (Ubezpieczenie)</h4>
      </div>
      <form id="updateNotificationsInsurance" name="updateNotificationsInsurance" class="form-horizontal" novalidate="">
      {{ csrf_field() }}
      <div class="modal-body">
        <input type="hidden" name="notificationInsuranceId" id="notificationInsuranceId" value="">
        <div class="row">
<!--Pole - Data wydarzenia-->
      <div class="col-xs-6">
        <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Data wydarzenia">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
        <input type="text" id="datepicker2" class="form-control pull-right" name="date_event" data-date-format = "yyyy-mm-dd" >
        </div>
        @if ($errors->has('date01'))
                  <span class="help-block">
                      <strong>{{ $errors->first('date01') }}</strong>
                  </span>
        @endif
      </div>
<!--Przypomnienie (ile dni wcześniej)-->
      <div class="col-xs-6" data-toggle="tooltip" data-placement="top" title="Przypomnienie wydarzenia">
        <select class="form-control" id="reminder" name="reminder" required>
          <option value="1">1 dzień wcześniej</option>
          <option value="2">2 dni wcześniej</option>
          <option value="3">3 dni wcześniej</option>
        </select>
        @if ($errors->has('reminder'))
                  <span class="help-block">
                      <strong>{{ $errors->first('reminder') }}</strong>
                  </span>
        @endif
        </div>
      </div>
      <br>
<!--Pole - Opis wydarzenia-->
          <div class="row">
            <div class="col-xs-12">
              <textarea class="form-control" id="descriptionEdit" name="description" rows="3" placeholder="Opis..." required></textarea>
              @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
              @endif
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="button" class="btn btn-primary" id="btn-updateNotification" value="update">Aktualizuj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
