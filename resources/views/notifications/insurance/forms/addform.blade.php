<!--Formularz dodawania powiadomienia (ubezpieczenie) w oknie modalnym-->
<div class="modal fade" id="modal-default-addNotification">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Dodawanie Powiadomienia</h4>
      </div>
      <form method="post" action="{{ route('addNotification', $car_one->id) }}" enctype="multipart/form-data">
      @csrf
      <div class="modal-body">
        <input type="hidden" id="category" name="category" value="Ubezpieczenie">
        <div class="row">
<!--Pole - Data wydarzenia-->
      <div class="col-xs-6">
          <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Data wydarzenia">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
          <input type="text" id="datepicker1" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date01" placeholder="____-__-__" required>
            @if ($errors->has('date01'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date01') }}</strong>
                    </span>
          @endif
        </div>
      </div>
<!--Pole - Przypomnienie (ile dni wcześniej)-->
          <div class="col-xs-6" data-toggle="tooltip" data-placement="top" title="Przypomnienie wydarzenia">
            <select class="form-control" id="reminder" name="reminder" required>
              <option label="Wybierz..."></option>
              <option>1 dzień wcześniej</option>
              <option>2 dni wcześniej</option>
              <option>3 dni wcześniej</option>
            </select>
            @if ($errors->has('reminder'))
                      <span class="help-block">
                          <strong>{{ $errors->first('reminder') }}</strong>
                      </span>
            @endif
            </div>
          </div>
          <br>
<!--Pole - Opis wydarzenia-->
          <div class="row">
            <div class="col-xs-12">
              <textarea class="form-control" id="descriptionAdd" name="description" rows="3" placeholder="Opis..." required></textarea>
              @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
              @endif
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Dodaj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
