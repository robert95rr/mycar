<!--Window modal with information no data (notifications) for user-->
  <div class="modal modal-info fade" id="modal-informationAboutNoData">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="headerIconInformation"><i class="fa fa-info-circle"></i></h4>
          </div>
          <div class="modal-body">
            <h4 id="informationHeaderModal">Brak zapisanych powiadomień</h4>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
