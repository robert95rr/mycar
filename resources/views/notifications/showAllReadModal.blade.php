<!--Window modal with read notifications for user-->
  <div class="modal modal-default fade" id="modal-default-showAllReadNotifications">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" id="headerNotifications">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><i class="fa fa-bell"></i> Przeczytane powiadomienia</h4>
          </div>
          <div class="modal-body" id="contentNotifications">
            <h4><i class="fa fa-calendar"></i>data</h4>
            <p>opis</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-sm pull-right" id="btn-deleteAllRead">Usuń wszystkie</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
