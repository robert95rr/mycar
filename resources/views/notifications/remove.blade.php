<!--Usuwanie powiadomienia w oknie modalnym-->
    <div class="modal modal-danger fade" id="modal-danger-remove-n">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title center">Usuwanie powiadomienia</h4>
                  </div>
                  <form id="delete_notification" name="delete_notification">
                  {{ csrf_field() }}
                  <div class="modal-body">
                    <p class="center">Czy na pewno chcesz usunąć to powiadomienie ?</p>
                      <input type="hidden" name="notification_delete_id" id="notification_delete_id" value="">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Nie</button>
                    <button class="btn btn-outline" id="btn-deleteN" value="deleteN">Tak</button>
                  </div>
                </form>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
