<!--Window modal with notification for user-->
  <div class="modal modal-warning fade" id="modal-default-showNotification">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" id="headerNotification">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">kategoria</h4>
          </div>
          <div class="modal-body" id="contentNotification">
            <h4><i class="fa fa-calendar"></i>data</h4>
            <p>opis</p>
          </div>
          <div class="modal-footer" id="footerNotification">
            <button type="button" class="btn btn-outline pull-right" id="markAsRead">Oznacz jako przeczytane</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
