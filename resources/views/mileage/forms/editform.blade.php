<!--Formularz edycji przebiegu w oknie modalnym-->
<div class="modal fade" id="modal-default-edit-m">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edycja Przebiegu</h4>
      </div>
      <form method="post" action="{{ route('updateMileage', $car_one->id) }}" >
      {{ method_field('PATCH') }}
      {{ csrf_field() }}
      <div class="modal-body">
        <input type="hidden" name="mileage_id" id="mileage_id" value="">
        <div class="row">
<!--Pole - Wartość(km)-->
          <div class="col-xs-6">
            <div class="input-group">
            <input type="text" id="value" class="form-control" name="value" >
              <span class="input-group-addon">Km</span>
              @if ($errors->has('value'))
                      <span class="help-block">
                          <strong>{{ $errors->first('value') }}</strong>
                      </span>
              @endif
          </div>
        </div>
<!--Pole - data-->
          <div class="col-xs-6">
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            <input type="text" id="datepicker" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date">
              @if ($errors->has('date'))
                      <span class="help-block">
                          <strong>{{ $errors->first('date') }}</strong>
                      </span>
            @endif
            </div>
          </div>

        </div>
        <br>

<!--Pole - opis-->
        <div class="row">
            <div class="col-xs-12">
              <textarea class="form-control" id="description" name="description" rows="3" ></textarea>

              @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
              @endif
            </div>
          </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Aktualizuj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
