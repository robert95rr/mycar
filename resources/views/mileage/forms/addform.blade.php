<!--Formularz dodawania przebiegu w oknie modalnym-->
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Dodawanie Przebiegu</h4>
      </div>
      <form method="post" action="{{ route('addMileage', $car_one->id) }}" enctype="multipart/form-data">
      @csrf
      <div class="modal-body">
        <div class="row">
<!--Pole - Wartość(km)-->
          <div class="col-xs-6">
            <div class="input-group">
            <input type="text" id="value_add" class="form-control" name="value" placeholder="Wartość" required>
              <span class="input-group-addon">Km</span>
          </div>
          @if ($errors->has('value'))
                  <span class="help-block">
                      <strong>{{ $errors->first('value') }}</strong>
                  </span>
          @endif
        </div>
<!--Pole - data-->
          <div class="col-xs-6">
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            <input type="text" id="datepicker1" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date" placeholder="____-__-__" required>
              @if ($errors->has('date'))
                      <span class="help-block">
                          <strong>{{ $errors->first('date') }}</strong>
                      </span>
            @endif
            </div>
          </div>

        </div>
        <br>

<!--Pole - opis-->
        <div class="row">
            <div class="col-xs-12">
              <textarea class="form-control" id="description_add" name="description" rows="3" placeholder="Opis..." required></textarea>
              @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
              @endif
            </div>
          </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Dodaj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
