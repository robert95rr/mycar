<!--Formularz usuwania przebiegu w oknie modalnym-->
    <div class="modal modal-danger fade" id="modal-danger-remove-m">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title delete">Usuwanie Przebiegu</h4>
                  </div>
                  <form method="post" action="{{ route('removeMileage') }}" >
                  {{ method_field('delete') }}
                  {{ csrf_field() }}
                  <div class="modal-body">
                    <p class="delete">Czy na pewno chcesz usunąć ten przebieg ?</p>
                      <input type="hidden" name="mileage_id" id="mileage_id_delete" value="">
                      <input type="hidden" name="car_id" id="car_id" value="{{$car_one->id}}">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Nie</button>
                    <button class="btn btn-outline">Tak</button>
                  </div>
                  </form>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
