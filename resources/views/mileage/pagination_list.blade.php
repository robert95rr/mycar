<!-- The time line -->
<ul class="timeline">
  @foreach ($mileage as $mileages)
  <!-- timeline time label -->
  <li class="time-label">
        <span class="bg-maroon">
          {{ $mileages->date }}
        </span>
  </li>
  <!-- /.timeline-label -->
  <!-- timeline item -->
  <li>

    <div class="timeline-item">

      <h3 class="timeline-header">Przebieg: <b>{{ $mileages->value }}</b> Km</h3>

      <div class="timeline-body">
        {{ $mileages->description }}
      </div>
      <div class="timeline-footer">
        <button class="btn btn-warning btn-xs"  data-myvalue = "{{ $mileages->value }}" data-mydate = "{{ $mileages->date }}" data-mydescription = "{{ $mileages->description }}"  data-mileageid ={{$mileages->id}} data-toggle="modal" data-target="#modal-default-edit-m">Edytuj</button>
        <button class="btn btn-danger btn-xs" data-mileageid ={{$mileages->id}} data-toggle="modal" data-target="#modal-danger-remove-m">Usuń</button>
      </div>
    </div>
  </li>
  <!-- END timeline item -->
  <!-- timeline item -->
  @endforeach
    @if(count($mileage) > 0)
      <li><i class="fa fa-road bg-blue"></i></li>
    @endif
</ul>{{$mileage->links()}}
