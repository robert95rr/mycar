@extends('layouts.app')

@section('sidebar')

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form>
    <!-- /.search form -->
    @guest
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">HEADER</li>
      <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
          <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#">Link in level 2</a></li>
              <li><a href="#">Link in level 2</a></li>
            </ul>
          </li>
        </ul>
        @else
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">HEADER</li>
              <li class="active treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Auta</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li class="treeview">
                    <a href="#"> Moje Auta
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                      <ul class="treeview-menu">
                        @foreach ($car as $cars)
                        <li><a href="{{ route('showCar', $cars->id) }}"><i class="fa fa-dot-circle-o"></i>{{$cars->make}}&nbsp;{{$cars->model}}</a></li>
                        @endforeach
                      </ul>
                  </li>
                    <li><a href="{{ route('addFormCar') }}">Dodaj</a></li>
                </ul>
              </li>
              <li class="header"></li>
              <li>
                <a href="{{ route('showMileage', $car_one->id) }}">
                    <i class="fa fa-road"></i> <span>Przebieg</span><!--Przebieg auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showFuel', $car_one->id) }}">
                  <i class="fa fa-tint"></i> <span>Paliwo</span><!--Paliwo auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showInsurance', $car_one->id) }}">
                  <i class="fa fa-shield"></i> <span>Ubezpieczenie</span><!--Ubezpieczenie auta-->
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-dot-circle-o"></i>
                  <span>Ogumienie</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @foreach ($tire as $tires)
                  <li><a href="{{ route('showTire', [$tires->id, $car_one->id]) }}"><i class="fa fa-circle-o"></i>{{$tires->manufacturer}}&nbsp;{{$tires->model}}
                    @if($tires->type_id == 1)
                    <small class="label pull-right bg-yellow">Letnie</small>
                    @elseif($tires->type_id == 2)
                    <small class="label pull-right bg-blue">Zimowe</small>
                    @else
                    <small class="label pull-right bg-green">Całoroczne</small>
                    @endif
                    </a></li>
                  @endforeach
                  <li><a href="{{ route('addFormTire', $car_one->id) }}">Dodaj</a></li>
                </ul>
              </li><!--Ogumienie auta-->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-wrench"></i>
                  <span>Serwis</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{ route('showService', [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Serwis Auta</a></li>
                  <li><a href="{{ route('showServicePlan', [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Plan Serwisowy</a></li>
                </ul>
              </li><!--Serwis auta-->
              <li>
                <a href="{{ route('showCosts', $car_one->id) }}">
                  <i class="fa fa-usd"></i> <span>Koszty</span><!--Koszty auta-->
                </a>
              </li>
              <li class="header">POWIADOMIENIA</li>
              <li>
                <a href="{{ route('showTechnicalInspection', $car_one->id) }}">
                  <i class="fa fa-stethoscope"></i><span class="label bg-olive"> Przegląd techniczny </span>
                </a>
              </li>
        </ul>
          @endguest

@endsection

@section('content_header')
      <h1>
        {{$car_one->make}}&nbsp;{{$car_one->model}} <small>| Ubezpieczenie</small></h1><!--Nazwa auta (selected)-->
     <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('showCar', $car_one->id) }}">{{$car_one->make}}&nbsp;{{$car_one->model}}</a></li>
        <li class="active">Ubezpieczenie</li>
      </ol><br/>
@endsection

@section('content')

<div class="row">
  <div class="center-block col-md-5" style="float: none;">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">Aktualne ubezpieczenie</a></li>
        <li><a href="#tab_2" data-toggle="tab">Historia</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div class="row">
            <div class="col-md-6 col-md-offset-6">
          <button type="button" class="btn bg-olive btn-sm pull-right"  data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i>
            Dodaj
          </button></div></div>
          @isset($insurance_current)
          <div id="insurance{{$insurance_current->id}}">
            <h3 class="text-center">{{ $insurance_current->company_name }}</h3>
            <p class="text-muted text-center">Firma ubezpieczeniowa</p>
            <hr>
            <strong><i class="fa fa-shield margin-r-5"></i>Nazwa ubezpieczenia</strong>
            <p class="text-muted">{{ $insurance_current->product_name }}</p>
            <br>
            <strong>Rodzaj ubezpieczenia</strong>
            <p class="text-muted">{{ $insurance_current->insurance_type }}</p>
            <br>
            <strong>Numer polisy</strong>
            <p class="text-muted">{{ $insurance_current->policy_number }}</p>
            <br>
            <strong><i class="fa fa-calendar margin-r-5"></i>Okres ubezpieczenia</strong>
            <p class="text-muted">{{ $insurance_current->period_from }} - {{ $insurance_current->period_to }}</p>
            <br>
            <strong>Cena</strong>
            <p class="text-muted"><span class="label label-danger">{{ $insurance_current->price }} zł</span></p>
            <br>
            <strong><i class="fa fa-road margin-r-5"></i>Przebieg</strong>
            <p class="text-muted">{{ $insurance_current->mileage }} km</p><hr>
            <div class="btn-group">
            <button class="btn btn-warning btn-sm open_modalEdit" value="{{$insurance_current->id}}"><i class="fa fa-edit"></i> Edytuj</button>
            </div>
            <div class="btn-group pull-right">
              <input type="hidden" id="car_id" name="car_id" value="{{$car_one->id}}">
              <button type="button" class="btn btn-primary btn-sm open_modalAdd"><i class="fa fa-bullhorn"></i> Powiadomienia</button>
              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li class="dropdown-header"><b>Aplikacja</b></li>
                <li><a href="#" data-toggle="modal" data-target="#modal-default-addNotification">Dodaj</a></li>
                <li><a href="showN" id="showNI">Pokaż</a></li>
                <li class="dropdown-header"><b>Email</b></li>
                <li><a href="#" data-toggle="modal" data-target="#modal-default-sendNotification">Wyślij</a></li>
              </ul>
            </div>
          </div>
          <br>
          @endisset
          <!--------------------------------------------->
          <!--BOX Notification-->
          <br>
          <div class="center-block col-md-12" style="float: none;" id="block_notificationsIns">
          </div>
          <!-- /.col -->
            <!------------------------------------------->
        </div>

        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
          <div class="row">
            <h4 class="text-center">Ubezpieczenia</h4>
          </div><hr>
          @if($insuranceHistory != '[]')
          @isset($insuranceHistory)
            <div class="box-footer box-comments">
          @foreach ($insuranceHistory as $insuranceHistories)

              <div class="box-comment">
                <div class="comment-text">
                      <span class="username">
                        {{ $insuranceHistories->company_name }}
                        <span class="text-muted pull-right">{{ $insuranceHistories->period_from }} - {{ $insuranceHistories->period_to }}</span>
                      </span><br><!-- /.username -->
                  <strong>Nazwa ubezpieczenia:</strong> {{ $insuranceHistories->product_name }}<br>
                  <strong>Rodzaj ubezpieczenia:</strong> {{ $insuranceHistories->insurance_type }}<br>
                  <strong>Numer polisy:</strong> {{ $insuranceHistories->policy_number }}
                  <p class="pull-right"><strong>Cena: </strong><span class="label label-danger"> {{ $insuranceHistories->price }} zł</span></p>
                </div>
                <!-- /.comment-text -->
              </div>
          @endforeach
        </div><hr>
        @endisset
        @endif
        <!-- /.box-footer -->
        </div>
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>

<br>
<!--FORMS-->
@include('insurance.forms.addform')
@include('insurance.forms.editform')
@include('notifications.insurance.forms.addform')
@include('notifications.insurance.forms.editform')
@include('notifications.remove')
@include('notifications.insurance.forms.sendform')

<script src="{{ asset("adminlte/jquery/dist/jquery.min.js") }}"></script>
@if(session('status'))
@include('notifications.successModal')
<script type="text/javascript">
$(document).ready(function(){
  setTimeout(function(){
    $('#modal-positiveFeedback').modal('show');
  }, 1000);
  setTimeout(function(){
    $('#modal-positiveFeedback').modal('hide');
  }, 3000);
});
</script>
@endif
<script src="{{ asset("js/ajaxInsurance/update.js") }}"></script>
<script src="{{ asset("js/ajaxInsurance/notifications/showNotifications.js")}}"></script>
<script src="{{ asset("js/ajaxInsurance/notifications/update.js")}}"></script>
<script src="{{ asset("js/ajaxNotification/remove.js")}}"></script>
@endsection
