<!--Formularz dodawania ubezpieczenia w oknie modalnym-->
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Dodawanie Ubezpieczenia</h4>
      </div>
      <form method="post" action="{{ route('addInsurance', $car_one->id) }}" enctype="multipart/form-data">
      @csrf
      <div class="modal-body">
        <div class="row">
<!--Pole - Nazwa firmy ubezpieczeniowej-->
      <div class="col-xs-6">
            <input type="text" id="company_nameAdd" class="form-control" name="company_name" placeholder="Nazwa firmy" required>
          @if ($errors->has('company_name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('company_name') }}</strong>
                  </span>
          @endif
        </div>
<!--Pole - Nazwa produktu(nazwa ubezpieczenia)-->
          <div class="col-xs-6">
            <input type="text" id="product_nameAdd" class="form-control" name="product_name" placeholder="Nazwa produktu" required>
            @if ($errors->has('product_name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('product_name') }}</strong>
                      </span>
            @endif
            </div>
          </div>
          <br>
<!--Pole - Radzaj ubezpieczenia-->
          <div class="row">
            <div class="col-xs-6">
              <select class="form-control select2" id="insurance_typeAdd" multiple="multiple" name="insurance_type[]" data-placeholder="Insurance type" style="width: 100%;" required>
                @foreach ($type_insurances as $type_insurance)
                    <option value="{{$type_insurance}}">{{$type_insurance}}</option>
                @endforeach
                </select>
              @if ($errors->has('insurance_type'))
                      <span class="help-block">
                          <strong>{{ $errors->first('insurance_type') }}</strong>
                      </span>
              @endif
          </div>
<!--Pole - Numer polisy ubezpieczeniowej-->
          <div class="col-xs-6">
            <input type="text" id="policy_numberAdd" class="form-control" name="policy_number" placeholder="Numer polisy" required>
            @if ($errors->has('policy_number'))
                      <span class="help-block">
                          <strong>{{ $errors->first('policy_number') }}</strong>
                      </span>
            @endif
          </div>
        </div>
        <hr>
<!--Pole - Okres ubezpieczenia od-->
          <div class="row">
            <div class="col-xs-6">
              <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Okres ubezpieczenia od">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
              <input type="text" id="datepicker4" class="form-control pull-right" name="period_from" data-date-format = "yyyy-mm-dd" placeholder = "____-__-__" >
            </div>
              @if ($errors->has('period_from'))
                        <span class="help-block">
                            <strong>{{ $errors->first('period_from') }}</strong>
                        </span>
              @endif
            </div>
<!--Pole - Okres ubezpieczenia do-->
          <div class="col-xs-6">
            <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Okres ubezpieczenia do">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            <input type="text" id="datepicker5" class="form-control pull-right" name="period_to" data-date-format = "yyyy-mm-dd" placeholder = "____-__-__" >
            </div>
            @if ($errors->has('period_to'))
                      <span class="help-block">
                          <strong>{{ $errors->first('period_to') }}</strong>
                      </span>
            @endif
            </div>
          </div>
          <br>
<!--Pole - Cena-->
      <div class="row">
        <div class="col-xs-6">
          <div class="input-group">
          <input type="text" id="priceAdd" class="form-control" name="price" placeholder="Cena" required>
          <span class="input-group-addon">zł</span>
        </div>
          @if ($errors->has('price'))
                    <span class="help-block">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
          @endif
        </div>
<!--Pole - Przebieg auta-->
          <div class="col-xs-6">
            <div class="input-group">
            <input type="text" id="mileageAdd" class="form-control" name="mileage" placeholder="Przebieg" required>
            <span class="input-group-addon">Km</span>
            </div>
            @if ($errors->has('mileage'))
                      <span class="help-block">
                          <strong>{{ $errors->first('mileage') }}</strong>
                      </span>
            @endif
            </div>
          </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Dodaj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
