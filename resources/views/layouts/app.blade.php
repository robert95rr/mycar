<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" style="height: auto; min-height: 100%;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'myCar_v2') }}</title>


    <link rel="stylesheet" href="{{ asset("adminlte/bootstrap/dist/css/bootstrap.min.css") }}" type="text/css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset("adminlte/font-awesome/css/font-awesome.min.css") }}" type="text/css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset("adminlte/Ionicons/css/ionicons.min.css") }}" type="text/css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("adminlte/dist/css/AdminLTE.min.css") }}" type="text/css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset("adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}" type="text/css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset("adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}" type="text/css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
          <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset("css/styleApp.css") }}" type="text/css">
    <link rel="stylesheet" href="{{ asset("adminlte/dist/css/skins/_all-skins.min.css") }}" type="text/css">
    <link rel="stylesheet" href="{{ asset("adminlte/dist/css/skins/skin-green.min.css") }}" type="text/css">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset("adminlte/select2/dist/css/select2.min.css") }}" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style>
</head>

<body class="hold-transition skin-green fixed sidebar-mini" style="height: auto; min-height: 100%;">
      <div class="wrapper" style="height: auto; min-height: 100%;">

        <!-- Main Header -->
        <header class="main-header">

          <!-- Logo -->
          @guest
          <a href="/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>m</b>C</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>my</b>Car</span>
          </a>
          @else
          <a href="/home" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>m</b>C</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>my</b>Car</span>
          </a>
          @endguest

          <!-- Header Navbar -->
              <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                  <span class="sr-only">Toggle navigation</span>
                </a>

                      <!-- Navbar Right Menu -->
                  <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav navbar-right">
                        @guest
                            <li><a href="{{ route('login') }}">Zaloguj się</a></li>
                            <li><a href="{{ route('register') }}">Zarejestruj się</a></li>
                            <li><a href=""> </a></li>
                        @else
                    <!-- Notifications: style can be found in dropdown.less -->
                      <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <i class="fa fa-bell-o"></i>
                          @if($results->count() > 0)
                          <span class="label label-warning">{{ $results->count() }}</span>
                          @endif
                        </a>
                        <ul class="dropdown-menu">
                          @if($results->count() > 0)
                            @if($results->count() > 1 && $results->count() < 5)
                            <li class="header center">Masz <b>{{ $results->count() }}</b> powiadomienia</li>
                            @elseif($results->count() > 4)
                            <li class="header center">Masz <b>{{ $results->count() }}</b> powiadomień</li>
                            @else
                            <li class="header center">Masz <b>{{ $results->count() }}</b> powiadomienie</li>
                            @endif
                          @else
                          <li class="header center">Brak aktualnych powiadomień</li>
                          @endif
                          <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                              @if($results->count())
                              @foreach($results as $notification)
                              @if($notification->data[0]['category'] == 'Ubezpieczenie')
                              <li id="showNotification" value="{{$notification->id}}">
                                <a href="#">
                                  <i class="fa fa-shield text-blue"></i> {{ $notification->data[0]['category'] }} &nbsp;&nbsp;<b> {{ $notification->data[0]['date_event'] }} </b>
                                </a>
                              </li>
                              @elseif($notification->data[0]['category'] == 'Serwis')
                              <li id="showNotification" value="{{$notification->id}}">
                                <a href="#">
                                  <i class="fa fa-wrench text-red"></i> {{ $notification->data[0]['category'] }} &nbsp;&nbsp;<b> {{ $notification->data[0]['date_event'] }} </b>
                                </a>
                              </li>
                              @elseif($notification->data[0]['category'] == 'Przeglad techniczny')
                              <li id="showNotification" value="{{$notification->id}}">
                                <a href="#">
                                  <i class="fa fa-stethoscope text-black"></i> {{ $notification->data[0]['category'] }} &nbsp;&nbsp;<b> {{ $notification->data[0]['date_event'] }} </b>
                                </a>
                              </li>
                              @endif
                              @endforeach
                              @endif
                            </ul>
                          </li>
                          @if($readNotifications->count())
                          <li class="footer" id="showAllReadNotifications">
                            <a href="#">Przeczytane powiadomienia</a>
                          </li>
                          @endif
                        </ul>
                      </li>

                          <!-- User Account Menu -->
                            <li class="dropdown user user-menu">
                              <!-- Menu Toggle Button -->
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <!-- The user image in the navbar-->
                                <img src="{{ asset("photos/Robert_Rezler_2019.png") }}" class="user-image" alt="User Image">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{{ Auth::user()->first_name }}</span>
                              </a>
                              <ul class="dropdown-menu">

                                 <li>
                                   @if(isset($car_one))
                                    <a href="{{ route('showUser', [Auth::user()->id, $car_one->id]) }}" id="a_profile"><i class="fa fa-user"></i>Mój Profil</a>
                                   @else
                                    <a href="{{ route('showUserNotCars', Auth::user()->id) }}" id="a_profile"><i class="fa fa-user"></i>Mój Profil</a>
                                   @endif
                                 </li>
                                 <li class="divider"></li>
                                 <li>
                                   <a href="{{ route('logout') }}"   onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>Wyloguj się</a>

                                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                          @csrf
                                      </form>
                                 </li>
                                </ul>
                              </li>
                           @endguest
                        </ul>
                  </div>
            </nav>
          </header>
<!--Lewa kolumna nawigacyjna-->

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    @yield('sidebar')
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    @yield('content_header')
  </section>

  <!-- Main content -->
  <section class="content container-fluid">
    @include('notifications.showModal')
    @include('notifications.showAllReadModal')
    @yield('content')
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      <b>my</b>Car
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018/19 </strong>
  </footer>

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <script src="{{ asset("adminlte/jquery/dist/jquery.min.js") }}"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset("adminlte/bootstrap/dist/js/bootstrap.min.js") }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset("adminlte/dist/js/adminlte.min.js") }}"></script>
  <!-- FastClick -->
  <script src="{{ asset("adminlte/fastclick/lib/fastclick.js") }}"></script>
  <!-- Optionally, you can add Slimscroll and FastClick plugins.
       Both of these plugins are recommended to enhance the
       user experience. -->
       <!-- DataTables -->
  <script src="{{ asset("adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
  <script src="{{ asset("adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
      <!-- SlimScroll -->
  <script src="{{ asset("adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js") }}"></script>
       <!-- AdminLTE for demo purposes -->
  <script src="{{ asset("adminlte/dist/js/demo.js") }}"></script>
  <!-- Select2 -->
  <script src="{{ asset("adminlte/select2/dist/js/select2.full.min.js") }}"></script>
  <!-- bootstrap datepicker -->
  <script src="{{ asset("adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")}}"></script>
  <script src="{{ asset("js/editModal.js")}}"></script>
  <script src="{{ asset("js/removeModal.js")}}"></script>
  <script src="{{ asset("js/ajaxNotification/show.js")}}"></script>
  <script src="{{ asset("js/ajaxNotification/showAllRead.js")}}"></script>
  <script src="{{ asset("js/ajaxNotification/removeAllRead.js")}}"></script>
  <script src="{{ asset("js/ajaxNotification/markAsRead.js")}}"></script>
  <script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date picker
  $('#datepicker').datepicker({
    autoclose: true
  })
  $('#datepicker1').datepicker({
    autoclose: true
  })
  $('#datepicker2').datepicker({
    autoclose: true
  })
  $('#datepicker3').datepicker({
    autoclose: true
  })
  $('#datepicker4').datepicker({
    autoclose: true
  })
  $('#datepicker5').datepicker({
    autoclose: true
  })
  $('#datepicker6').datepicker({
    autoclose: true
  })
  $('#datepicker7').datepicker({
    autoclose: true
  })
  $('#datepicker8').datepicker({
    autoclose: true
  })
  $('#datepicker9').datepicker({
    autoclose: true
  })

})

  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

  </script>
    </div>

</body>
</html>
