<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>myCar| Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ ("adminlte/bootstrap/dist/css/bootstrap.min.css") }}" type="text/css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ ("adminlte/font-awesome/css/font-awesome.min.css") }}" type="text/css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ ("adminlte/Ionicons/css/ionicons.min.css") }}" type="text/css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ ("adminlte/dist/css/AdminLTE.min.css") }}" type="text/css">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ ("adminlte/plugins/iCheck/square/blue.css") }}" type="text/css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="/"><b>my</b>Car</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Zarejestruj nowe członkostwo</p>

    <form action="{{ route('register') }}" method="post">
      @csrf

<!--Pole email-->
                  <div class="form-group has-feedback">
                    <input type="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                      @if ($errors->has('email'))
                          <span class="invalid-feedback">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                  </div>

<!--Pole password-->
                      <div class="form-group has-feedback">
                        <input type="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Hasło" required>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                          @if ($errors->has('password'))
                              <span class="invalid-feedback">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                      </div>

<!--Pole confirm_password-->
                              <div class="form-group has-feedback">
                                <input type="password" id="password_confirmation" class="form-control" name="password_confirmation" placeholder="Powtórz hasło" required>
                                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                              </div>

<!--Pole first_name-->
                        <div class="form-group has-feedback">
                          <input type="text" id="first_name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" placeholder="Imię" required>
                          <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>

<!--Pole last_name-->
                        <div class="form-group has-feedback">
                          <input type="text" id="last_name" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" placeholder="Nazwisko" required>
                          <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>

<!--Pole phone_number-->
                          <div class="form-group has-feedback">
                            <input type="text" id="phone_number" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ old('phone_number') }}" placeholder="Numer telefonu" required>
                            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                              @if ($errors->has('phone_number'))
                                  <span class="invalid-feedback">
                                      <strong>{{ $errors->first('phone_number') }}</strong>
                                  </span>
                              @endif
                          </div>

                          <div class="row">
                            <div class="col-xs-8">
                              <div class="checkbox icheck">
                                <label>
                                  <input type="checkbox"> Zagadzam się na <a href="#">warunki</a>
                                </label>
                              </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-xs-4">
                              <button type="submit" class="btn btn-primary btn-block btn-flat">Zarejestruj</button>
                            </div>
                            <!-- /.col -->
                          </div>
      </form>

      <a href="/login" class="text-center">Mam już członkostwo</a>
       </div>
       <!-- /.form-box -->
     </div>
     <!-- /.register-box -->

     <!-- jQuery 3 -->
<script src="{{ ("adminlte/jquery/dist/jquery.min.js") }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ ("adminlte/bootstrap/dist/js/bootstrap.min.js") }}"></script>
<!-- iCheck -->
<script src="{{ ("adminlte/plugins/iCheck/icheck.min.js") }}"></script>
<script>
 $(function () {
   $('input').iCheck({
     checkboxClass: 'icheckbox_square-blue',
     radioClass: 'iradio_square-blue',
     increaseArea: '20%' /* optional */
   });
 });
</script>
</body>
</html>
