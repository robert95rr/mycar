<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>myCar| Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset("adminlte/bootstrap/dist/css/bootstrap.min.css") }}" type="text/css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset("adminlte/font-awesome/css/font-awesome.min.css") }}" type="text/css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset("adminlte/Ionicons/css/ionicons.min.css") }}" type="text/css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset("adminlte/dist/css/AdminLTE.min.css") }}" type="text/css">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset("adminlte/plugins/iCheck/square/blue.css") }}" type="text/css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 </head>
 <body class="hold-transition login-page">
 <div class="login-box">
   <div class="login-logo">
     <a href="/"><b>my</b>Car</a>
   </div>
   <!-- /.login-logo -->
   <div class="login-box-body">
     <p class="login-box-msg">Zaloguj się, aby rozpocząć sesję</p>


                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group has-feedback">
                        <input type="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" autofocus required>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                          @if ($errors->has('email'))
                              <span class="invalid-feedback">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                        </div>

                        <div class="form-group has-feedback">
                        <input type="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Hasło">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                          @if ($errors->has('password'))
                              <span class="invalid-feedback">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                        </div>

                        <div class="row">
                          <div class="col-xs-8">
                            <div class="checkbox icheck">
                              <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Zapamiętaj mnie
                              </label>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Zaloguj</button>
                          </div>
                          <!-- /.col -->
                        </div>
                      </form>

                      <a href="{{ route('password.request') }}">Zapomniałem hasła</a><br>
                         <a href="/register" class="text-center">Zarejestruj się</a>


          </div><!-- /.login-box-body -->
      </div><!-- /.login-box -->

      <!-- jQuery 3 -->
    <script src="{{ asset("adminlte/jquery/dist/jquery.min.js") }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset("adminlte/bootstrap/dist/js/bootstrap.min.js") }}"></script>
    <!-- iCheck -->
    <script src="{{ asset("adminlte/plugins/iCheck/icheck.min.js") }}"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' /* optional */
        });
      });
    </script>
</body>
</html>
