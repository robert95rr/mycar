@extends('layouts.app')

@section('sidebar')

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form>
    <!-- /.search form -->
    @guest
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">HEADER</li>
      <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
          <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#">Link in level 2</a></li>
              <li><a href="#">Link in level 2</a></li>
            </ul>
          </li>
        </ul>
        @else
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">HEADER</li>
              <li class="active treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Auta</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li class="treeview">
                    <a href="#"> Moje Auta
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                      <ul class="treeview-menu">
                        @foreach ($car as $cars)
                        <li><a href="{{ route('showCar', $cars->id) }}"><i class="fa fa-dot-circle-o"></i>{{$cars->make}}&nbsp;{{$cars->model}}</a></li>
                        @endforeach
                      </ul>
                  </li>
                    <li><a href="{{ route('addFormCar') }}">Dodaj</a></li>
                </ul>
              </li>
              <li class="header"></li>
              <li>
                <a href="{{ route('showMileage', $car_one->id) }}">
                    <i class="fa fa-road"></i> <span>Przebieg</span><!--Przebieg auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showFuel', $car_one->id) }}">
                  <i class="fa fa-tint"></i> <span>Paliwo</span><!--Paliwo auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showInsurance', $car_one->id) }}">
                  <i class="fa fa-shield"></i> <span>Ubezpieczenie</span><!--Ubezpieczenie auta-->
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-dot-circle-o"></i>
                  <span>Ogumienie</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @foreach ($tire as $tires)
                  <li><a href="{{ route('showTire', [$tires->id, $car_one->id]) }}"><i class="fa fa-circle-o"></i>{{$tires->manufacturer}}&nbsp;{{$tires->model}}
                    @if($tires->type_id == 1)
                    <small class="label pull-right bg-yellow">Letnie</small>
                    @elseif($tires->type_id == 2)
                    <small class="label pull-right bg-blue">Zimowe</small>
                    @else
                    <small class="label pull-right bg-green">Całoroczne</small>
                    @endif
                    </a></li>
                  @endforeach
                  <li><a href="{{ route('addFormTire', $car_one->id) }}">Dodaj</a></li>
                </ul>
              </li><!--Ogumienie auta-->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-wrench"></i>
                  <span>Serwis</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{ route('showService',  [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Serwis Auta</a></li>
                  <li><a href="{{ route('showServicePlan',  [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Plan Serwisowy</a></li>
                </ul>
              </li><!--Serwis auta-->
              <li>
                <a href="{{ route('showCosts', $car_one->id) }}">
                  <i class="fa fa-usd"></i> <span>Koszty</span><!--Koszty auta-->
                </a>
              </li>
              <li class="header">POWIADOMIENIA</li>
              <li>
                <a href="{{ route('showTechnicalInspection', $car_one->id) }}">
                  <i class="fa fa-stethoscope"></i><span class="label bg-olive"> Przegląd techniczny </span>
                </a>
              </li>
        </ul>
          @endguest

@endsection

@section('content_header')
      <h1>
        {{$car_one->make}}&nbsp;{{$car_one->model}} <small>| Paliwo</small></h1><!--Nazwa auta (selected)-->
     <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('showCar', $car_one->id) }}">{{$car_one->make}}&nbsp;{{$car_one->model}}</a></li>
        <li class="active">Paliwo</li>
      </ol><br/>
@endsection

@section('content')

<div class="row">
  <div class="col-md-5 col-md-offset-1">
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Oblicz koszt w przedziale czasowym</h3>
            </div>
            <form method="post" id="frmCalculateCost" name="frmCalculateCost">
            @csrf
            <div class="box-body">
              <input type="hidden" id="carIdFS" name="carIdFS" value="{{$car_one->id}}">
              <div class="row">
                <div class="col-xs-4">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                  <input type="text" id="datepicker2" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date2" placeholder="____-__-__" >
                  </div>
                    @if ($errors->has('date2'))
                            <span class="help-block">
                                <strong>{{ $errors->first('date2') }}</strong>
                            </span>
                    @endif
                  </div>
                <div class="col-xs-4">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                  <input type="text" id="datepicker3" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date3" value=" " >
                  </div>
                    @if ($errors->has('date3'))
                            <span class="help-block">
                                <strong>{{ $errors->first('date3') }}</strong>
                            </span>
                    @endif
              </div>
              <div class="col-xs-2">
                  <button type="button" class="btn btn-primary btn-sm" id="btn-calculateFS" value="calculateFS">Oblicz</button>
              </div>
                </div>
              </div>
            </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <div class="col-md-5">
              <div class="box box-primary">
                      <div class="box-header with-border">
                        <h3 class="box-title">Oblicz średnie zużycie w przedziale czasowym</h3>
                      </div>
                      <form method="post" id="frmCalculateMediumUsed" name="frmCalculateMediumUsed">
                      @csrf
                      <div class="box-body">
                        <input type="hidden" id="carIdMU" name="carIdMU" value="{{$car_one->id}}">
                        <div class="row">
                          <div class="col-xs-4">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                            <input type="text" id="datepicker4" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date_2" placeholder="____-__-__" >
                            </div>
                              @if ($errors->has('date_2'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('date_2') }}</strong>
                                      </span>
                              @endif
                            </div>
                          <div class="col-xs-4">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                            <input type="text" id="datepicker5" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date_3" value=" " >
                            </div>
                              @if ($errors->has('date_3'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('date_3') }}</strong>
                                      </span>
                              @endif
                        </div>
                        <div class="col-xs-2">
                            <button type="button" class="btn btn-primary btn-sm" id="btn-calculateMU" value="calculateMU">Oblicz</button>
                        </div>
                          </div>
                        </div>
                      </form>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
        </div><br>

@include('fuel.forms.addform')

    <!--Table(Fuel)-->
    <div class="row">
          <div class="center-block col-md-11" style="float: none;">
            <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Tankowanie | <b>{{$car_one->fuel_type}}</b></h3>
              <button type="button" class="btn bg-olive btn-sm pull-right" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i>
                Dodaj
              </button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-sm table-bordered table-striped">
                <thead>
                <tr>
                  <th>Ilość paliwa przed [L]</th>
                  <th>Ilość paliwa wlanego [L]</th>
                  <th>Cena [zł/L]</th>
                  <th>Przebieg [Km]</th>
                  <th>Data</th>
                  <th>Koszt</th>
                  <th>Zużycie paliwa</th>
                  <th>Stacja Paliw</th>
                  <th>#Akcja</th>
                </tr>
                </thead>
                <tbody>
            @foreach ($fuel as $fuels)
                <tr>
                  <td>{{ $fuels->amount_before }} l</td>
                  <td>{{ $fuels->amount_refueled }} l</td>
                  <td>{{ $fuels->refueling_price }}</td>
                  <td>{{ $fuels->mileage }}</td>
                  <td>{{ $fuels->date }}</td>
                  <td class="total_cost">{{ $fuels->total_cost }} zł</td>
                  <td class="fuel_usage">{{ $fuels->fuel_usage }} l/100km</td>
                  <td>{{ $fuels->place }}</td>
                  <td>
                    <button class="btn btn-warning btn-xs" data-toggle="modal" data-amountbefore = "{{$fuels->amount_before}}" data-amountrefueled = "{{$fuels->amount_refueled}}" data-refuelingprice = "{{$fuels->refueling_price}}" data-mileage = "{{$fuels->mileage}}" data-date = "{{$fuels->date}}" data-place = "{{$fuels->place}}" data-fuelid = {{$fuels->id}} data-target="#modal-default-edit-f">Edytuj</button>
                    <button class="btn btn-danger btn-xs" data-fuelid = {{$fuels->id}} data-toggle="modal" data-target="#modal-danger-remove-f">Usuń</button>
                  </td>
                </tr>
            @endforeach
              </tbody>
              <tfoot>
              <tr>
                <th>Ilość paliwa przed [L]</th>
                <th>Ilość paliwa wlanego [L]</th>
                <th>Cena [zł/L]</th>
                <th>Przebieg [Km]</th>
                <th>Data</th>
                <th>Koszt</th>
                <th>Zużycie paliwa</th>
                <th>Stacja Paliw</th>
                <th></th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div><!-- /.row --><br>

@if(count($fuel) != 0)
  <div class="row">
    <div class="center-block col-md-10" style="float: none;">
      <div class="box box-primary">
        <div class="box-header with-border"><i class="fa fa-bar-chart"></i><h2 class="box-title">Wykresy</h2>
        </div>
        <div class="box-body">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-dollar"></i> Koszt</a></li>
              <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-tachometer"></i> Zużycie</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <div class="col-md-6 col-md-offset-6">
                  <button type="button" class="btn btn-danger btn-sm pull-right open_modalChartC" value="{{$car_one->id}}"><i class="fa fa-calendar"></i> Wybierz zakres</button>
                </div><br><hr>
                <canvas id="barChart" style="height:100px; width:300px;"></canvas>
              <!-- /.tab-pane -->
              </div>
              <div class="tab-pane" id="tab_2">
                <div class="col-md-12">
                  <button type="button" class="btn btn-primary btn-sm pull-right open_modalChartU" value="{{$car_one->id}}"><i class="fa fa-calendar"></i> Wybierz zakres</button>
                </div><br><hr>
                <canvas id="barChart1" style="height:100px; width:300px;"></canvas>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
      </div>
    </div>
  </div>
@endif
<!--FORMS-->
@include('fuel.forms.selectrange_chartcost')
@include('fuel.forms.selectrange_chartusage')
@include('fuel.forms.editform')
@include('fuel.forms.remove')

    <!--Window modal with cost fuel-->
      <div class="modal modal-danger fade" id="modal-calculateFS">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title cost">Koszt paliwa</h4>
              </div>
              <div class="modal-body">
                <h3 id="cost_CalcFS">Koszt zł</h3>
                <hr>
                <p id="title_CalcFS">data</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Ok</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

  <!--Window modal with medium used fuel -->
    <div class="modal modal-primary fade" id="modal-calculateMU">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title usage">Średnie zużycie paliwa</h4>
            </div>
            <div class="modal-body">
              <h3 id="used_CalcMU">Koszt zł</h3>
              <hr>
              <p id="date_CalcMU">data</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Ok</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


  <!-- jQuery 3 -->
  <script src="{{ asset("adminlte/jquery/dist/jquery.min.js") }}"></script>
  <script src="{{ asset("js/ajaxFuel/calculateCostInTimeout.js") }}"></script>
  <script src="{{ asset("js/ajaxFuel/calculateUsageInTimeout.js") }}"></script>
  <script src="{{ asset("js/ajaxFuel/showChartCost.js") }}"></script>
  <script src="{{ asset("js/ajaxFuel/showChartUsage.js") }}"></script>

@endsection
