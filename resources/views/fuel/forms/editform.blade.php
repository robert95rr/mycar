<!--Formularz edycji tankowania w oknie modalnym-->
<div class="modal fade" id="modal-default-edit-f">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edycja Tankowania</h4>
      </div>
      <form method="post" action="{{ route('updateFuel', $car_one->id) }}" >
      {{ method_field('PATCH') }}
      {{ csrf_field() }}
      <div class="modal-body">
        <input type="hidden" name="fuel_id" id="fuel_id" value="">
        <div class="row">
<!--Pole - Ilosc paliwa przed tankowaniem(litry)-->
          <div class="col-xs-4">
            <div class="input-group">
            <input type="text" id="amount_before" class="form-control" name="amount_before">
              <span class="input-group-addon">L</span>
          </div>
          @if ($errors->has('amount_before'))
                  <span class="help-block">
                      <strong>{{ $errors->first('amount_before') }}</strong>
                  </span>
          @endif
        </div>
<!--Pole - Ilosc paliwa zatankowanego(litry)-->
          <div class="col-xs-4">
            <div class="input-group">
            <input type="text" id="amount_refueled" class="form-control" name="amount_refueled">
              <span class="input-group-addon">L</span>
            </div>
            @if ($errors->has('amount_refueled'))
                      <span class="help-block">
                          <strong>{{ $errors->first('amount_refueled') }}</strong>
                      </span>
            @endif
            </div>
<!--Pole - Cena paliwa(zł/L)-->
            <div class="col-xs-4">
              <div class="input-group">
                <input type="text" id="refueling_price" class="form-control" name="refueling_price">
                  <span class="input-group-addon">zł/L</span>
              </div>
              @if ($errors->has('refueling_price'))
                      <span class="help-block">
                          <strong>{{ $errors->first('refueling_price') }}</strong>
                      </span>
              @endif
            </div>
          </div>
          <br>
<!--Pole - Przebieg(km)-->
        <div class="row">
          <div class="col-xs-6">
            <div class="input-group">
            <input type="text" id="mileage" class="form-control" name="mileage">
              <span class="input-group-addon">Km</span>
            </div>
            @if ($errors->has('mileage'))
                      <span class="help-block">
                          <strong>{{ $errors->first('mileage') }}</strong>
                      </span>
            @endif
          </div>
<!--Pole - Data tankowania-->
            <div class="col-xs-6">
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
              <input type="text" id="datepicker" class="form-control pull-right" data-date-format = "yyyy-mm-dd" name="date">
                @if ($errors->has('date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
              @endif
            </div>
          </div>
        </div><br>
<!--Pole - Miejsce tankowania(Stacja paliw, miejscowosc)-->
        <div class="row">
          <div class="col-xs-12">
            <div class="input-group">
            <input type="text" id="place" class="form-control" name="place">
              <div class="input-group-addon">
                <i class="fa fa-map-marker"></i>
              </div>
            </div>
            @if ($errors->has('place'))
                      <span class="help-block">
                          <strong>{{ $errors->first('place') }}</strong>
                      </span>
            @endif
            </div>
          </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Aktualizuj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
