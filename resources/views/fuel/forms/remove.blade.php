<!--Formularz usuwania tankowania w oknie modalnym-->
    <div class="modal modal-danger fade" id="modal-danger-remove-f">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title delete">Usuwanie Tankowania</h4>
                  </div>
                  <form method="post" action="{{ route('removeFuel', $car_one->id) }}" >
                  {{ method_field('delete') }}
                  {{ csrf_field() }}
                  <div class="modal-body">
                    <p class="delete">Czy na pewno chcesz usunąć to tankowanie ?</p>
                      <input type="hidden" name="fuel_id" id="fuel_idDalete" value="">
                      <input type="hidden" name="car_id" id="car_idDelete" value="{{$car_one->id}}">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Nie</button>
                    <button class="btn btn-outline">Tak</button>
                  </div>
                </form>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
