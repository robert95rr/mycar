<!--Formularz wyboru zakresu czasowego dla wykresu z użyciami paliwa-->
<div class="modal fade" id="modal-default-selectDates-chartUsage">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Wybierz zakres</h4>
      </div>
      <form id="selectDates1" name="selectDates1" class="form-horizontal" novalidate="">
      {{ csrf_field() }}
      <div class="modal-body">
        <input type="hidden" id="car_idUsage" name="car_id" value="{{$car_one->id}}">
        <div class="row">
          <div class="col-xs-6">
            <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Data początkowa">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            <input type="text" id="datepicker9" class="form-control pull-right" name="date3" data-date-format = "yyyy-mm-dd">
              </div>
          </div><!--Drugie pole-->
          <div class="col-xs-6">
            <div class="input-group date" data-toggle="tooltip" data-placement="top" title="Data końcowa">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            <input type="text" id="datepicker8" class="form-control pull-right" name="date4" data-date-format = "yyyy-mm-dd">
              </div>
          </div>
        </div>
      </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
    <button type="button" class="btn btn-primary" id="btn-showChartU" value="showChartU">Pokaż</button>
  </div>
</form>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
