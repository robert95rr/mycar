<!doctype html>
<html lang="{{ app()->getLocale() }}" style="height: auto; min-height: 100%;">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport">

        <title>myCar_v2</title>

        <link rel="stylesheet" href="{{ asset("adminlte/bootstrap/dist/css/bootstrap.min.css") }}" type="text/css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset("adminlte/font-awesome/css/font-awesome.min.css") }}" type="text/css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{ asset("adminlte/Ionicons/css/ionicons.min.css") }}" type="text/css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset("adminlte/dist/css/AdminLTE.min.css") }}" type="text/css">
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect. -->
        <link rel="stylesheet" href="{{ asset("adminlte/dist/css/skins/skin-green.min.css") }}" type="text/css">
        <link rel="stylesheet" href="{{ asset("css/welcome.css") }}" type="text/css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>

    <body class="hold-transition skin-green layout-top-nav" style="height: auto; min-height: 100%;">
      <div class="wrapper" style="height: auto; min-height: 100%;">

        <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>m</b>C</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>my</b>Car</span>
    </a>

    <!-- Header Navbar -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->

          <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        @if (Route::has('login'))
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          @auth
              <li><a href="{{ url('/home') }}">Home</a></li>
              <li><a href="#"></a></li>
          @else
              <li><a href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in"></span> &nbsp;Zaloguj się</a></li>
              <li><a href="{{ route('register') }}"><span class="glyphicon glyphicon-user"></span> &nbsp;Zarejestruj się</a></li>

          @endauth
        </ul>
          @endif
      </div>
    </nav>
  </header>
  <!-- Lewa kolumna nawigacyjna -->

  <!-- Left side column. contains the logo and sidebar -->
  <!--<aside class="main-sidebar">



  <!-Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <br/><br/><br/><br/>
      <h1>
        Witamy w <b>my</b>Car<br/>
        <small>Zarządzaj swoim samochodem</small>
      </h1>
    </section>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <script src="{{ asset("adminlte/jquery/dist/jquery.min.js") }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset("adminlte/bootstrap/dist/js/bootstrap.min.js") }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset("adminlte/dist/js/adminlte.min.js") }}"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. -->



        </div>
    </body>
</html>
