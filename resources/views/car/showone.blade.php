@extends('layouts.app')

@section('sidebar')

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form>
    <!-- /.search form -->
    @guest
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">HEADER</li>
      <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
          <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#">Link in level 2</a></li>
              <li><a href="#">Link in level 2</a></li>
            </ul>
          </li>
        </ul>
        @else
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MENU</li>
              <li class="active treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Auta</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li class="treeview">
                    <a href="#"> Moje Auta
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                      <ul class="treeview-menu">
                        @foreach ($car as $cars)
                        <li><a href="{{ route('showCar', $cars->id) }}"><i class="fa fa-dot-circle-o"></i>{{$cars->make}}&nbsp;{{$cars->model}}</a></li>
                        @endforeach
                      </ul>
                  </li>
                    <li><a href="{{ route('addFormCar') }}">Dodaj</a></li>
                </ul>
              </li>
              <li class="header"></li>
              <li>
                <a href="{{ route('showMileage', $car_one->id) }}">
                    <i class="fa fa-road"></i> <span>Przebieg</span><!--Przebieg auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showFuel', $car_one->id) }}">
                  <i class="fa fa-tint"></i> <span>Paliwo</span><!--Paliwo auta-->
                </a>
              </li>
              <li>
                <a href="{{ route('showInsurance', $car_one->id) }}">
                  <i class="fa fa-shield"></i> <span>Ubezpieczenie</span><!--Ubezpieczenie auta-->
                </a>
              </li>
              <li class="treeview">
              <a href="#">
                <i class="fa fa-dot-circle-o"></i>
                <span>Ogumienie</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @foreach ($tire as $tires)
                <li ><a href="{{ route('showTire', [$tires->id, $car_one->id]) }}"><i class="fa fa-circle-o"></i>{{$tires->manufacturer}}&nbsp;{{$tires->model}}
                  @if($tires->type_id == 1)
                  <small class="label pull-right bg-yellow">Letnie</small>
                  @elseif($tires->type_id == 2)
                  <small class="label pull-right bg-blue">Zimowe</small>
                  @else
                  <small class="label pull-right bg-green">Całoroczne</small>
                  @endif
                  </a></li>
                @endforeach
                <li><a href="{{ route('addFormTire', $car_one->id) }}">Dodaj</a></li><!--Ogumienie auta-->
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-wrench"></i>
                <span>Serwis</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('showService', [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Serwis Auta</a></li>
                <li><a href="{{ route('showServicePlan', [$car_one->id, $car_one->make, $car_one->model]) }}"><i class="fa fa-circle-o"></i>Plan Serwisowy</a></li>
              </ul>
            </li><!--Serwis auta-->
              <li>
                <a href="{{ route('showCosts', $car_one->id) }}">
                  <i class="fa fa-usd"></i> <span>Koszty</span><!--Koszty auta-->
                </a>
              </li>
              <li class="header">POWIADOMIENIA</li>
              <li>
                <a href="{{ route('showTechnicalInspection', $car_one->id) }}">
                  <i class="fa fa-stethoscope"></i><span class="label bg-olive"> Przegląd techniczny </span>
                </a>
              </li>
        </ul>
          @endguest

@endsection

@section('content_header')
      <h1>
       Auto
       <small>| Profil</small>
     </h1>
     <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Auta</a></li>
        <li><a href="#">Moje Auta</a></li>
        <li class="active">{{$car_one->make}}&nbsp;{{$car_one->model}}</li>
      </ol><br/>
@endsection


@section('content')

      <div class="row">
         <div class="center-block col-md-5" style="float: none;">

           <!-- Profile Image -->
           <div class="box box-primary">
             <div class="box-body box-profile"><br>
               @if(!empty($photo_car))
               <img class="img-responsive img-thumbnail center" data-toggle="modal" title="Edytuj zdjęcie" data-target="#modal-default-photo-edit" src="/photos/{{$photo_car->name}}" alt="Car profile picture"><!--180x128-->
               @else
               <img class="img-responsive img-thumbnail addPhoto center" data-toggle="modal" data-target="#modal-default-photo" src="/photos/default.jpg" alt="Car profile picture"><!--180x128-->
               @endif
               <h3 class="profile-username text-center">{{$car_one->make}}&nbsp;{{$car_one->model}}</h3>

               <p class="text-muted text-center">{{$vehicle_type_one->name}}</p>

               <ul class="list-group list-group-unbordered">
                 <li class="list-group-item">
                   <b>Rok produkcji</b> <a class="pull-right">{{$car_one->year_production}}</a>
                 </li>
                 <li class="list-group-item">
                   <b>Paliwo</b> <a class="pull-right">{{$car_one->fuel_type}}</a>
                 </li>
                 <li class="list-group-item">
                   <b>Pojemność skokowa</b> <a class="pull-right">{{$car_one->engine_size}} cm3</a>
                 </li>
                 <li class="list-group-item">
                   <b>Moc</b> <a class="pull-right">{{$car_one->engine_power}} KM</a>
                 </li>
                 <li class="list-group-item">
                   <b>Kolor</b> <a class="pull-right">{{$car_one->interior_colour}}</a>
                 </li>
                 <li class="list-group-item">
                   <b>Numer rejestracji</b> <a class="pull-right">{{$car_one->registration_number}}</a>
                 </li>
                 <li class="list-group-item">
                   <b>Numer VIN</b> <a class="pull-right">{{$car_one->vin_number}}</a>
                 </li>
               </ul>
               <button type="button" class="btn btn-warning margin btn-sm pull-right" data-toggle="modal" data-target="#modal-default"><i class="fa fa-edit"></i>
                 Edytuj
               </button>
               <button type="button" class="btn btn-danger margin btn-sm" data-toggle="modal" data-target="#modal-danger"><i class="fa fa-trash-o"></i>
                 Usuń
               </button>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->
         </div>
        <!-- /.col -->
      </div>
    <!-- /.row -->
<!--FORMS-->
@include('car.forms.editform')
@include('car.forms.remove')

  <div class="modal fade" id="modal-default-photo">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Dodawanie Zdjęcia</h4>
            </div>
            <form method="POST" action="{{ route('addPhotoCar', $car_one->id) }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-4">
                      <input type="file" name="name" id="name">
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
                  <button type="submit" class="btn btn-primary">Dodaj</button>
                </div>
              </form>
          </div>
            <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
  </div>
    <!-- /.modal -->

@isset($photo_select)
    <div class="modal fade" id="modal-default-photo-edit">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Edycja Zdjęcia</h4>
          </div>
          <form method="POST" action="{{ route('updatePhotoCar', $photo_select->id) }}" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
              <div class="row">
                <img class="img-responsive img-rounded center" src="/photos/{{$photo_select->name}}" width="200" height="90" alt="Photo car to edit"><br>
                <div class="col-md-4">
                  <input type="file" name="name" id="namePhotoUpdate"/>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
              <button type="submit" class="btn btn-primary">Aktualizuj</button>
            </div>
          </form>
        </div>
      </div>
    </div>
@endisset

@endsection
