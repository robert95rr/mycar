<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edycja Auta</h4>
      </div>
        <form class="form-horizontal" method="POST" action="{{ route('updateCar', $car_one->id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-body">
        <div class="row">
<!--Pole - marka-->
          <div class="col-xs-6">
            <input type="text" id="make" class="form-control" name="make" required value="{{ old('make') ?: $car_one->make }}">

              @if ($errors->has('make'))
                      <span class="help-block">
                          <strong>{{ $errors->first('make') }}</strong>
                      </span>
              @endif
          </div>
<!--Pole - model-->
          <div class="col-xs-6">
            <input type="text" id="model" class="form-control" name="model" required value="{{ old('model') ?: $car_one->model }}">

              @if ($errors->has('model'))
                      <span class="help-block">
                          <strong>{{ $errors->first('model') }}</strong>
                      </span>
              @endif
          </div>

        </div>
        <br>

<!--Pole - typ nadwozia-->
        <div class="row">
            <div class="col-xs-6">
              <select class="form-control" id="vehicle_type_id" name="vehicle_type_id" required>
                  <option value="">Wybierz</option>
                  @foreach ($vehicle_types as $vehicle_type)
                      @if ($vehicle_type->id == $car_one->vehicle_type_id)
                        <option value="{{$vehicle_type->id}}" selected="selected">{{$vehicle_type->name}}</option>
                      @else
                        <option value="{{$vehicle_type->id}}">{{$vehicle_type->name}}</option>
                      @endif
                  @endforeach
              </select>
              @if ($errors->has('vehicle_type_id'))
                      <span class="help-block">
                          <strong>{{ $errors->first('vehicle_type_id') }}</strong>
                      </span>
              @endif
            </div>
<!--Pole - typ paliwa-->
            <div class="col-xs-6">
              <select class="form-control" id="fuel_type" name="fuel_type" required>
                            <option value="">Wybierz</option>
                             <option @if(old('fuel_type',$car_one->fuel_type) == 'Benzyna') selected @endif>Benzyna</option>
                             <option @if(old('fuel_type',$car_one->fuel_type) == 'Diesel') selected @endif>Diesel</option>
                             <option @if(old('fuel_type',$car_one->fuel_type) == 'Benzyna+LPG') selected @endif>Benzyna+LPG</option>
                             <option @if(old('fuel_type',$car_one->fuel_type) == 'Benzyna+CNG') selected @endif>Benzyna+CNG</option>
                             <option @if(old('fuel_type',$car_one->fuel_type) == 'Elektryczny') selected @endif>Elektryczny</option>
                             <option @if(old('fuel_type',$car_one->fuel_type) == 'Hybryda') selected @endif>Hybryda</option>
                             <option @if(old('fuel_type',$car_one->fuel_type) == 'Wodór') selected @endif>Wodór</option>
                           </select>
                @if ($errors->has('fuel_type'))
                        <span class="help-block">
                          <strong>{{ $errors->first('fuel_type') }}</strong>
                        </span>
                @endif
            </div>

          </div>
          <br>

<!--Pole - rok produkcji-->
          <div class="row">
              <div class="col-xs-4">
              <input type="text" id="year_production" class="form-control" name="year_production" required value="{{ old('year_production') ?: $car_one->year_production }}">
              @if ($errors->has('year_production'))
                      <span class="help-block">
                          <strong>{{ $errors->first('year_production') }}</strong>
                      </span>
              @endif
            </div>
          </div>
          <br>

<!--Pole - rozmiar silnika-->
        <div class="row">
            <div class="col-xs-6">
              <div class="input-group">
                <input type="text" id="engine_size" class="form-control" name="engine_size" required value="{{ old('engine_size') ?: $car_one->engine_size }}">
                <span class="input-group-addon">cm3</span>
                @if ($errors->has('engine_size'))
                        <span class="help-block">
                            <strong>{{ $errors->first('engine_size') }}</strong>
                        </span>
                @endif
            </div>
          </div>

<!--Pole - moc silnika-->
            <div class="col-xs-6">
              <div class="input-group">
                <input type="text" id="engine_power" class="form-control" name="engine_power" required value="{{ old('engine_power') ?: $car_one->engine_power }}">
                <span class="input-group-addon">KM</span>
                @if ($errors->has('engine_power'))
                        <span class="help-block">
                            <strong>{{ $errors->first('engine_power') }}</strong>
                        </span>
                @endif
            </div>
          </div>
        </div>
        <br>

<!--Pole - numer rejestracyjny-->
        <div class="row">
            <div class="col-xs-6">
              <input type="text" id="registration_number" class="form-control" name="registration_number" required value="{{ old('registration_number') ?: $car_one->registration_number }}">
              @if ($errors->has('registration_number'))
                      <span class="help-block">
                          <strong>{{ $errors->first('registration_number') }}</strong>
                      </span>
              @endif
            </div>

<!--Pole - numer VIN-->
            <div class="col-xs-6">
              <input type="text" id="vin_number" class="form-control" name="vin_number" required value="{{ old('vin_number') ?: $car_one->vin_number }}">
              @if ($errors->has('vin_number'))
                      <span class="help-block">
                          <strong>{{ $errors->first('vin_number') }}</strong>
                      </span>
              @endif
            </div>
          </div>
          <br>

<!--Pole - kolor zewnetrzny-->
        <div class="row">
            <div class="col-xs-4">
              <select class="form-control" id="interior_colour" name="interior_colour" required>
                  <option value="">Wybierz</option>
                  <option @if(old('interior_colour',$car_one->interior_colour) == 'Biały') selected @endif>Biały</option>
                  <option @if(old('interior_colour',$car_one->interior_colour) == 'Czarny') selected @endif>Czarny</option>
                  <option @if(old('interior_colour',$car_one->interior_colour) == 'Szary') selected @endif>Szary</option>
                  <option @if(old('interior_colour',$car_one->interior_colour) == 'Srebrny') selected @endif>Srebrny</option>
                  <option @if(old('interior_colour',$car_one->interior_colour) == 'Niebieski') selected @endif>Niebieski</option>
                  <option @if(old('interior_colour',$car_one->interior_colour) == 'Brązowy - Beżowy') selected @endif>Brązowy - Beżowy</option>
                  <option @if(old('interior_colour',$car_one->interior_colour) == 'Czerwony') selected @endif>Czerwony</option>
                  <option @if(old('interior_colour',$car_one->interior_colour) == 'Zielony') selected @endif>Zielony</option>
                  <option @if(old('interior_colour',$car_one->interior_colour) == 'Żółty - Złoty') selected @endif>Żółty - Złoty</option>
                  <option @if(old('interior_colour',$car_one->interior_colour) == 'Inny kolor') selected @endif>Inny kolor</option>
              </select>
              @if ($errors->has('interior_colour'))
                      <span class="help-block">
                          <strong>{{ $errors->first('interior_colour') }}</strong>
                      </span>
              @endif
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary">Aktualizuj</button>
      </div>
    </form>
  </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
