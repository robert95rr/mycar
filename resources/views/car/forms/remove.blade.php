<div class="modal modal-danger fade" id="modal-danger">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title delete">Usuwanie Auta</h4>
              </div>
              <div class="modal-body">
                <p class="delete">Czy na pewno chcesz usunąć to auto ?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Nie</button>
                <a class="btn btn-outline" href="{{ route('removeCar', $car_one->id) }}">Tak</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
