@extends('layouts.app')

@section('sidebar')

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form>
    <!-- /.search form -->
    @guest
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">HEADER</li>
      <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
          <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#">Link in level 2</a></li>
              <li><a href="#">Link in level 2</a></li>
            </ul>
          </li>
        </ul>
        @else
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">HEADER</li>
              <li class="active treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Auta</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li class="treeview">
                  <a href="#"> Moje Auta
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    @foreach ($car as $cars)
                    <li><a href="{{ route('showCar', $cars->id) }}"><i class="fa fa-dot-circle-o"></i>{{$cars->make}}&nbsp;{{$cars->model}}</a></li>
                    @endforeach
                  </ul>
                </li>
                <li><a href="{{ route('addFormCar') }}">Dodaj</a></li>
                </ul>
              </li>
              <li class="header"></li>
        </ul>
          @endguest

@endsection

@section('content_header')
      <h1>
       Auta - Formularz
     </h1>
     <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Auta</a></li>
        <li class="active">Dodaj Auto</li>
    </ol><br/>
@endsection


@section('content')

  <button type="button" class="btn bg-olive margin btn-sm" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i>
    Dodaj
  </button>

      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Dodawanie Auta</h4>
            </div>
            <form method="post" action="{{ route('addCar') }}" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
              <div class="row">
    <!--Pole - marka-->
                <div class="col-xs-6">
                  <input type="text" id="make" class="form-control" name="make" placeholder="Marka">

                    @if ($errors->has('make'))
                            <span class="help-block">
                                <strong>{{ $errors->first('make') }}</strong>
                            </span>
                    @endif
                </div>
    <!--Pole - model-->
                <div class="col-xs-6">
                  <input type="text" id="model" class="form-control" name="model" placeholder="Model">

                    @if ($errors->has('model'))
                            <span class="help-block">
                                <strong>{{ $errors->first('model') }}</strong>
                            </span>
                  @endif
                </div>

              </div>
              <br>

    <!--Pole - typ nadwozia-->
              <div class="row">
                  <div class="col-xs-6">
                    <select class="form-control" id="vehicle_type_id" name="vehicle_type_id">
                        <option value="">Wybierz...</option>
                        @foreach ($vehicle_types as $vehicle_type)
                            <option value="{{$vehicle_type->id}}">{{$vehicle_type->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('vehicle_type_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('vehicle_type_id') }}</strong>
                            </span>
                    @endif
                  </div>
    <!--Pole - typ paliwa-->
                  <div class="col-xs-6">
                    <select class="form-control" id="fuel_type" name="fuel_type" data-placeholder="Fuel type">
                                   <option label="Wybierz..."></option>
                                   <option>Benzyna</option>
                                   <option>Diesel</option>
                                   <option>Benzyna+LPG</option>
                                   <option>Benzyna+CNG</option>
                                   <option>Elektryczny</option>
                                   <option>Hybryda</option>
                                   <option>Wodór</option>
                                 </select>
                      @if ($errors->has('fuel_type'))
                              <span class="help-block">
                                <strong>{{ $errors->first('fuel_type') }}</strong>
                              </span>
                      @endif
                  </div>

                </div>
                <br>

    <!--Pole - rok produkcji-->
                <div class="row">
                    <div class="col-xs-4">
                    <input type="text" id="year_production" class="form-control" name="year_production" placeholder="Rok produkcji">
                    @if ($errors->has('year_production'))
                            <span class="help-block">
                                <strong>{{ $errors->first('year_production') }}</strong>
                            </span>
                    @endif
                  </div>
                </div>
                <br>

    <!--Pole - rozmiar silnika-->
              <div class="row">
                  <div class="col-xs-6">
                    <div class="input-group">
                      <input type="text" id="engine_size" class="form-control" name="engine_size" placeholder="Pojemność silnika">
                      <span class="input-group-addon">cm3</span>
                      @if ($errors->has('engine_size'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('engine_size') }}</strong>
                              </span>
                      @endif
                  </div>
                </div>

    <!--Pole - moc silnika-->
                  <div class="col-xs-6">
                    <div class="input-group">
                      <input type="text" id="engine_power" class="form-control" name="engine_power" placeholder="Moc silnika">
                      <span class="input-group-addon">KM</span>
                      @if ($errors->has('engine_power'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('engine_power') }}</strong>
                              </span>
                      @endif
                  </div>
                </div>
              </div>
              <br>

    <!--Pole - numer rejestracyjny-->
              <div class="row">
                  <div class="col-xs-6">
                    <input type="text" id="registration_number" class="form-control" name="registration_number" placeholder="Numer rejestracji - PKA_89KIT">
                    @if ($errors->has('registration_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('registration_number') }}</strong>
                            </span>
                    @endif
                  </div>

    <!--Pole - numer VIN-->
                  <div class="col-xs-6">
                    <input type="text" id="vin_number" class="form-control" name="vin_number" placeholder="Numer VIN">
                    @if ($errors->has('vin_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('vin_number') }}</strong>
                            </span>
                    @endif
                  </div>
                </div>
                <br>

    <!--Pole - kolor zewnetrzny-->
              <div class="row">
                  <div class="col-xs-4">
                    <select class="form-control" id="interior_colour" name="interior_colour" data-placeholder="Interior colour">
                        <option label="Wybierz..."></option>
                        <option>Biały</option>
                        <option>Czarny</option>
                        <option>Szary</option>
                        <option>Srebrny</option>
                        <option>Niebieski</option>
                        <option>Brązowy - Beżowy</option>
                        <option>Czerwony</option>
                        <option>Zielony</option>
                        <option>Żółty - Złoty</option>
                        <option>Inny kolor</option>
                    </select>
                    @if ($errors->has('interior_colour'))
                            <span class="help-block">
                                <strong>{{ $errors->first('interior_colour') }}</strong>
                            </span>
                    @endif
                  </div>
                </div>
                <br>
                <p class="help-block">VIN - numer identyfikacji nadwozia.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
              <button type="submit" class="btn btn-primary">Dodaj</button>
            </div>
          </form>
        </div>
          <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
    </div>
      <!-- /.modal -->

@endsection
