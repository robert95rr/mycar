<?php
use Illuminate\Support\Facades\Auth;
use App\Notifications\CarReminder;
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');
Route::get('/home', 'HomeController@index')->name('home');
//Model -> User
Route::get('user/show/profile/{id}/{carId}', 'UserController@show')->name('showUser');
Route::get('user/show/profile/{id}', 'UserController@showNotCars')->name('showUserNotCars');
Route::get('ajaxUpdateU/{userId?}', 'UserController@getToEdit');
Route::put('ajaxUpdateU/{userId?}', 'UserController@update');
//Model -> Car
Route::get('car/addform', 'CarController@addform')->name('addFormCar');
Route::post('car/add', 'CarController@add')->name('addCar');
Route::get('car/showone/{id}', 'CarController@showone')->name('showCar');
Route::post('car/update/{id}', 'CarController@update')->name('updateCar');
Route::get('car/remove/{id}', 'CarController@remove')->name('removeCar');
Route::post('car/photo/add/{id}', 'CarController@store')->name('addPhotoCar');
Route::post('car/photo/update/{id}', 'CarController@updatePhoto')->name('updatePhotoCar');
//Model -> Mileage
Route::get('mileage/show/car/{id}', 'MileageController@show')->name('showMileage');
Route::get('/fetch_dataMileage/{carId?}', 'MileageController@fetch_data');
Route::post('mileage/add/{id}', 'MileageController@add')->name('addMileage');
Route::patch('mileage/update/{id}', 'MileageController@update')->name('updateMileage');
Route::delete('mileage/remove', 'MileageController@remove')->name('removeMileage');
//Model -> Fuel
Route::get('fuel/show/car/{id}', 'FuelController@show')->name('showFuel');
Route::post('fuel/add/{id}', 'FuelController@add')->name('addFuel');
Route::patch('fuel/update/{id}', 'FuelController@update')->name('updateFuel');
Route::delete('fuel/remove/{id}', 'FuelController@remove')->name('removeFuel');
Route::post('ajaxCalculateFS/{carId?}', 'FuelController@calculateOne');
Route::post('ajaxCalculateMU/{carId?}', 'FuelController@calculateTwo');
Route::get('fuel/ajaxChartC/{carId?}', 'FuelController@getToShowC');
Route::get('fuel/ajaxChartC1/{carId?}', 'FuelController@createChartC');
Route::get('fuel/ajaxChartU/{carId?}', 'FuelController@getToShowU');
Route::get('fuel/ajaxChartU1/{carId?}', 'FuelController@createChartU');
//Model -> Insurance
Route::get('insurance/show/car/{id}', 'InsuranceController@show')->name('showInsurance');
Route::post('insurance/add/{id}', 'InsuranceController@add')->name('addInsurance');
Route::get('ajaxUpdateInsurance/{insuranceId?}', 'InsuranceController@getToEdit');
Route::put('ajaxUpdateInsurance/{insuranceId?}', 'InsuranceController@update');
Route::get('ajaxShowNotificationsInInsurance/{carId?}', 'InsuranceController@showNotifications');
//Model -> Notification
Route::post('notification/add/{id}', 'NotificationController@add')->name('addNotification');
Route::get('ajaxUpdateNotification/{notificationId?}', 'NotificationController@getToEdit');
Route::put('ajaxUpdateNotification/{notificationId?}', 'NotificationController@update');
Route::get('ajaxShowNotification/{notificationId?}', 'NotificationController@show');
Route::get('ajaxRemoveN/{notificationId?}', 'NotificationController@getToRemove');
Route::delete('ajaxRemoveN/{notificationId?}', 'NotificationController@removeNotification');
Route::get('ajaxShowNotificationsAllRead', 'NotificationController@showAllRead');
Route::delete('ajaxRemoveAllReadNotifications', 'NotificationController@removeAllRead');
Route::get('ajaxNotificationMarkAsRead/{notificationIdAsRead?}', 'NotificationController@markAsRead');
Route::post('notification/sendEmail/{id}', 'NotificationController@send')->name('sendNotification');
//Model -> Tire
Route::get('tire/addform/car/{id}', 'TireController@addform')->name('addFormTire');
Route::get('tire/show/{tires}/car/{car_oneId}', 'TireController@showone')->name('showTire');
Route::post('tire/add/{id}', 'TireController@add')->name('addTire');
Route::post('tire/update/{tire_one}/car/{car_one}', 'TireController@update')->name('updateTire');
Route::delete('tire/remove', 'TireController@remove')->name('removeTire');
Route::get('tire/season/show/{tire_oneId}/car/{car_oneId}', 'TireController@showSeason')->name('showSeasonTires');
Route::get('tire/name/show/{tire_oneId}/car/{car_oneId}', 'TireController@showName')->name('showNameTires');
//Model -> Tread_condition
Route::get('tread_condition/LP/show/{tire_one}/car/{car_oneId}', 'TireController@showLP')->name('showTreadConditionLP');
Route::post('tread_condition/LP/add/{id}', 'TireController@addTC')->name('addTreadCondition');
Route::patch('tread_condition/update/{id}', 'TireController@updateTC')->name('updateTreadCondition');
Route::delete('tread_condition/remove', 'TireController@removeTC')->name('removeTreadCondition');
Route::get('tread_condition/LT/show/{tire_one}/car/{car_oneId}', 'TireController@showLT')->name('showTreadConditionLT');
Route::get('tread_condition/PP/show/{tire_one}/car/{car_oneId}', 'TireController@showPP')->name('showTreadConditionPP');
Route::get('tread_condition/PT/show/{tire_one}/car/{car_oneId}', 'TireController@showPT')->name('showTreadConditionPT');
//Model -> Service
Route::get('service/show/car/{id}/name/{car_oneMake}/{car_oneModel}', 'ServiceController@show')->name('showService');
Route::post('service/add/car/{id}', 'ServiceController@add')->name('addService');
Route::get('ajaxUpdate/{serviceId?}', 'ServiceController@getToEdit');
Route::put('ajaxUpdate/{serviceId?}', 'ServiceController@updateService');
Route::get('ajaxRemove/{serviceId?}', 'ServiceController@getToRemove');
Route::delete('ajaxRemove/{serviceId?}/{car_id?}', 'ServiceController@removeService');
Route::post('serviceL/update/{serviceId}/car/{id}', 'ServiceController@updateL')->name('updateServiceL');
Route::get('serviceL/remove/{id}', 'ServiceController@removeServiceL')->name('removeServiceL');
//Model -> Service_plan
Route::get('service_plan/show/car/{id}/name/{car_oneMake}/{car_oneModel}', 'ServiceController@showPlan')->name('showServicePlan');
Route::post('service_plan/car/{id}', 'ServiceController@addServicePlan')->name('addServicePlan');
Route::patch('service_plan/update', 'ServiceController@updateServicePlan')->name('updateServicePlan');
Route::delete('service_plan/delete', 'ServiceController@removeServicePlan')->name('removeServicePlan');
Route::get('ajaxShowNotificationsInService/{carId?}', 'ServiceController@showNotifications');
//Model -> Cost
Route::get('costs/show/car/{id}', 'CostController@show')->name('showCosts');
Route::get('/fetch_data/{carId?}', 'CostController@fetch_data');
Route::post('costs/add/car/{id}', 'CostController@add')->name('addCost');
Route::get('ajaxUpdateC/{costId?}', 'CostController@getToEdit');
Route::put('ajaxUpdateC/{costId?}', 'CostController@update');
Route::get('ajaxRemoveC/{costId?}', 'CostController@getToRemove');
Route::delete('ajaxRemoveC/{costId?}', 'CostController@removeCost');
Route::post('costsL/update/{costlastId}/car/{id}', 'CostController@updateL')->name('updateCostL');
Route::get('costsL/remove/{id}', 'CostController@removeCostL')->name('removeCostL');
Route::post('ajaxCalculate/{carId}', 'CostController@calculateTime');
Route::post('ajaxShowCosts/{carId?}', 'CostController@showCostsTime');
Route::post('ajaxShowCostsType/{carId?}', 'CostController@showCostType');
Route::post('costs/showTime/car/{id}', 'CostController@showTime')->name('showCostTime');
//TechnicalInspection
Route::get('technicalInspection/show/car/{id}', 'TechnicalInspectionController@show')->name('showTechnicalInspection');
Route::get('ajaxShowNotificationsTechnicalInspection/{carId?}', 'TechnicalInspectionController@showNotifications');

Auth::routes();
