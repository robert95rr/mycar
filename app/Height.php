<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Height extends Model
{
  protected $fillable = [
    'value'
  ];

  protected $table = 'height';
}
