<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
      'car_id',
      'service_plan_id',
      'date_service',
      'mileage',
      'next_service_mileage',
      'description'
    ];

    protected $table = 'service';
}
