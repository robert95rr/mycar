<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Width extends Model
{
    protected $fillable = [
      'value'
    ];

    protected $table = 'width';
}
