<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fuel extends Model
{
  protected $fillable = [
    'car_id',
    'amount_before',
    'amount_refueled',
    'refueling_price',
    'mileage',
    'date',
    'place'
  ];

  protected $table = 'fuel';
}
