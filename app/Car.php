<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public function vehicle_type()
    {
      return $this->belongsTo(App/Vehicle_type);
    }

    protected $fillable = [
      'user_id',
      'vehicle_type_id',
      'make',
      'model',
      'year_production',
      'engine_size',
      'engine_power',
      'fuel_type',
      'registration_number',
      'vin_number',
      'interior_colour'
    ];
}
