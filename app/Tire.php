<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tire extends Model
{
  protected $fillable = [
    'car_id',
    'type_id',
    'width_id',
    'height_id',
    'diameter_id',
    'manufacturer',
    'model',
    'dot',
    'date_put'
  ];

  
}
