<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer('*', function($view)
        {
          if(Auth::check()){
          $user = User::find(Auth::id());
          $notifications = $user->notifications()->get();
          $dateToday = date('Y-m-d');
          $idNotifications = [];
          foreach($notifications as $notification)//Pobieranie powiadomień, które mają być wyświetlone dla użytkownika
          {
            $dateEvent = $notification->data[0]['date_event'];
            $reminder = $notification->data[0]['reminder'];
            $day = substr($dateEvent, 8, 2);
            $month = substr($dateEvent, 5, 2);
            $year = substr($dateEvent, 0, 4);
            $dateReminder = '';

            switch($reminder)
            {
              case 1:
                    if($day > 1)
                    {
                      $dayReminder = $day - $reminder;
                      if($dayReminder < 10)
                      {
                        $dayReminder0 = '0'.$dayReminder;
                        $dateReminder = str_replace($day, $dayReminder0, $dateEvent);
                      }
                      else
                      $dateReminder = substr_replace($dateEvent, $dayReminder, 8);
                    }
                    else
                    {
                      if($month == 1)
                      {
                        $dayReminder = 31;
                        $monthReminder = 12;
                        $yearReminder = $year - 1;
                        $dateReminder = str_replace([$year, $month, $day], [$yearReminder, $monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 2 || $month == 4 || $month == 6 || $month == 8 || $month == 9 || $month == 11)
                      {
                        $dayReminder = 31;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 5 || $month == 7 || $month == 10 || $month == 12)
                      {
                        $dayReminder = 30;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 3 && $year % 4 == 0 && $year % 100 != 0 || $year % 400 == 0)
                      {
                        $dayReminder = 29;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      else
                      {
                        $dayReminder = 28;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                    }
                    break;
              case 2:
                    if($day > 2)
                    {
                      $dayReminder = $day - $reminder;
                      if($dayReminder < 10)
                      {
                        $dayReminder0 = '0'.$dayReminder;
                        $dateReminder = str_replace($day, $dayReminder0, $dateEvent);
                      }
                      else
                      $dateReminder = str_replace($day, $dayReminder, $dateEvent);
                    }
                    elseif($day == 2)
                    {
                      if($month == 1)
                      {
                        $dayReminder = 31;
                        $monthReminder = 12;
                        $yearReminder = $year - 1;
                        $dateReminder = str_replace([$year, $month, $day], [$yearReminder, $monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 2 || $month == 4 || $month == 6 || $month == 8 || $month == 9 || $month == 11)
                      {
                        $dayReminder = 31;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 5 || $month == 7 || $month == 10 || $month == 12)
                      {
                        $dayReminder = 30;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 3 && $year % 4 == 0 && $year % 100 != 0 || $year % 400 == 0)
                      {
                        $dayReminder = 29;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      else
                      {
                        $dayReminder = 28;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                    }
                    else//day == 1
                    {
                      if($month == 1)
                      {
                        $dayReminder = 30;
                        $monthReminder = 12;
                        $yearReminder = $year - 1;
                        $dateReminder = str_replace([$year, $month, $day], [$yearReminder, $monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 2 || $month == 4 || $month == 6 || $month == 8 || $month == 9 || $month == 11)
                      {
                        $dayReminder = 30;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 5 || $month == 7 || $month == 10 || $month == 12)
                      {
                        $dayReminder = 29;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 3 && $year % 4 == 0 && $year % 100 != 0 || $year % 400 == 0)
                      {
                        $dayReminder = 28;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      else
                      {
                        $dayReminder = 27;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                    }
                    break;
              case 3:
                    if($day > 3)
                    {
                      $dayReminder = $day - $reminder;
                      if($dayReminder < 10)
                      {
                        $dayReminder0 = '0'.$dayReminder;
                        $dateReminder = str_replace($day, $dayReminder0, $dateEvent);
                      }
                      else
                      $dateReminder = str_replace($day, $dayReminder, $dateEvent);
                    }
                    elseif($day == 3)
                    {
                      if($month == 1)
                      {
                        $dayReminder = 31;
                        $monthReminder = 12;
                        $yearReminder = $year - 1;
                        $dateReminder = str_replace([$year, $month, $day], [$yearReminder, $monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 2 || $month == 4 || $month == 6 || $month == 8 || $month == 9 || $month == 11)
                      {
                        $dayReminder = 31;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 5 || $month == 7 || $month == 10 || $month == 12)
                      {
                        $dayReminder = 30;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 3 && $year % 4 == 0 && $year % 100 != 0 || $year % 400 == 0)
                      {
                        $dayReminder = 29;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      else
                      {
                        $dayReminder = 28;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                    }
                    elseif($day == 2)
                    {
                      if($month == 1)
                      {
                        $dayReminder = 30;
                        $monthReminder = 12;
                        $yearReminder = $year - 1;
                        $dateReminder = str_replace([$year, $month, $day], [$yearReminder, $monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 2 || $month == 4 || $month == 6 || $month == 8 || $month == 9 || $month == 11)
                      {
                        $dayReminder = 30;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 5 || $month == 7 || $month == 10 || $month == 12)
                      {
                        $dayReminder = 29;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 3 && $year % 4 == 0 && $year % 100 != 0 || $year % 400 == 0)
                      {
                        $dayReminder = 28;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      else
                      {
                        $dayReminder = 27;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                    }
                    else//day == 1
                    {
                      if($month == 1)
                      {
                        $dayReminder = 29;
                        $monthReminder = 12;
                        $yearReminder = $year - 1;
                        $dateReminder = str_replace([$year, $month, $day], [$yearReminder, $monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 2 || $month == 4 || $month == 6 || $month == 8 || $month == 9 || $month == 11)
                      {
                        $dayReminder = 29;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 5 || $month == 7 || $month == 10 || $month == 12)
                      {
                        $dayReminder = 28;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      elseif($month == 3 && $year % 4 == 0 && $year % 100 != 0 || $year % 400 == 0)
                      {
                        $dayReminder = 27;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                      else
                      {
                        $dayReminder = 26;
                        $monthReminder = $month - 1;
                        $dateReminder = str_replace([$month, $day], [$monthReminder, $dayReminder], $dateEvent);
                      }
                    }
                    break;
            }

            if($dateToday == $dateReminder || ($dateToday <= $dateEvent && $dateToday > $dateReminder))
            {
              $notificationSelectId = $notification->id;
              array_push($idNotifications, $notificationSelectId);
            }
          }//----------------------------------------Koniec pętli--------------------------------------------------
          $results = $user->unreadNotifications()->whereIn('id', $idNotifications)->get();
          $readNotifications = $user->notifications()->whereNotNull('read_at')->get();
          $view->with(compact(['results', 'readNotifications']));
        }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
