<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tread_condition extends Model
{
  protected $fillable = [
    'tires_id',
    'start_tread',
    'end_tread',
    'start_date',
    'end_date',
    'start_mileage',
    'end_mileage',
    'diff_mileage',
    'tire_position'
  ];

  protected $table = 'tread_condition';
}
