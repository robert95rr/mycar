<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
  protected $fillable = [
    'car_id_cost',
    'service_id',
    'cost_type',
    'price',
    'place',
    'date',
    'description'
  ];
}
