<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle_type extends Model
{
    public function cars()
    {
      return $this->hasMany(App/Car);
    }

    protected $fillable = [
      'name'
    ];
}
