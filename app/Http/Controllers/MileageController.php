<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Mileage;
use App\Car;
use App\Tire;

class MileageController extends Controller
{
    public function show($id)
    {
      $mileage = Mileage::where('car_id', $id)->orderBy('date', 'desc')->paginate(3);
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($id);
      $tire = Tire::where('car_id', $id)->get();
      return view('mileage/show', ['mileage' => $mileage, 'car' => $car, 'car_one' => $car_one, 'tire' => $tire]);
    }

    public function fetch_data(Request $request, $carId)
    {
      if($request->ajax())
      {
        $mileage = Mileage::where('car_id', $carId)->orderBy('date', 'desc')->paginate(3);
        return view('mileage/pagination_list', compact('mileage'))->render();
      }
    }
    public function add(Request $request, $id)
    {
      $this->validate($request, [
           'value' => 'required|string|max:7',
           'date' => 'required|date',
           'description' => 'required|string|max:255',
       ]);

       $mileage_id = Mileage::where('car_id', $id)->where('id', '>', 0 )->first();
       if($mileage_id === NULL)
       {
         $mileage = new Mileage();
         $mileage->car_id = $id;
         $mileage->value = $request['value'];
         $mileage->date =  $request['date'];
         $mileage->description = $request['description'];
         $mileage->save();

         return redirect()->route('showMileage', [$id]);
       }

       $mileage_select = Mileage::where('car_id', $id)->orderBy('id', 'desc')->first()->value;

       $mileage_v = $request['value'];
       if($mileage_v <= $mileage_select)
       {
         $mileage = Mileage::where('car_id', $id)->orderBy('date', 'desc')->paginate(3);
         $car = Car::where('user_id', Auth::id())->get();
         $car_one = Car::findOrFail($id);
         $tire = Tire::where('car_id', $id)->get();
         return view('mileage/show', ['mileage' => $mileage, 'car' => $car, 'car_one' => $car_one, 'tire' => $tire, 'news' => 'Pole - wartość, powinno być większe niż ostatni zapisany przebieg!']);
       }

       $date_select = Mileage::where('car_id', $id)->orderBy('id', 'desc')->first()->date;

       $mileage_d = $request['date'];
       if($mileage_d <= $date_select)
       {
         $mileage = Mileage::where('car_id', $id)->orderBy('date', 'desc')->paginate(3);
         $car = Car::where('user_id', Auth::id())->get();
         $car_one = Car::findOrFail($id);
         $tire = Tire::where('car_id', $id)->get();
         return view('mileage/show', ['mileage' => $mileage, 'car' => $car, 'car_one' => $car_one, 'tire' => $tire, 'news' => 'Pole - data, powinno być późniejsze niż ostatni zapisany przebieg!']);
       }

       $mileage = new Mileage();
       $mileage->car_id = $id;
       $mileage->value = $mileage_v;
       $mileage->date =  $mileage_d;
       $mileage->description = $request['description'];
       $mileage->save();

       return redirect()->route('showMileage', [$id]);
    }

    public function remove(Request $request)
    {
      $mileage = Mileage::findOrFail($request->mileage_id);
      $mileage->delete();

       return back();
    }

    public function update(Request $request, $id)
    {
      $mileage_id = $request->mileage_id;
      $value_main = Mileage::where('id', $mileage_id)->value('value');//wartosc wybrana do edycji
      $value_first = Mileage::where('car_id', $id)->orderBy('id', 'asc')->first()->value;//pierwsza wartosc dla wybranego auta
      if($value_first == $value_main)
      {
        $value_lower = 0;
      }
      else
      {
        $value_lower = Mileage::where('car_id', $id)->orderBy('id', 'desc')->where('value', '<', $value_main)->first()->value;//wartosc pierwsza mniejsza od wybranej
      }

      $value_last = Mileage::where('car_id', $id)->orderBy('id', 'desc')->first()->value;//ostatnia wartosc dla wybranego auta
      if($value_last == $value_main)
      {
        $value_higher = 9999999;
      }
      else
      {
        $value_higher = Mileage::where('car_id', $id)->orderBy('id', 'asc')->where('value', '>', $value_main)->first()->value;//wartosc pierwsza wieksza od wybranej
      }
      $mileage_value = $request->value;//do aktualizacji
      if($mileage_value <= $value_lower || $mileage_value >= $value_higher)
      {
        $mileage = Mileage::where('car_id', $id)->get();
        $car = Car::where('user_id', Auth::id())->get();
        $car_one = Car::findOrFail($id);
        return view('mileage/show', ['mileage' => $mileage, 'car' => $car, 'car_one' => $car_one, 'news' => 'Pole - wartość, powinno znajdować się między ' .$value_lower. ' a ' .$value_higher. ' !']);
      }
      //--------------------------------------------------------------------------------------------------------------------------------------------------------
      $date_main = Mileage::where('id', $mileage_id)->value('date');//data wybrana do edycji
      $date_first = Mileage::where('car_id', $id)->orderBy('id', 'asc')->first()->date;//pierwsza data dla wybranego auta
      if($date_first ==  $date_main)
      {
        $date_lower = '2000-01-01';
      }
      else
      {
        $date_lower = Mileage::where('car_id', $id)->orderBy('id', 'desc')->where('date', '<', $date_main)->first()->date;//data pierwsza mniejsza od wybranej
      }

      $date_last = Mileage::where('car_id', $id)->orderBy('id', 'desc')->first()->date;//ostatnia data dla wybranego auta
      if($date_last == $date_main)
      {
        $date_higher = '2118-12-31';
      }
      else
      {
        $date_higher = Mileage::where('car_id', $id)->orderBy('id', 'asc')->where('date', '>', $date_main)->first()->date;//data pierwsza wieksza od wybranej
      }
      $mileage_date = $request->date;//do aktualizacji
      if($mileage_date <= $date_lower || $mileage_date >= $date_higher)
      {
        $mileage = Mileage::where('car_id', $id)->get();
        $car = Car::where('user_id', Auth::id())->get();
        $car_one = Car::findOrFail($id);
        return view('mileage/show', ['mileage' => $mileage, 'car' => $car, 'car_one' => $car_one, 'news' => 'Pole - data, powinno znajdować się między ' .$date_lower. ' a ' .$date_higher. ' !']);
      }
      //----------------------------------------------------------------------------------------------------------------------------------------------------------

      $mileage = Mileage::findOrFail($request->mileage_id);
      $mileage->update($request->all());

      return back();
    }
}
