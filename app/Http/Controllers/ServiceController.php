<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\CarReminder;
use Illuminate\Support\Facades\DB;
use App\Car;
use App\Tire;
use App\Service;
use App\Service_plan;
use App\Cost;
use App\User;

class ServiceController extends Controller
{
    public function show(Request $request, $id, $car_oneMake, $car_oneModel)
    {
      $serviceCheck = Service::where('car_id', $id)->first();
      if($serviceCheck === NULL)
      {
        $car = Car::where('user_id', Auth::id())->get();
        $car_one = Car::findOrFail($id);
        $tire = Tire::where('car_id', $id)->get();
        $nameCar = $car_oneMake.' '.$car_oneModel;
        $servicePlans = Service_plan::where('car_model', $nameCar)->get();
        return view('service/show', ['car' => $car, 'car_one' => $car_one, 'tire' => $tire, 'servicePlans' => $servicePlans]);
      }
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($id);
      $tire = Tire::where('car_id', $id)->get();
      $nameCar = $car_oneMake.' '.$car_oneModel;
      $servicePlans = Service_plan::where('car_model', $nameCar)->get();
      $service = Service::where('car_id', $id)->orderBy('id', 'desc')->first();//ostatni serwis
      $idSP = Service::where('car_id', $id)->orderBy('id', 'desc')->value('service_plan_id');
      $service_task = Service_plan::where('id', $idSP)->value('service_task');
      if($service_task === NULL)
      {
        $service_task = 'Brak planu serwisowego dla tej czynności';
      }
      $last_id = Service::where('car_id', $id)->orderBy('id', 'desc')->first()->id;
      $serviceHistory = Service::leftJoin('service_plan', 'service.service_plan_id', '=', 'service_plan.id')->select('service.*', 'service_plan.service_task')->where('service.id', '<', $last_id)->orderBy('service.id', 'desc')->get();
      return view('service/show', ['car' => $car, 'car_one' => $car_one, 'tire' => $tire, 'servicePlans' => $servicePlans, 'service' => $service, 'service_task' => $service_task, 'serviceHistory' => $serviceHistory]);
    }

    public function add(Request $request, $id)
    {
      $this->validate($request, [
        'service_plan_id' => 'required|string',
        'date_service' => 'required|date',
        'mileage' => 'required|string|max:7',
        'next_service_mileage' => 'max:7',
        'description' => 'required|string|max:255',
      ]);

      $key = $request['service_plan_id'];
      $mil_next = $request['next_service_mileage'];
      if($key == 'NULL' || $mil_next === NULL)
      {
        $service = new Service();
        $service->car_id = $id;
        $service->date_service = $request['date_service'];
        $service->mileage = $request['mileage'];
        $service->next_service_mileage = 0;
        $service->description = $request['description'];
        $service->save();

        return back();
      }

      $service = new Service();
      $service->car_id = $id;
      $service->service_plan_id = $key;
      $service->date_service = $request['date_service'];
      $service->mileage = $request['mileage'];
      $service->next_service_mileage = $request['next_service_mileage'];
      $service->description = $request['description'];
      $service->save();

      return back();
    }

    public function getToEdit($serviceId)
    {
      $service = Service::findOrFail($serviceId);
      $id = Service::where('id', $serviceId)->value('service_plan_id');
      $servicetask = Service_plan::where('id', $id)->value('service_task');

      return response()->json([$service, $servicetask]);
    }

    public function updateService(Request $request, $serviceId)
    {
      $service = Service::findOrFail($serviceId);
      $service->car_id = $request['car_id'];
      $service->date_service = $request['date_service'];
      $service->mileage = $request['mileage'];
      $service->next_service_mileage = $request['next_service_mileage'];
      $service->description = $request['description'];
      $service->update($request->all());

      return response()->json($service);
    }

    public function getToRemove($serviceId)
    {
      $service = Service::findOrFail($serviceId);
      return response()->json($service);
    }

    public function removeService($serviceId, $car_id)
    {
      $serviceInCost = Cost::where('car_id', $car_id)->where('service_id', $serviceId)->get();
      $service = Service::findOrFail($serviceId);
      $service->delete();

      return response()->json($service);
    }

    public function updateL(Request $request, $serviceId, $id)
    {
      $service = Service::findOrFail($serviceId);
      $service->car_id = $id;
      $service->service_plan_id = $request['service_plan_id'];
      $service->date_service = $request['date_service'];
      $service->mileage = $request['mileage'];
      $service->next_service_mileage = $request['next_service_mileage'];
      $service->description = $request['description'];
      $service->update($request->all());

      return back();
    }

    public function removeServiceL($id)
    {
      $service = Service::findOrFail($id);
      $service->delete();

      return back();
    }

    public function showPlan(Request $request, $id, $car_oneMake, $car_oneModel)
    {
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($id);
      $tire = Tire::where('car_id', $id)->get();
      $nameCar = $car_oneMake.' '.$car_oneModel;
      $servicePlan = Service_plan::where('car_model', $nameCar)->get();
      return view('service/showplan', ['car' => $car, 'car_one' => $car_one, 'tire' => $tire, 'servicePlan' => $servicePlan]);
    }

    public function addServicePlan(Request $request, $id)
    {
      $this->validate($request, [
        'car_model' => 'required|string|max:20',
        'service_task' => 'required|string|max:30',
        'interval' => 'required|string|max:6',
        'description' => 'required|string|max:255',
      ]);

      $service_plan = new Service_plan();
      $service_plan->car_model = $request['car_model'];
      $service_plan->service_task = $request['service_task'];
      $service_plan->interval = $request['interval'];
      $service_plan->description = $request['description'];
      $service_plan->save();

      return back();
    }

    public function updateServicePlan(Request $request)
    {
      $service_plan = Service_plan::findOrFail($request->service_plan_id);
      $service_plan->car_model = $request['car_model'];
      $service_plan->service_task = $request['service_task'];
      $service_plan->interval = $request['interval'];
      $service_plan->description = $request['description'];
      $service_plan->update($request->all());

      return back();
    }

    public function removeServicePlan(Request $request)
    {
      $service_plan = Service_plan::findOrFail($request->service_plan_id);
      $service_plan->delete();

      return back();
    }

    public function showNotifications($carId)
    {
      $user = User::find(Auth::id());
      $result = $user->unreadNotifications()->where([
        ['data', 'LIKE', '%"car_id":"'.$carId.'"%'],
        ['data', 'LIKE', '%"category":"Serwis"%'],
      ])->get();
      return response()->json($result);
    }
}
