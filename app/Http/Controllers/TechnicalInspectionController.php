<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\CarReminder;
use App\Car;
use App\Tire;
use App\User;

class TechnicalInspectionController extends Controller
{
    public function show($id)
    {
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($id);
      $tire = Tire::where('car_id', $id)->get();
      return view('technicalInspection/show', ['car' => $car, 'car_one' => $car_one, 'tire' => $tire]);
    }

    public function showNotifications($carId)
    {
      $user = User::find(Auth::id());
      $result = $user->unreadNotifications()->where([
        ['data', 'LIKE', '%"car_id":"'.$carId.'"%'],
        ['data', 'LIKE', '%"category":"Przeglad techniczny"%'],
      ])->get();
      return response()->json($result);
    }
}
