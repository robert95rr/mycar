<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Car;
use App\Tire;
use App\Service;
use App\Cost;

class CostController extends Controller
{
    public function show(Request $request, $id)
    {
      $costsSelectedCar = Cost::leftJoin('service', 'costs.service_id', '=', 'service.id')->select('costs.*', 'service.car_id')->where('car_id_cost', $id)->orWhere('car_id', $id)->get();//złaczenie tabel w celu użyskania wszystkich danych na temat danego auta
      $allCt = $costsSelectedCar->pluck('cost_type')->toArray();//pobranie wszystkich typów kosztu wybranego auta i konwersja kolekcji na tablice
      $ct = array_unique($allCt);//usunięcie zduplikowanych wartości
      $freshCt = array_values($ct);//uporządkowanie kluczy wartości w tablicy
      $count = count($freshCt);//policzenie ilości elementów w tablicy
      $tab1 = [];
      for($i=0; $i < $count;$i++)
      {
        $sel = $freshCt[$i];
        $search = $costsSelectedCar->where('cost_type', $sel)->sum('price');
        array_push($tab1, $search);
      }
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($id);
      $tire = Tire::where('car_id', $id)->get();
      $costsJoinWithService = Cost::leftJoin('service', 'costs.service_id', '=', 'service.id');
      $allCostsWithService = $costsJoinWithService->select('costs.service_id')->whereNotNull('service_id')->where('car_id_cost', $id)->orWhere('car_id', $id);//pobranie id serwisów, które zostały zapisane jako koszt
      $serviceNotToCosts = Service::where('car_id', $id)->whereNotIn('id', $allCostsWithService)->get();//pobranie serwisów, które nie zostały jeszcze zapisane jako koszt
      $services = Service::where('car_id', $id)->get();
      $costsSelectedCar1 = Cost::leftJoin('service', 'costs.service_id', '=', 'service.id')->select('costs.*', 'service.car_id')->where('car_id_cost', $id)->orWhere('car_id', $id);
      $cost_last = $costsSelectedCar1->latest()->first();
      $findCostType = $ct;
      if($cost_last == NULL)
      {
        $costs = $costsSelectedCar1->orderBy('id', 'desc')->get();
        return view('costs/show', ['car' => $car, 'car_one' => $car_one, 'tire' => $tire, 'serviceNotToCosts' => $serviceNotToCosts, 'services' => $services, 'costs' => $costs, 'findCostType' => $findCostType, 'freshCt' => $freshCt, 'tab1' => $tab1]);
      }
      $cost_last_id = $costsSelectedCar1->latest()->first()->id;
      $costs = Cost::leftJoin('service', 'costs.service_id', '=', 'service.id')->select('costs.*', 'service.car_id')
      ->where('costs.id', '<', $cost_last_id)->where('car_id_cost', $id)->orWhere('car_id', $id)->where('costs.id', '<', $cost_last_id)
      ->orderBy('costs.id', 'desc')->paginate(3);
      $service_to_cost = Service::where('car_id', $id)->get();
      return view('costs/show', ['car' => $car, 'car_one' => $car_one, 'tire' => $tire, 'serviceNotToCosts' => $serviceNotToCosts, 'services' => $services, 'cost_last' => $cost_last, 'costs' => $costs, 'findCostType' => $findCostType, 'service_to_cost' => $service_to_cost, 'freshCt' => $freshCt, 'tab1' => $tab1]);
    }

    public function fetch_data(Request $request, $carId)
    {
      if($request->ajax())
      {
        $costsSelectedCar =Cost::leftJoin('service', 'costs.service_id', '=', 'service.id')->select('costs.*', 'service.car_id')->where('car_id_cost', $carId)->orWhere('car_id', $carId);
        $service_to_cost = Service::where('car_id', $carId)->get();
        $cost_last_id = $costsSelectedCar->latest()->first()->id;
        $costs = Cost::leftJoin('service', 'costs.service_id', '=', 'service.id')->select('costs.*', 'service.car_id')
        ->where('costs.id', '<', $cost_last_id)->where('car_id_cost', $carId)->orWhere('car_id', $carId)->where('costs.id', '<', $cost_last_id)
        ->orderBy('costs.id', 'desc')->paginate(3);
        return view('costs/pagination_list', ['service_to_cost' => $service_to_cost], compact('costs'))->render();
      }
    }

    public function add(Request $request, $id)
    {
      $this->validate($request, [
        'service_id' => 'string',
        'cost_type' => 'required|string|max:50',
        'date' => 'required|date',
        'price' => 'required|string|max:8',
        'place' => 'required|string|max:100',
        'description' => 'required|string|max:255',
      ]);

      $service_id = $request['service_id'];
      if($service_id == 'NULL')
      {
        $cost = new Cost();
        $cost->car_id_cost = $id;
        $cost->cost_type = $request['cost_type'];
        $cost->price = $request['price'];
        $cost->place = $request['place'];
        $cost->date = $request['date'];
        $cost->description = $request['description'];
        $cost->save();

        return back();
      }

      $cost = new Cost();
      $cost->service_id = $service_id;
      $cost->cost_type = $request['cost_type'];
      $cost->price = $request['price'];
      $cost->place = $request['place'];
      $cost->date = $request['date'];
      $cost->description = $request['description'];
      $cost->save();

      return back();
    }

    public function getToEdit($costId)
    {
      $cost = Cost::findOrFail($costId);
      return response()->json($cost);
    }

    public function update(Request $request, $costId)
    {
      $cost = Cost::findOrFail($costId);
      $cost->service_id = $request['service_id'];
      $cost->cost_type = $request['cost_type'];
      $cost->date = $request['date'];
      $cost->price = $request['price'];
      $cost->place = $request['place'];
      $cost->description = $request['description'];
      $cost->update($request->all());

      return response()->json($cost);
    }

    public function updateL(Request $request, $costlastId, $id)
    {
      $service_id = $request['service_id'];
      if($service_id == 'NULL')
      {
        $costL = Cost::findOrFail($costlastId);
        $costL->car_id_cost = $id;
        $costL->cost_type = $request['cost_type'];
        $costL->date = $request['date'];
        $costL->price = $request['price'];
        $costL->place = $request['place'];
        $costL->description = $request['description'];
        $costL->update();

        return back();
      }

      $costL = Cost::findOrFail($costlastId);
      $costL->car_id_cost = $id;
      $costL->service_id = $service_id;
      $costL->cost_type = $request['cost_type'];
      $costL->date = $request['date'];
      $costL->price = $request['price'];
      $costL->place = $request['place'];
      $costL->description = $request['description'];
      $costL->update($request->all());

      return back();
    }

    public function removeCostL($id)
    {
      $costL = Cost::findOrFail($id);
      $costL->delete();

      return back();
    }

    public function getToRemove($costId)
    {
      $cost = Cost::findOrFail($costId);
      return response()->json($cost);
    }

    public function removeCost($costId)
    {
      $cost = Cost::findOrFail($costId);
      $cost->delete();

      return response()->json($cost);
    }

    public function calculateTime(Request $request, $carId)
    {
      $this->validate($request, [
        'date2' => 'required|date',
        'date3' => 'required|date',
      ]);

      $date2 = $request['date2'];
      $date3 = $request['date3'];
      $cost_calculateT = Cost::leftJoin('service', 'costs.service_id', '=', 'service.id')->select('costs.*', 'service.car_id')
      ->whereBetween('date', [$date2, $date3])->where('car_id_cost', $carId)->orWhere('car_id', $carId)
      ->whereBetween('date', [$date2, $date3])->sum('price');

      return response()->json([$cost_calculateT, $date2, $date3]);
    }

    public function showCostsTime(Request $request, $carId)
    {
      $this->validate($request, [
        'date_2' => 'required|date',
        'date_3' => 'required|date',
      ]);

      $date_2 = $request['date_2'];
      $date_3 = $request['date_3'];
      $costs = Cost::leftJoin('service', 'costs.service_id', '=', 'service.id')->select('costs.*', 'service.car_id')
      ->whereBetween('date', [$date_2, $date_3])->where('car_id_cost', $carId)->orWhere('car_id', $carId)
      ->whereBetween('date', [$date_2, $date_3])->orderBy('created_at', 'desc')->get();

      return response()->json([$costs, $date_2, $date_3]);
    }

    public function showCostType(Request $request, $carId)
    {
      $costType = $request['selectType'];
      $findCosts = Cost::leftJoin('service', 'costs.service_id', '=', 'service.id')->select('costs.*', 'service.car_id')
      ->where('cost_type', $costType)->where('car_id_cost', $carId)->orWhere('car_id', $carId)
      ->where('cost_type', $costType)->orderBy('created_at', 'desc')->get();

      return response()->json([$findCosts, $costType]);
    }

  }
