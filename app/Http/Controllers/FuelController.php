<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Fuel;
use App\Car;
use App\Tire;

class FuelController extends Controller
{
    public function show($id)
    {
      $fuel = Fuel::where('car_id', $id)->get();
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($id);
      $tire = Tire::where('car_id', $id)->get();
      return view('fuel/show', ['fuel' => $fuel, 'car' => $car, 'car_one' => $car_one, 'tire' => $tire]);
    }

    public function add(Request $request, $id)
    {
      $this->validate($request, [
        'amount_before' => 'required|string|max:2',
        'amount_refueled' => 'required|string|max:2',
        'refueling_price' => 'required|string|max:4',
        'mileage' => 'required|string|max:7',
        'date' => 'required|date',
        'place' => 'required|string|max:255',
      ]);

      $fuel_id = Fuel::where('car_id', $id)->where('id', '>', 0)->first();//Sprawdzenie w bazie danych, czy istnieje jakikolwiek rekord w tabeli - Fuel
      if($fuel_id === NULL)
      {
        $fuel_cost_ar = $request['amount_refueled'];
        $fuel_cost_rp = $request['refueling_price'];
        $fuel_cost = $fuel_cost_ar * $fuel_cost_rp;

        $fuel = new Fuel();
        $fuel->car_id = $id;
        $fuel->amount_before = $request['amount_before'];
        $fuel->amount_refueled = $fuel_cost_ar;
        $fuel->refueling_price = $fuel_cost_rp;
        $fuel->mileage = $request['mileage'];
        $fuel->date = $request['date'];
        $fuel->total_cost = $fuel_cost;
        $fuel->fuel_usage = 0;
        $fuel->mil_driven = 0;
        $fuel->place = $request['place'];
        $fuel->save();

        return redirect()->route('showFuel', [$id]);
      }

      $fuel_b = $request['amount_before'];
      $fuel_cost_ar = $request['amount_refueled'];
      $fuel_cost_rp = $request['refueling_price'];
      $fuel_m = $request['mileage'];
      $fuel_cost = $fuel_cost_ar * $fuel_cost_rp;

      $fuel_ba = Fuel::where('car_id', $id)->orderBy('id', 'desc')->first()->amount_before;
      $fuel_ra = Fuel::where('car_id', $id)->orderBy('id', 'desc')->first()->amount_refueled;
      $fuel_mil = Fuel::where('car_id', $id)->orderBy('id', 'desc')->first()->mileage;
      $fuel_total = $fuel_ba + $fuel_ra;
      $fuel_used = $fuel_total - $fuel_b;
      $fuel_mil_total = $fuel_m - $fuel_mil;
      $fuel_used_total = (100 * $fuel_used)/$fuel_mil_total;

      $fuel = new Fuel();
      $fuel->car_id = $id;
      $fuel->amount_before = $fuel_b;
      $fuel->amount_refueled = $fuel_cost_ar;
      $fuel->refueling_price = $fuel_cost_rp;
      $fuel->mileage = $fuel_m;
      $fuel->date = $request['date'];
      $fuel->total_cost = $fuel_cost;
      $fuel->fuel_usage = $fuel_used_total;
      $fuel->mil_driven = $fuel_mil_total;
      $fuel->place = $request['place'];
      $fuel->save();

      return redirect()->route('showFuel', [$id]);
    }

    public function update(Request $request, $id)
    {
      $fuel_id = $request['fuel_id'];//pobieram id edytowanego tankowania
      $fuel_car_id = Fuel::where('car_id', $id)->where('id', '>', 0)->first()->id;//szukam pierwszego rekordu w tabeli - Fuel, dla wybranego auta
      if($fuel_id == $fuel_car_id)//spełnia warunek, gdy id edytowanego tankowania jest rowne id pobranego z bazy pierwszego rekordu
      {
        $fuel_cost_ar = $request['amount_refueled'];
        $fuel_cost_rp = $request['refueling_price'];
        $fuel_cost = $fuel_cost_ar * $fuel_cost_rp;


        $fuel = Fuel::findOrFail($request->fuel_id);
        $fuel->car_id = $id;
        $fuel->amount_before = $request['amount_before'];
        $fuel->amount_refueled = $fuel_cost_ar;
        $fuel->refueling_price = $fuel_cost_rp;
        $fuel->mileage = $request['mileage'];
        $fuel->date = $request['date'];
        $fuel->total_cost = $fuel_cost;
        $fuel->fuel_usage = 0;
        $fuel->mil_driven = 0;
        $fuel->place = $request['place'];
        $fuel->update($request->all());

        $fuel_car_id_next = Fuel::where('car_id', $id)->where('id', '>', $fuel_car_id)->first();//sprawdzam czy istnieje w bazie danych następny rekord w tabeli - Fuel
        if($fuel_car_id_next === NULL)//spełnia warunek, gdy nie ma następnego rekordu bazie danych
        {
          return back();
        }

        $fuel_next_id = Fuel::where('car_id', $id)->orderBy('id', 'asc')->where('id', '>', $fuel_id)->first()->id;//pobieram id następnego rekordu w tabeli - Fuel
        $fuel_ab = Fuel::where('car_id', $id)->where('id', $fuel_next_id)->value('amount_before');//pobieram wartosc -> ilosc przed, następnego rekordu
        $fuel_ar = Fuel::where('car_id', $id)->where('id', $fuel_next_id)->value('amount_refueled');//pobieram wartosc -> ilosc wlanego, następnego rekordu
        $fuel_rp = Fuel::where('car_id', $id)->where('id', $fuel_next_id)->value('refueling_price');//pobieram wartosc -> cena paliwa, następnego rekordu
        $fuel_mil = Fuel::where('car_id', $id)->where('id', $fuel_next_id)->value('mileage');//pobieram wartosc -> przebieg, następnego rekordu

        $fuel_amb = Fuel::where('car_id', $id)->orderBy('id', 'desc')->where('mileage', '<', $fuel_mil)->first()->amount_before;//pobieram wartosc - ilosc przed w tabeli - Fuel z rekordu w ktorym przebieg jest mniejszy
        $fuel_amr = Fuel::where('car_id', $id)->orderBy('id', 'desc')->where('mileage', '<', $fuel_mil)->first()->amount_refueled;// pobieram wartosc - ilosc wlanego w tabeli - Fuel z rekordu w ktorym przebieg jest mniejszy
        $fuel_milage = Fuel::where('car_id', $id)->orderBy('id', 'desc')->where('mileage', '<', $fuel_mil)->first()->mileage;//pobieram wartosc - przebieg w tabeli - Fuel z rekordu w ktorym przebieg jest mniejszy
        $fuel_total = $fuel_amb + $fuel_amr;//ilośc paliwa po tankowaniu w poprzednim rekordzie
        $fuel_used = $fuel_total - $fuel_ab;//ilosc paliwa zużytego do następnego tankowania (to update)
        $fuel_mil_total = $fuel_mil - $fuel_milage;//droga przejechana między tankowaniami (to update)
        $fuel_used_total = (100 * $fuel_used)/$fuel_mil_total;//zużycie paliwa między tankowaniami (to update)

        $fuel_next = Fuel::where('id', $fuel_next_id)->update(['fuel_usage' => $fuel_used_total, 'mil_driven' => $fuel_mil_total]);//aktualizacja następnego rekordu w bazie danych
        return back();
      }//koniec - edycja pierwszego tankowania

      $fuel_b = $request['amount_before'];//pobieramy wartosc - ilosc przed
      $fuel_cost_ar = $request['amount_refueled'];//pobieramy wartosc - ilosc wlanego paliwa
      $fuel_cost_rp = $request['refueling_price'];//pobieramy cene paliwa
      $fuel_m = $request['mileage'];//pobieramy przebieg auta w chwili tankowania
      $fuel_cost = $fuel_cost_ar * $fuel_cost_rp;//liczymy koszt paliwa

      $fuel_ba = Fuel::where('car_id', $id)->orderBy('id', 'desc')->where('mileage', '<', $fuel_m)->first()->amount_before;//pobieramy ilosc przed z poprzedniego tankowania
      $fuel_ra = Fuel::where('car_id', $id)->orderBy('id', 'desc')->where('mileage', '<', $fuel_m)->first()->amount_refueled;//pobieramy ilosc wlanego z poprzedniego tankowania
      $fuel_mil = Fuel::where('car_id', $id)->orderBy('id', 'desc')->where('mileage', '<', $fuel_m)->first()->mileage;//pobieramy przebieg z poprzedniego tankowania
      $fuel_total = $fuel_ba + $fuel_ra;//suma paliwa w baku z poprzedniego tankowania
      $fuel_used = $fuel_total - $fuel_b;//paliwo zużyte między tankowaniami 18
      $fuel_mil_total = $fuel_m - $fuel_mil;//przebyta odległosc między tankowaniami 335
      $fuel_used_total = (100 * $fuel_used)/$fuel_mil_total;//zużycie paliwa między tankowaniami

      $fuel = Fuel::findOrFail($request->fuel_id);
      $fuel->car_id = $id;
      $fuel->amount_before = $fuel_b;
      $fuel->amount_refueled = $fuel_cost_ar;
      $fuel->refueling_price = $fuel_cost_rp;
      $fuel->mileage = $fuel_m;
      $fuel->date = $request['date'];
      $fuel->total_cost = $fuel_cost;
      $fuel->fuel_usage = $fuel_used_total;
      $fuel->mil_driven = $fuel_mil_total;
      $fuel->place = $request['place'];
      $fuel->update($request->all());

      //Nastepne tankowanie do updatu (fuel_usage)

      $fuel_car_id_next = Fuel::where('car_id', $id)->where('id', '>', $fuel_id)->first();
      if($fuel_car_id_next === NULL)//spelniony warunek, gdy edytowane tankowanie jest ostatnie zapisane w bazie danych
      {
        return back();
      }

      $fuel_next_id = Fuel::where('car_id', $id)->orderBy('id', 'asc')->where('id', '>', $fuel_id)->first()->id;
      $fuel_ab = Fuel::where('car_id', $id)->where('id', $fuel_next_id)->value('amount_before');//
      $fuel_mil = Fuel::where('car_id', $id)->where('id', $fuel_next_id)->value('mileage');//

      $fuel_amb = Fuel::where('car_id', $id)->orderBy('id', 'desc')->where('mileage', '<', $fuel_mil)->first()->amount_before;
      $fuel_amr = Fuel::where('car_id', $id)->orderBy('id', 'desc')->where('mileage', '<', $fuel_mil)->first()->amount_refueled;
      $fuel_milage = Fuel::where('car_id', $id)->orderBy('id', 'desc')->where('mileage', '<', $fuel_mil)->first()->mileage;
      $fuel_total = $fuel_amb + $fuel_amr;
      $fuel_used = $fuel_total - $fuel_ab;
      $fuel_mil_total = $fuel_mil - $fuel_milage;
      $fuel_used_total = (100 * $fuel_used)/$fuel_mil_total;

      $fuel_next = Fuel::where('id', $fuel_next_id)->update(['fuel_usage' => $fuel_used_total, 'mil_driven' => $fuel_mil_total]);//aktualizacja kolejnego rekordu w bazie danych
      return back();
    }//koniec - edycja kolejnego tankowania

    public function remove(Request $request, $id)
    {
      $fuel_id = $request['fuel_id'];
      $fuel_id_first = Fuel::where('car_id', $id)->where('id', '>', 0)->first()->id;
      if($fuel_id == $fuel_id_first)
      {
        $fuel = Fuel::findOrFail($fuel_id);
        $fuel->delete();

        $fuel_id_next = Fuel::where('car_id', $id)->where('id', '>', $fuel_id)->first();
        if($fuel_id_next === NULL)
        {
          return back();
        }
        $fuel_id_next = Fuel::where('car_id', $id)->where('id', '>', $fuel_id)->first()->id;
        $fuel_next = Fuel::where('id', $fuel_id_next)->update(['fuel_usage' => 0, 'mil_driven' => 0]);

        return back();
      }

        $fuel_select = Fuel::findOrFail($fuel_id);
        $fuel_select->delete();

        $fuel_next = Fuel::where('car_id', $id)->where('id', '>', $fuel_id)->first();
        if($fuel_next === NULL)
        {
          return back();
        }
        $fuel_next = Fuel::where('car_id', $id)->where('id', '>', $fuel_id)->first()->id;
        $fuel_before = Fuel::where('car_id', $id)->where('id', '<', $fuel_next)->first()->id;

        $fuel_am_bef = Fuel::where('car_id', $id)->where('id', $fuel_before)->value('amount_before');
        $fuel_am_ref = Fuel::where('car_id', $id)->where('id', $fuel_before)->value('amount_refueled');
        $fuel_milage = Fuel::where('car_id', $id)->where('id', $fuel_before)->value('mileage');

        $fuel_am_bef_next = Fuel::where('car_id', $id)->where('id', $fuel_next)->value('amount_before');
        $fuel_milage_next = Fuel::where('car_id', $id)->where('id', $fuel_next)->value('mileage');

        $fuel_total_before = $fuel_am_bef + $fuel_am_ref;
        $fuel_used = $fuel_total_before - $fuel_am_bef_next;
        $fuel_mil_driven = $fuel_milage_next - $fuel_milage;
        $fuel_used_total = (100 * $fuel_used)/$fuel_mil_driven;

        $fuel_next = Fuel::where('id', $fuel_next)->update(['fuel_usage' => $fuel_used_total, 'mil_driven' => $fuel_mil_driven]);
        return back();

    }

    public function calculateOne(Request $request, $carId)
    {
      $this->validate($request, [
        'date2' => 'date',
        'date3' => 'date',
      ]);

      $date2 = $request['date2'];
      $date3 = $request['date3'];
      $cost_fuel = Fuel::where('car_id', $carId)->whereBetween('date', [$date2, $date3])->sum('total_cost');

      return response()->json([$cost_fuel, $date2, $date3]);
    }

    public function calculateTwo(Request $request, $carId)
    {
      $this->validate($request, [
        'date_2' => 'date',
        'date_3' => 'date',
      ]);

      $date_2 = $request['date_2'];
      $date_3 = $request['date_3'];
      $fuelMilDriven = Fuel::where('car_id', $carId)->whereBetween('date', [$date_2, $date_3])->pluck('mil_driven')->toArray();
      $fuel_c = Fuel::where('car_id', $carId)->whereBetween('date', [$date_2, $date_3])->pluck('mil_driven')->count();
      $consumptionFuel = Fuel::where('car_id', $carId)->whereBetween('date', [$date_2, $date_3])->pluck('fuel_usage');
      $suma = 0;
      for($i = 0;$i < $fuel_c;$i++)
      {
        $result = $fuelMilDriven[$i] * $consumptionFuel[$i];
        $suma += $result;
      }
      $sumMil = array_sum($fuelMilDriven);
      $result_total = round($suma/$sumMil, 2);
      return response()->json([$result_total, $date_2, $date_3]);
    }

    public function getToShowC($carId)
    {
      $fuelCostDate1 = Fuel::where('car_id', $carId)->select('date')->first();
      $fuelCostDate2 = Fuel::where('car_id', $carId)->select('date')->latest()->first();

      return response()->json([$fuelCostDate1, $fuelCostDate2]);
    }

    public function createChartC(Request $request, $carId)
    {
      $date1 = $request['date1'];
      $date2 = $request['date2'];

      $fuel_data = Fuel::where('car_id', $carId)->select('date', 'total_cost')->whereBetween('date', [$date1, $date2])->get();

      return response()->json($fuel_data);
    }

    public function getToShowU($carId)
    {
      $fuelUsageDate1 = Fuel::where('car_id', $carId)->select('date')->first();
      $fuelUsageDate2 = Fuel::where('car_id', $carId)->select('date')->latest()->first();

      return response()->json([$fuelUsageDate1, $fuelUsageDate2]);
    }

    public function createChartU(Request $request, $carId)
    {
      $date1 = $request['date3'];
      $date2 = $request['date4'];

      $fuel_data = Fuel::where('car_id', $carId)->select('date', 'fuel_usage')->whereBetween('date', [$date1, $date2])->get();

      return response()->json($fuel_data);
    }

}
