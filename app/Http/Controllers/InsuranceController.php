<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\CarReminder;
use Illuminate\Support\Facades\DB;
use App\Insurance;
use App\Car;
use App\Tire;
use App\User;

class InsuranceController extends Controller
{
  public function show($id)
  {
    $insuranceCheck = Insurance::where('car_id', $id)->first();
    if($insuranceCheck === NULL)
    {
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($id);
      $type_insurances = ["OC", "AC", "ASSISTANCE", "NNW", "Ubezpieczenie Szyb", "Ubezpieczenie Opon"];
      $type_select = Insurance::where('car_id', $id)->value('insurance_type');
      $tab_types = explode(',',$type_select);
      $tire = Tire::where('car_id', $id)->get();
      $insuranceHistory = $insuranceCheck;
      return view('insurance/show', ['car' => $car, 'car_one' => $car_one, 'type_insurances' => $type_insurances, 'insuranceHistory' => $insuranceHistory, 'tab_types' => $tab_types, 'tire' => $tire]);
    }
    $insurance_current = Insurance::where('car_id', $id)->orderBy('id','desc')->first();//Pobranie aktualnego ubezpieczenia
    $last_id = Insurance::where('car_id', $id)->orderBy('id', 'desc')->first()->id;//Pobranie ostatniego id
    $insuranceHistory = Insurance::where('car_id', $id)->where('id', '<', $last_id)->orderBy('id', 'desc')->get();
    $car = Car::where('user_id', Auth::id())->get();
    $car_one = Car::findOrFail($id);
    $type_insurances = ["OC", "AC", "ASSISTANCE", "NNW", "Ubezpieczenie Szyb", "Ubezpieczenie Opon"];
    $type_select = Insurance::where('car_id', $id)->where('id', $last_id)->value('insurance_type');
    $tab_types = explode(',',$type_select);
    $tire = Tire::where('car_id', $id)->get();
    return view('insurance/show', ['insurance_current' => $insurance_current, 'insuranceHistory' => $insuranceHistory, 'car' => $car, 'car_one' => $car_one, 'type_insurances' => $type_insurances, 'tab_types' => $tab_types, 'tire' => $tire]);
  }

  public function add(Request $request, $id)
  {
    $this->validate($request, [
      'company_name' => 'required|string|max:25',
      'product_name' => 'required|string|max:30',
      'insurance_type' => 'required|max:20',
      'policy_number' => 'required|string|max:12',
      'period_from' => 'required|date',
      'period_to' => 'required|date',
      'price' => 'required|string|max:6',
      'mileage' => 'required|string|max:7',
    ]);

    $ins_type_test = $request['insurance_type'];
    $show_type = implode(',', $ins_type_test);

    $insurance = new Insurance();
    $insurance->car_id = $id;
    $insurance->company_name = $request['company_name'];
    $insurance->product_name = $request['product_name'];
    $insurance->insurance_type = $show_type;
    $insurance->policy_number = $request['policy_number'];
    $insurance->period_from = $request['period_from'];
    $insurance->period_to = $request['period_to'];
    $insurance->price = $request['price'];
    $insurance->mileage = $request['mileage'];
    $insurance->save();

    return redirect()->route('showInsurance', [$id]);
  }

  public function getToEdit($insuranceId)
  {
    $insurance = Insurance::findOrFail($insuranceId);
    $types = $insurance->insurance_type;
    $arrayTypes = explode(',',$types);
    return response()->json([$insurance, $arrayTypes]);
  }

  public function update(Request $request, $insuranceId)
  {
    $test = $request['insurance_type'];
    $result = implode(',', $test);

    $insurance = Insurance::find($insuranceId);
    $insurance->car_id = $request['car_id'];
    $insurance->company_name = $request['company_name'];
    $insurance->product_name = $request['product_name'];
    $insurance->insurance_type = $result;
    $insurance->policy_number = $request['policy_number'];
    $insurance->period_from = $request['period_from'];
    $insurance->period_to = $request['period_to'];
    $insurance->price = $request['price'];
    $insurance->mileage = $request['mileage'];
    $insurance->update();

    return response()->json($insurance);
  }

  public function showNotifications($carId)
  {
    $user = User::find(Auth::id());

    $result = $user->unreadNotifications()->where([
      ['data', 'LIKE', '%"car_id":"'.$carId.'"%'],
      ['data', 'LIKE', '%"category":"Ubezpieczenie"%'],
    ])->get();
    return response()->json($result);
  }

}
