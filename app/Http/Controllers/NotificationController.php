<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Notifications\CarReminder;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Car;
use App\User;

class NotificationController extends Controller
{
    public function add(Request $request, $id)
    {
      $this->validate($request, [
        'date01' => 'required|date',
        'reminder' => 'required|string',
        'description' => 'required|string|max:124',
      ]);

      $carId = $id;
      $category = $request['category'];
      $dateEvent = $request['date01'];
      $reminder = $request['reminder'];
      if($reminder == '1 dzień wcześniej')
      {
        $numberOfDays = '1';
      }
      elseif($reminder == '2 dni wcześniej')
      {
        $numberOfDays = '2';
      }
      else
      {
        $numberOfDays = '3';
      }
      $description = $request['description'];

      $content = [
        'car_id' => $carId,
        'category' => $category,
        'date_event' => $dateEvent,
        'reminder' => $numberOfDays,
        'description' => $description,
      ];

      User::find(Auth::id())->notifyNow(new CarReminder($content), ['database']);

      if($category == 'Ubezpieczenie')
      {
        return redirect('insurance/show/car/'.$carId)->with('status', 'Powiadomienie zostało zapisane');
      }
      elseif($category == 'Serwis')
      {
        $carMake = Car::where('id', $carId)->value('make');
        $carModel = Car::where('id', $carId)->value('model');
        return redirect('service_plan/show/car/'.$carId.'/name/'.$carMake.'/'.$carModel)->with('status', 'Powiadomienie zostało zapisane');
      }
      elseif($category == 'Przeglad techniczny')
      {
        return redirect('technicalInspection/show/car/'.$carId)->with('status', 'Powiadomienie zostało zapisane');
      }
      else
      {
        return 'ErrorException!!!';
      }

    }

    public function getToEdit($notificationId)
    {
      $user = User::find(Auth::id());
      $notification = $user->notifications()->where('id', $notificationId)->get();
      return response()->json($notification);
    }

    public function update(Request $request, $notificationId)
    {
      $dateEvent = $request['date_event'];
      $reminder = $request['reminder'];
      $description = $request['description'];
      $user = User::find(Auth::id());
      $notificationDB = $user->notifications()->where('id', $notificationId)->first();
      $carId = $notificationDB->data[0]['car_id'];
      $category = $notificationDB->data[0]['category'];
      $notificationUpdate = DB::update("UPDATE `notifications` SET `data` = '[{\"car_id\":\"$carId\",\"category\":\"$category\",\"date_event\":\"$dateEvent\",\"reminder\":\"$reminder\",\"description\":\"$description\"}]' WHERE `notifications`.`id` = '$notificationId'");
      $notificationUpdated = $user->notifications()->where('id', $notificationId)->get();

      return response()->json($notificationUpdated);
    }

    public function show($notificationId)
    {
      $user = User::find(Auth::id());
      $notificationShow = $user->notifications()->where('id', $notificationId)->first();
      $carId = $notificationShow->data[0]['car_id'];
      $carMake = Car::where('id', $carId)->value('make');
      $carModel = Car::where('id', $carId)->value('model');
      $date = $notificationShow->data[0]['date_event'];
      $dayOfWeek = date("l", strtotime($date));
      $dayOfWeekPL = '';
      switch($dayOfWeek)
      {
        case "Monday":
                  $dayOfWeekPL = 'Poniedziałek';
                  break;
        case "Tuesday":
                  $dayOfWeekPL = 'Wtorek';
                  break;
        case "Wednesday":
                  $dayOfWeekPL = 'Środa';
                  break;
        case "Thursday":
                  $dayOfWeekPL = 'Czwartek';
                  break;
        case "Friday":
                  $dayOfWeekPL = 'Piątek';
                  break;
        case "Saturday":
                  $dayOfWeekPL = 'Sobota';
                  break;
        case "Sunday":
                  $dayOfWeekPL = 'Niedziela';
                  break;
      }
      return response()->json([$notificationShow, $carMake, $carModel, $dayOfWeekPL]);
    }

    public function markAsRead($notificationIdAsRead)
    {
      $user = User::find(Auth::id());
      $notificationToMarkAsRead = $user->notifications()->where('id', $notificationIdAsRead)->first();
      $notificationToMarkAsRead->markAsRead();
      return response()->json($notificationToMarkAsRead);
    }

    public function getToRemove($notificationId)
    {
      $user = User::find(Auth::id());
      $notificationToRemove = $user->notifications()->where('id', $notificationId)->first();
      return response()->json($notificationToRemove);
    }

    public function removeNotification($notificationId)
    {
      $user = User::find(Auth::id());
      $notificationDeleted = $user->notifications()->where('id', $notificationId)->first();
      $notificationDeleted->delete();
      return response()->json($notificationDeleted);
    }

    public function showAllRead()
    {
      $user = User::find(Auth::id());
      $allReadNotifications = $user->notifications()->whereNotNull('read_at')->get();
      $carsId = [];
      $allReadCars = [];
      foreach($allReadNotifications as $not)
      {
        $carId = $not->data[0]['car_id'];
        array_push($carsId, $carId);
      }
      for($i = 0; $i < count($carsId); $i++)
      {
        $cars = Car::select('make', 'model')->where('id', $carsId[$i])->first();
        array_push($allReadCars, $cars);
      }

      return response()->json([$allReadNotifications, $allReadCars]);
    }

    public function removeAllRead()
    {
      $user = User::find(Auth::id());
      $selectToDelete = $user->notifications()->whereNotNull('read_at')->delete();
      return response()->json($selectToDelete);
    }

    public function send(Request $request, $id)
    {
      $this->validate($request, [
        'descriptionSend' => 'required|string|max:188',
      ]);

      $carId = $id;
      $carMake = Car::where('id', $carId)->value('make');
      $carModel = Car::where('id', $carId)->value('model');
      $car = $carMake. ' ' .$carModel;
      $category = $request['category'];
      $messageContent = $request['descriptionSend'];

      $content = [
        'car' => $car,
        'category' => $category,
        'message' => $messageContent,
      ];
      $user = User::find(Auth::id());
      Notification::sendNow($user, new CarReminder($content), ['mail']);
      if($category == 'ubezpieczenie')
      {
        return redirect('insurance/show/car/'.$carId)->with('status', 'Wiadomość została wysłana');
      }
      elseif($category == 'serwis')
      {
        return redirect('service_plan/show/car/'.$carId.'/name/'.$carMake.'/'.$carModel)->with('status', 'Wiadomość została wysłana');
      }
      elseif($category == 'przegląd techniczny')
      {
        return redirect('technicalInspection/show/car/'.$carId)->with('status', 'Wiadomość została wysłana');
      }
      else
      {
        return 'ErrorException!!!';
      }
    }
}
