<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Car;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::where('user_id', Auth::id())->first();
      if($car_one === NULL)
      {
        return view('home', ['car' => $car]);
      }
      return view('home', ['car' => $car, 'car_one' => $car_one]);
    }
}
