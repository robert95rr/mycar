<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Car;
use App\Tire;

class UserController extends Controller
{
    public function show($id, $carId)
    {
      $user = User::findOrFail($id);
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($carId);
      $tire = Tire::where('car_id', $carId)->get();
      return view('user/show', ['user' => $user, 'car' => $car, 'car_one' => $car_one, 'tire' => $tire]);
    }

    public function showNotCars($id)
    {
      $user = User::findOrFail($id);
      $car = Car::where('user_id', Auth::id())->get();
      return view('user/show', ['user' => $user, 'car' => $car]);
    }

    public function getToEdit($userId)
    {
      $user = User::findOrFail($userId);
      return response()->json($user);
    }

    public function update(Request $request, $userId)
    {
      $user = User::findOrFail($userId);
      $user->first_name = $request['first_name'];
      $user->last_name = $request['last_name'];
      $user->email = $request['email'];
      $user->phone_number = $request['phone_number'];
      $user->update($request->all());

      return response()->json($user);
    }
}
