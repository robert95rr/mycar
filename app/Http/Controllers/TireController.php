<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Car;
use App\Tire;
use App\Width;
use App\Height;
use App\Diameter;
use App\Type;
use App\Tread_condition;

class TireController extends Controller
{

    public function addform($id)
    {
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($id);
      $tire = Tire::where('car_id', $id)->get();
      $widths = Width::all();
      $heights = Height::all();
      $diameters = Diameter::all();
      $types = Type::all();
      return view('tire/addform', ['tire' => $tire, 'car' => $car, 'car_one' => $car_one], compact('widths', 'heights', 'diameters', 'types'));
    }

    public function add(Request $request, $id)
    {
      $this->validate($request, [
        'manufacturer' => 'required|string|max:25',
        'model_t' => 'required|string|max:40',
        'type_id' => 'required|numeric',
        'dot' => 'max:4',
        'width_id' => 'required|numeric',
        'height_id' => 'required|numeric',
        'diameter_id' => 'required|numeric',
        'date_put' => 'required|date',
      ]);

      $dotCheck = $request['dot'];
      if($dotCheck === NULL)
      {
        $dotCheck = '____';
      }

      $tire = new Tire();
      $tire->car_id = $id;
      $tire->type_id = $request['type_id'];
      $tire->width_id = $request['width_id'];
      $tire->height_id = $request['height_id'];
      $tire->diameter_id = $request['diameter_id'];
      $tire->manufacturer = $request['manufacturer'];
      $tire->model = $request['model_t'];
      $tire->dot = $dotCheck;
      $tire->date_put = $request['date_put'];
      $tire->save();

      return redirect()->route('addFormTire', [$id]);
    }

    public function showone($tires, $car_oneId)
    {
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($car_oneId);
      $tire_one = Tire::findOrFail($tires);
      $width_one = Width::findOrFail($tire_one->width_id);
      $height_one = Height::findOrFail($tire_one->height_id);
      $diameter_one = Diameter::findOrFail($tire_one->diameter_id);
      $type_one = Type::findOrFail($tire_one->type_id);
      $seasons = Tread_condition::where('tires_id', $tires)->distinct()->pluck('season');
      $names = Tread_condition::where('tires_id', $tires)->distinct()->pluck('name');
      if(Auth::id() == $car_one->user_id)
      {
        $widths = Width::all();
        $heights = Height::all();
        $diameters = Diameter::all();
        $types = Type::all();
        $tire = Tire::where('car_id', $car_oneId)->get();
        return view('tire/showone', ['car' => $car, 'car_one' => $car_one, 'tire_one' => $tire_one, 'tire' => $tire, 'width_one' => $width_one, 'height_one' => $height_one, 'diameter_one' => $diameter_one, 'type_one' => $type_one, 'seasons' => $seasons, 'names' => $names], compact('widths', 'heights', 'diameters', 'types'));
      }
      return redirect()->route('showTire', [$tires, $car_oneId]);
    }

    public function update(Request $request, $tire_one, $car_one)
    {
      $tire = Tire::find($tire_one);
      $tire->car_id = $car_one;
      $tire->type_id = $request['type_id'];
      $tire->width_id = $request['width_id'];
      $tire->height_id = $request['height_id'];
      $tire->diameter_id = $request['diameter_id'];
      $tire->manufacturer = $request['manufacturer'];
      $tire->model = $request['model_t'];
      $tire->dot = $request['dot'];
      $tire->date_put = $request['date_put'];
      $tire->update($request->all());

      return redirect()->route('showTire', [$tire_one, $car_one]);
    }

    public function remove(Request $request)
    {
      $tread_condition = Tire::findOrFail($request->tire_id);
      $tread_condition->delete();

      return back();
    }

    public function showLP($tire_one, $car_oneId)
    {
      $tread_condition = Tread_condition::where('tires_id', $tire_one)->where('tire_position', 'LP')->get();
      $tire_pos_lp = 'LP';
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($car_oneId);
      $seasons = Tread_condition::where('tires_id', $tire_one)->distinct()->pluck('season');
      $names = Tread_condition::where('tires_id', $tire_one)->distinct()->pluck('name');
      $tire_one = Tire::findOrFail($tire_one);
      $tire = Tire::where('car_id', $car_oneId)->get();
      $width_one = Width::findOrFail($tire_one->width_id);
      $height_one = Height::findOrFail($tire_one->height_id);
      $diameter_one = Diameter::findOrFail($tire_one->diameter_id);
      $type_one = Type::findOrFail($tire_one->type_id);
      $widths = Width::all();
      $heights = Height::all();
      $diameters = Diameter::all();
      $types = Type::all();
      return view('tire/showone', ['tread_condition' => $tread_condition, 'tire_pos_lp' => $tire_pos_lp, 'car' => $car, 'car_one' => $car_one, 'tire_one' => $tire_one, 'tire' => $tire, 'width_one' => $width_one, 'height_one' => $height_one, 'diameter_one' => $diameter_one, 'type_one' => $type_one, 'seasons' => $seasons, 'names' => $names], compact('widths', 'heights', 'diameters', 'types'));
    }

    public function showLT($tire_one, $car_oneId)
    {
      $tread_condition = Tread_condition::where('tires_id', $tire_one)->where('tire_position', 'LT')->get();
      $tire_pos_lt = 'LT';
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($car_oneId);
      $seasons = Tread_condition::where('tires_id', $tire_one)->distinct()->pluck('season');
      $names = Tread_condition::where('tires_id', $tire_one)->distinct()->pluck('name');
      $tire_one = Tire::findOrFail($tire_one);
      $tire = Tire::where('car_id', $car_oneId)->get();
      $width_one = Width::findOrFail($tire_one->width_id);
      $height_one = Height::findOrFail($tire_one->height_id);
      $diameter_one = Diameter::findOrFail($tire_one->diameter_id);
      $type_one = Type::findOrFail($tire_one->type_id);
      $widths = Width::all();
      $heights = Height::all();
      $diameters = Diameter::all();
      $types = Type::all();
      return view('tire/showone', ['tread_condition' => $tread_condition, 'tire_pos_lt' => $tire_pos_lt, 'car' => $car, 'car_one' => $car_one, 'tire_one' => $tire_one, 'tire' => $tire, 'width_one' => $width_one, 'height_one' => $height_one, 'diameter_one' => $diameter_one, 'type_one' => $type_one, 'seasons' => $seasons, 'names' => $names], compact('widths', 'heights', 'diameters', 'types'));
    }

    public function showPP($tire_one, $car_oneId)
    {
      $tread_condition = Tread_condition::where('tires_id', $tire_one)->where('tire_position', 'PP')->get();
      $tire_pos_pp = 'PP';
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($car_oneId);
      $seasons = Tread_condition::where('tires_id', $tire_one)->distinct()->pluck('season');
      $names = Tread_condition::where('tires_id', $tire_one)->distinct()->pluck('name');
      $tire_one = Tire::findOrFail($tire_one);
      $tire = Tire::where('car_id', $car_oneId)->get();
      $width_one = Width::findOrFail($tire_one->width_id);
      $height_one = Height::findOrFail($tire_one->height_id);
      $diameter_one = Diameter::findOrFail($tire_one->diameter_id);
      $type_one = Type::findOrFail($tire_one->type_id);
      $widths = Width::all();
      $heights = Height::all();
      $diameters = Diameter::all();
      $types = Type::all();
      return view('tire/showone', ['tread_condition' => $tread_condition, 'tire_pos_pp' => $tire_pos_pp, 'car' => $car, 'car_one' => $car_one, 'tire_one' => $tire_one, 'tire' => $tire, 'width_one' => $width_one, 'height_one' => $height_one, 'diameter_one' => $diameter_one, 'type_one' => $type_one, 'seasons' => $seasons, 'names' => $names], compact('widths', 'heights', 'diameters', 'types'));
    }

    public function showPT($tire_one, $car_oneId)
    {
      $tread_condition = Tread_condition::where('tires_id', $tire_one)->where('tire_position', 'PT')->get();
      $tire_pos_pt = 'PT';
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($car_oneId);
      $seasons = Tread_condition::where('tires_id', $tire_one)->distinct()->pluck('season');
      $names = Tread_condition::where('tires_id', $tire_one)->distinct()->pluck('name');
      $tire_one = Tire::findOrFail($tire_one);
      $tire = Tire::where('car_id', $car_oneId)->get();
      $width_one = Width::findOrFail($tire_one->width_id);
      $height_one = Height::findOrFail($tire_one->height_id);
      $diameter_one = Diameter::findOrFail($tire_one->diameter_id);
      $type_one = Type::findOrFail($tire_one->type_id);
      $widths = Width::all();
      $heights = Height::all();
      $diameters = Diameter::all();
      $types = Type::all();
      return view('tire/showone', ['tread_condition' => $tread_condition, 'tire_pos_pt' => $tire_pos_pt, 'car' => $car, 'car_one' => $car_one, 'tire_one' => $tire_one, 'tire' => $tire, 'width_one' => $width_one, 'height_one' => $height_one, 'diameter_one' => $diameter_one, 'type_one' => $type_one, 'seasons' => $seasons, 'names' => $names], compact('widths', 'heights', 'diameters', 'types'));
    }

    public function addTC(Request $request, $id)
    {
      if($request->end_date === NULL){
      $this->validate($request, [
        'name' => 'required|string|max:20',
        'dot' => 'max:4',
        'start_tread' => 'max:4',
        'end_tread' => 'max:4',
        'start_date' => 'required|date',
        'start_mileage' => 'required|string|max:7',
        'end_mileage' => 'max:7',
        'tire_position' => 'required|string',
        'season' => 'required|string|max:10',
      ]);
    }else
    {
      $this->validate($request, [
        'name' => 'required|string|max:20',
        'dot' => 'max:4',
        'start_tread' => 'max:4',
        'end_tread' => 'max:4',
        'start_date' => 'required|date',
        'end_date' => 'date',
        'start_mileage' => 'required|string|max:7',
        'end_mileage' => 'string|max:7',
        'tire_position' => 'required|string',
        'season' => 'required|string|max:10',
      ]);
    }

      $st_mil = $request['start_mileage'];
      $en_mil = $request['end_mileage'];
      if($en_mil === NULL)
      {
        $en_mil = 0;
        $diff_mil = 0;
      }
      else
      {
        $diff_mil = $en_mil - $st_mil;
      }
      $dotCheck = $request['dot'];
      if($dotCheck === NULL)
      {
        $dotCheck = '____';
      }
      $startTreadCheck = $request['start_tread'];
      $endTreadCheck = $request['end_tread'];
      if($startTreadCheck === NULL)
      {
        $startTreadCheck = 0;
      }
      if($endTreadCheck === NULL)
      {
        $endTreadCheck = 0;
      }
      $endDateCheck = $request['end_date'];
      if($endDateCheck === NULL)
      {
        $tread_condition = new Tread_condition();//do poprawy nazwa C
        $tread_condition->tires_id = $id;
        $tread_condition->name = $request['name'];
        $tread_condition->dot = $dotCheck;
        $tread_condition->start_tread = $startTreadCheck;
        $tread_condition->end_tread = $endTreadCheck;
        $tread_condition->start_date = $request['start_date'];
        $tread_condition->start_mileage = $st_mil;
        $tread_condition->end_mileage = $en_mil;
        $tread_condition->diff_mileage = $diff_mil;
        $tread_condition->tire_position = $request['tire_position'];
        $tread_condition->season = $request['season'];
        $tread_condition->save();

        return back();
      }

      $tread_condition = new Tread_condition();
      $tread_condition->tires_id = $id;
      $tread_condition->name = $request['name'];
      $tread_condition->dot = $dotCheck;
      $tread_condition->start_tread = $startTreadCheck;
      $tread_condition->end_tread = $endTreadCheck;
      $tread_condition->start_date = $request['start_date'];
      $tread_condition->end_date = $endDateCheck;
      $tread_condition->start_mileage = $st_mil;
      $tread_condition->end_mileage = $en_mil;
      $tread_condition->diff_mileage = $diff_mil;
      $tread_condition->tire_position = $request['tire_position'];
      $tread_condition->season = $request['season'];
      $tread_condition->save();

      return back();
    }

    public function updateTC(Request $request, $id)//id -> tire_id
    {
      $start_mileage = $request['start_mileage'];
      $end_mileage = $request['end_mileage'];
      if($end_mileage == 0)
      {
        $diff_mileage = 0;
      }
      else
      {
        $diff_mileage = $end_mileage - $start_mileage;
      }

      $tread_condition = Tread_condition::findOrFail($request->tread_condition_id);
      $tread_condition->tires_id = $id;
      $tread_condition->name = $request['name'];
      $tread_condition->dot = $request['dot'];
      $tread_condition->start_tread = $request['start_tread'];
      $tread_condition->end_tread = $request['end_tread'];
      $tread_condition->start_date = $request['start_date'];
      $tread_condition->end_date = $request['end_date'];
      $tread_condition->start_mileage = $start_mileage;
      $tread_condition->end_mileage = $end_mileage;
      $tread_condition->diff_mileage = $diff_mileage;
      $tread_condition->tire_position = $request['tire_position'];
      $tread_condition->season = $request['season'];
      $tread_condition->update($request->all());

      return back();
    }

    public function removeTC(Request $request)
    {
      $tread_condition = Tread_condition::findOrFail($request->tread_condition_id);
      $tread_condition->delete();

      return back();
    }

    public function showSeason(Request $request, $tire_oneId, $car_oneId)
    {
      $seasonCheck = $request['seasonSelect'];
      $seasonSelected = Tread_condition::where('tires_id', $tire_oneId)->where('season', $seasonCheck)->get();
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($car_oneId);
      $tire_one = Tire::findOrFail($tire_oneId);
      $width_one = Width::findOrFail($tire_one->width_id);
      $height_one = Height::findOrFail($tire_one->height_id);
      $diameter_one = Diameter::findOrFail($tire_one->diameter_id);
      $type_one = Type::findOrFail($tire_one->type_id);
      $seasons = Tread_condition::where('tires_id', $tire_oneId)->distinct()->pluck('season');
      $names = Tread_condition::where('tires_id', $tire_oneId)->distinct()->pluck('name');
      $widths = Width::all();
      $heights = Height::all();
      $diameters = Diameter::all();
      $types = Type::all();
      $tire = Tire::where('car_id', $car_oneId)->get();
      return view('tire/showone', ['seasonSelected' => $seasonSelected, 'car' => $car, 'car_one' => $car_one, 'tire_one' => $tire_one, 'tire' => $tire, 'width_one' => $width_one, 'height_one' => $height_one, 'diameter_one' => $diameter_one, 'type_one' => $type_one, 'seasonCheck' => $seasonCheck, 'seasons' => $seasons, 'names' => $names], compact('widths', 'heights', 'diameters', 'types'));
    }

    public function showName(Request $request, $tire_oneId, $car_oneId)
    {
      $nameCheck = $request['nameSelect'];
      $nameSelected = Tread_condition::where('tires_id', $tire_oneId)->where('name', $nameCheck)->get();
      $car = Car::where('user_id', Auth::id())->get();
      $car_one = Car::findOrFail($car_oneId);
      $tire_one = Tire::findOrFail($tire_oneId);
      $width_one = Width::findOrFail($tire_one->width_id);
      $height_one = Height::findOrFail($tire_one->height_id);
      $diameter_one = Diameter::findOrFail($tire_one->diameter_id);
      $type_one = Type::findOrFail($tire_one->type_id);
      $seasons = Tread_condition::where('tires_id', $tire_oneId)->distinct()->pluck('season');
      $names = Tread_condition::where('tires_id', $tire_oneId)->distinct()->pluck('name');
      $widths = Width::all();
      $heights = Height::all();
      $diameters = Diameter::all();
      $types = Type::all();
      $tire = Tire::where('car_id', $car_oneId)->get();
      return view('tire/showone', ['nameSelected' => $nameSelected, 'car' => $car, 'car_one' => $car_one, 'tire_one' => $tire_one, 'tire' => $tire, 'width_one' => $width_one, 'height_one' => $height_one, 'diameter_one' => $diameter_one, 'type_one' => $type_one, 'nameCheck' => $nameCheck, 'seasons' => $seasons, 'names' => $names], compact('widths', 'heights', 'diameters', 'types'));
    }
}
