<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Car;
use App\Vehicle_type;
use App\Photo;
use App\Tire;

class CarController extends Controller
{
    public function addform()
    {
      $car = Car::where('user_id', Auth::id())->get();
      $vehicle_types = Vehicle_type::all();
      $car_one = Car::where('user_id', Auth::id())->first();
      return view('car/addform', ['car' => $car, 'car_one' => $car_one], compact('vehicle_types'));
    }

    public function add(Request $request)
    {
      $this->validate($request, [
           'make' => 'required|alpha|max:255',
           'model' => 'required|string|alpha_num|max:255',
           'vehicle_type_id' => 'required|numeric',
           'year_production' => 'required|numeric|max:4',
           'engine_size' => 'required|numeric|max:4',
           'engine_power' => 'required|numeric|max:4',
           'fuel_type' => 'required|string',
           'registration_number' => 'required|string|max:10',
           'vin_number' => 'required|alpha_num|min:17|max:17',
           'interior_colour' => 'required|string',
       ]);

        $car = new Car();
        $car->make = $request['make'];
        $car->model = $request['model'];
        $car->vehicle_type_id = $request['vehicle_type_id'];
        $car->user_id = Auth::user()->id;
        $car->year_production = $request['year_production'];
        $car->engine_size = $request['engine_size'];
        $car->engine_power = $request['engine_power'];
        $car->fuel_type = $request['fuel_type'];
        $car->registration_number = $request['registration_number'];
        $car->vin_number = $request['vin_number'];
        $car->interior_colour = $request['interior_colour'];
        $car->save();

		    return redirect()->route('home');
    }

    public function showone($id)
    {
      $checkPhotoCar = Photo::where('car_id', $id)->first();
      if($checkPhotoCar === NULL)
      {
        $car_one = Car::findOrFail($id);
        $vehicle_type_one = Vehicle_type::findOrFail($car_one->vehicle_type_id);
        $car = Car::where('user_id', Auth::id())->get();
        $photo_car = Photo::where('car_id', $id)->first();
        $vehicle_types = Vehicle_type::all();
        $tire = Tire::where('car_id', $id)->get();
        return view('car/showone', ['car' => $car, 'car_one' => $car_one, 'vehicle_types' => $vehicle_types, 'vehicle_type_one' => $vehicle_type_one, 'tire' => $tire], compact('photo_car'));
      }
      $car_one = Car::findOrFail($id);
      if(Auth::id() == $car_one->user_id) {
          $vehicle_type_one = Vehicle_type::findOrFail($car_one->vehicle_type_id);
          $photo_select = Photo::where('car_id', $id)->first();
          $car = Car::where('user_id', Auth::id())->get();
          $photo_car = Photo::where('car_id', $id)->first();
          $vehicle_types = Vehicle_type::all();
          $tire = Tire::where('car_id', $id)->get();
          return view('car/showone', ['car' => $car, 'car_one' => $car_one, 'vehicle_types' => $vehicle_types, 'vehicle_type_one' => $vehicle_type_one, 'tire' => $tire, 'photo_select' => $photo_select], compact('photo_car'));
      }
      return redirect()->route('showCar');
    }

    public function update(Request $request, $id)
    {

      $car = Car::find($id);
      $car->make = $request['make'];
      $car->model = $request['model'];
      $car->vehicle_type_id = $request['vehicle_type_id'];
      $car->user_id = Auth::user()->id;
      $car->year_production = $request['year_production'];
      $car->engine_size = $request['engine_size'];
      $car->engine_power = $request['engine_power'];
      $car->fuel_type = $request['fuel_type'];
      $car->registration_number = $request['registration_number'];
      $car->vin_number = $request['vin_number'];
      $car->interior_colour = $request['interior_colour'];

      if(Auth::id() == $car->user_id) {
            $car->update($request->all());
        }
      return redirect()->route('showCar', [$id]);
    }

    public function remove($id)
    {
      $car = Car::findOrFail($id);
      $car->delete();
      return redirect()->route('home');
    }

    public function store(Request $request, $id)
    {
      $originalPhoto = $request->file('name');
      $thumbnailPhoto = Image::make($originalPhoto);

      $targetPath = public_path('photos/');

      $thumbnailPhoto->resize(400,250);
      $thumbnailPhoto->save($targetPath.time().$originalPhoto->getClientOriginalName());

      $photo = new Photo();
      $photo->car_id = $id;
      $photo->name = time().$originalPhoto->getClientOriginalName();
      $photo->save();

      return redirect()->route('showCar', [$id]);
    }

    public function updatePhoto(Request $request, $id)
    {
      $originalPhoto = $request->file('name');
      $thumbnailPhoto = Image::make($originalPhoto);

      $targetPath = public_path('photos/');
      $photoToDelete = Photo::where('id', $id)->value('name');
      File::delete($targetPath.$photoToDelete);

      $thumbnailPhoto->resize(400,250);
      $thumbnailPhoto->save($targetPath.time().$originalPhoto->getClientOriginalName());

      $photoUpdate = Photo::findOrFail($id);
      $carId = $photoUpdate->car_id;
      $photoUpdate->car_id = $carId;
      $photoUpdate->name = time().$originalPhoto->getClientOriginalName();
      $photoUpdate->update();

      return back();
    }
}
