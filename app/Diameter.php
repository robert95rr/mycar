<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diameter extends Model
{
  protected $fillable = [
    'value'
  ];

  protected $table = 'diameter';
}
