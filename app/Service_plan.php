<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service_plan extends Model
{
    protected $fillable = [
      'car_model',
      'service_task',
      'interval',
      'description'
    ];

    protected $table = 'service_plan';
}
