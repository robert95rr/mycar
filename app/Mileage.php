<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mileage extends Model
{
  protected $fillable = [
    'car_id',
    'value',
    'date',
    'description'
  ];

  protected $table = 'mileage';

}
