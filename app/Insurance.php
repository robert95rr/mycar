<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
  protected $fillable = [
  'car_id',
  'company_name',
  'product_name',
  'insurance_type',
  'policy_number',
  'period_from',
  'period_to',
  'price',
  'mileage'
];

  protected $table = 'insurance';
}
